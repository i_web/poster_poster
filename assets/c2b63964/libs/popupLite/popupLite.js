jQuery(document).ready(function(){
    
    popupLite = {
        get: function( Args )
        {
            jQuery.ajax({
                type: "POST",
                url: "/ajax/popups/",
                data: { Args : Args },
                dataType: "json",
                beforeSend: function( XMLHttpRequest )
                {
                    flashLoader("open",0);
                    //console.log( { system : 'start of corePopup.get', Args : Args, XMLHttpRequest : XMLHttpRequest } );
                },
                error: function( XMLHttpRequest, textStatus, errorThrown )
                {
                    flashLoader("close",0);
                    //console.log( { system : 'error of corePopup.get', Args : Args, XMLHttpRequest : XMLHttpRequest, textStatus : textStatus, errorThrown : errorThrown } );
                },
                complete: function( XMLHttpRequest, textStatus )
                {
                    flashLoader("close",0);
                    //console.log( { system : 'success of corePopup.get', Args : Args, XMLHttpRequest : XMLHttpRequest, textStatus : textStatus } );
                },
                success: function(data)
                {
                    if(data.error == true)
                    {
                        console.log( data.response );
                        alert(data.response);
                        return false;
                    }
                    if(data.error == false)
                    {
                        if(Args.css)
                        {
                            popupLite.show(Args.title, data.response, Args.css );
                        }
                        else
                        {
                            popupLite.show(Args.title, data.response);
                        }
                    }
                }
            });
        },
        show: function( title, content, Args )
        {
            var popup = jQuery(".popupLite");
            
            if(Args)
            {
                popup.css(Args);
            }
            else
            {
                popup.removeAttr("style");
            }
            
            popup.html(content);
            
            popup.css({
                "visibility" : "hidden",
                "display" : "block",
            });
            
            var contentHeight = popup.find(".litePopupContent").outerHeight(true);
            
            popup.css({
                "visibility" : "visible",
                "display" : "none",
                "height" : contentHeight,
            });
            
            if( jQuery( window ).height() < jQuery( "html" ).height() )
            {
                jQuery("#bodyFade").show();
                jQuery("html,body").addClass("popupLock");
            }
            
            jQuery("#backFade").show();
            
            popup.show();
        },
        close: function()
        {
            jQuery(".popupLite").hide();
            setTimeout(function(){
                jQuery("#bodyFade").hide();
                jQuery("#backFade").hide();
                jQuery("html,body").removeClass("popupLock");
            },10)
            
            
            setTimeout(function(){
                //jQuery(".corePopup .corePopupBody .popup-loader").hide();
                jQuery(".popupLite").html("");
                jQuery(".popupLite").removeAttr("style");
            },500);
        },
        isVisible: function()
        {
            if( jQuery(".popupLite").is(":visible") )
            {
                return 1;
            }
            else
            {
                return 0;
            }
        },
    }
    
});