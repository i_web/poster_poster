jQuery(document).ready(function(){
    
    corePopup = {
        get: function( Args )
        {
            jQuery.ajax({
                type: "POST",
                url: "/ajax/popups/",
                data: { Args : Args },
                dataType: "json",
                beforeSend: function( XMLHttpRequest )
                {
                    flashLoader("open",0);
                    //console.log( { system : 'start of corePopup.get', Args : Args, XMLHttpRequest : XMLHttpRequest } );
                },
                error: function( XMLHttpRequest, textStatus, errorThrown )
                {
                    flashLoader("close",0);
                    //console.log( { system : 'error of corePopup.get', Args : Args, XMLHttpRequest : XMLHttpRequest, textStatus : textStatus, errorThrown : errorThrown } );
                },
                complete: function( XMLHttpRequest, textStatus )
                {
                    flashLoader("close",0);
                    //console.log( { system : 'success of corePopup.get', Args : Args, XMLHttpRequest : XMLHttpRequest, textStatus : textStatus } );
                },
                success: function(data)
                {
                    if(data.error == true)
                    {
                        console.log( data.response );
                        alert(data.response);
                        return false;
                    }
                    if(data.error == false)
                    {
                        if(Args.css)
                        {
                            corePopup.show(Args.title, data.response, Args.css );
                        }
                        else
                        {
                            corePopup.show(Args.title, data.response);
                        }
                    }
                }
            });
        },
        show: function( title, content, Args )
        {
            var popup = jQuery(".corePopup");
            var popupHeader = popup.find(".corePopupTitle");
            var popupBody = popup.find(".corePopupBody");
            var popupContent = popup.find(".corePopupContent");
            
            if(Args)
            {
                popup.css(Args);
            }
            else
            {
                popup.removeAttr("style");
            }
            
            popupHeader.text(title);
            popupContent.html(content);
            
            popup.css({
                "visibility" : "hidden",
                "display" : "block",
            });
            
            var contentHeight = popupContent.outerHeight(true);
            var headerHeight = popupHeader.outerHeight(true);
            var contentHeightNew = contentHeight+headerHeight;
            
            popup.css({
                "visibility" : "visible",
                "display" : "none",
                "height" : contentHeightNew,
            });
            
            if( jQuery( window ).height() < jQuery( "html" ).height() )
            {
                jQuery("#bodyFade").show();
                jQuery("html,body").addClass("popupLock");
            }
            
            jQuery("#backFade").show();
            
            popup.show();
        },
        close: function()
        {
            jQuery(".corePopup").hide();
            setTimeout(function(){
                jQuery("#bodyFade").hide();
                jQuery("#backFade").hide();
                jQuery("html,body").removeClass("popupLock");
            },10)
            
            
            setTimeout(function(){
                //jQuery(".corePopup .corePopupBody .popup-loader").hide();
                jQuery(".corePopup .corePopupTitle").text("");
                jQuery(".corePopup .corePopupContent").html("");
                jQuery(".corePopup").removeAttr("style");
            },500);
        },
        isVisible: function()
        {
            if( jQuery(".corePopup").is(":visible") )
            {
                return 1;
            }
            else
            {
                return 0;
            }
        },
    }
    
});