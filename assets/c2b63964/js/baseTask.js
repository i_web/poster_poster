// baseTask
baseTask = {
    viewDir: function()
    {
        window.history.pushState(null,null, urlModify( 'remove', { 'key' : 'taskID' } ) );
        loadAsync();
    },
    viewTask: function( taskID )
    {
        window.history.pushState(null,null, urlModify( 'add', { 'key' : 'taskID', 'value' : taskID } ) );
        loadAsync();
    },
    delete: function()
    {
        if( confirm( "Вы действительно хотите удалить задачу и весь вложенный материал?" ) )
        {
            coreRequest( "base.task.delete" );
            baseTask.viewDir();
        }
    },
    create: function()
    {
        corePopup.get({
            'title' : 'Создать задачу',
            'popup' : 'bases/tabsTask/create',
            'baseID' : jQuery.trim(jQuery("#baseID").val()),
            'css' : { 'width' : '400px' }
        });
    },
    save: function()
    {
        coreRequest( "base.task.save" );
    },
}
// baseTask