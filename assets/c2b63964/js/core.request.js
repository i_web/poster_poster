(function(jQuery) {
    
    jQuery.fn.coreRequest = function( options ) {
        var settings = jQuery.extend({
            type : "POST",
            url : false,
            data : false,
            dataType : "json",
            onBeforeSend : false,
            onError : true,
            onComplete : false,
            onSuccess : true,
            flashLoader : 0,
            reload : false,
        }, options);
        
        if( settings.url && settings.data )
        {
            jQuery.ajax({
                type: settings.type,
                url: settings.url,
                data: settings.data,
                dataType: settings.dataType,
                beforeSend: function(  )
                {
                    if( settings.onBeforeSend )
                    {
                        flashLoader("open", settings.flashLoader);
                    }
                },
                error: function( textStatus, errorThrown )
                {
                    if( settings.onError )
                    {
                        flashLoader("close", settings.flashLoader);
                    }
                },
                complete: function( textStatus )
                {
                    if( settings.onComplete )
                    {
                        flashLoader("close", settings.flashLoader);
                    }
                },
                success: function(data, textStatus)
                {
                    if( settings.onSuccess )
                    {
                        if(data.error == true)
                        {
                            flashLoader("close", settings.flashLoader);
                            alert(data.response);
                            return false;
                        }
                        if(data.error == false)
                        {
                            if( settings.reload )
                            {
                                window.location.reload( settings.reload );
                            }
                            else
                            {
                                this.html( data.response );
                            }
                        }
                    }
                }
            });
        }
        else
        {
            alert("Request Error");
        }
    }
    
    
}(jQuery));