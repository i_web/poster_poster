baseRunTime = {
    export: function()
    {
        var baseID = jQuery.trim(jQuery("#baseID").val());
        
        jQuery.ajax({
            type: "POST",
            url: "/ajax/base/",
            data: { action : "base.runtime.export", baseID : baseID },
            dataType: "json",
            beforeSend: function()
            {
                flashLoader("open",0);
            },
            success: function(data)
            {
                if(data.error == true)
                {
                    flashLoader("close",0);
                    alert(data.response);
                    return false;
                }
                if(data.error == false)
                {
                    window.location.reload();
                }
            }
        });
    },
    
    reload: function( itemID )
    {
        var baseID = jQuery.trim(jQuery("#baseID").val());
        
        jQuery.ajax({
            type: "POST",
            url: "/ajax/base/",
            data: { action : "base.runtime.reload", baseID : baseID, itemID : itemID },
            dataType: "json",
            beforeSend: function()
            {
                flashLoader("open",0);
            },
            success: function(data)
            {
                if(data.error == true)
                {
                    flashLoader("close",0);
                    alert(data.response);
                    return false;
                }
                if(data.error == false)
                {
                    window.location.reload();
                }
            }
        });
    },
    delete: function( itemID )
    {
        var baseID = jQuery.trim(jQuery("#baseID").val());
        
        jQuery.ajax({
            type: "POST",
            url: "/ajax/base/",
            data: { action : "base.runtime.delete", baseID : baseID, itemID : itemID },
            dataType: "json",
            beforeSend: function()
            {
                flashLoader("open",0);
            },
            success: function(data)
            {
                if(data.error == true)
                {
                    flashLoader("close",0);
                    alert(data.response);
                    return false;
                }
                if(data.error == false)
                {
                    window.location.reload();
                }
            }
        });
    },
}