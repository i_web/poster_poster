baseTags = {
    clevel: function(  )
    {
        var clevel = jQuery(".popupForm #id_parent").children(":selected").attr("id");
        jQuery(".popupForm #clevel").val(clevel);
    },
    create: function()
    {
        corePopup.get({
            'title' : 'Создать группу тегов',
            'popup' : 'bases/tabsTags/create',
            'baseID' : jQuery.trim(jQuery("#baseID").val()),
            'css' : { 'width' : '800px' }
        });
    },
    edit: function( itemID )
    {
        corePopup.get({
            'title' : 'Редактировать группу тегов',
            'popup' : 'bases/tabsTags/create',
            'baseID' : jQuery.trim(jQuery("#baseID").val()),
            'itemID' : itemID,
            'css' : { 'width' : '800px' }
        });
    },
    save: function()
    {
        jQuery.ajax({
            type: "POST",
            url: "/ajax/base/",
            data: { action : "base.tags.group.save", postData : addToArray( jQuery(".popupForm .user-data") ) },
            dataType: "json",
            beforeSend: function()
            {
                flashLoader("open",0);
            },
            success: function(data)
            {
                if(data.error == true)
                {
                    flashLoader("close",0);
                    alert(data.response);
                    return false;
                }
                if(data.error == false)
                {
                    flashLoader("close",0);
                    corePopup.close();
                    setTimeout(function(){
                        loadAsync();
                    },1000);
                }
            }
        });
    },
}