basePostList = {
    deleteItems: function()
    {
        if( basePostList.countListElements() > 0 && basePostList.countListCheckBox() > 0 )
        {
            var itemID = basePostList.saveListCheckBox();
            if( confirm( "Вы действительно хотите удалить выбранный материал?" ) )
            {
                jQuery.ajax({
                    type: "POST",
                    url: "/ajax/base/",
                    data: { action : "base.post.delete", itemID : itemID },
                    dataType: "json",
                    beforeSend: function()
                    {
                        corePopup.close();
                        flashLoader("open",0)
                    },
                    success: function(data)
                    {
                        if(data.error == true)
                        {
                            flashLoader("close",0)
                            alert(data.response);
                            return false;
                        }
                        if(data.error == false)
                        {
                            for(i=0;i<itemID.length;i++)
                            {
                                jQuery( 'li.post-itemID#'+itemID[i] ).remove();
                            }
                            flashLoader("close",0)
                            flashNotify('#'+itemID.length+' Постов успешно удалено',2000);
                        }
                    }
                });
            }
        }
        else
        {
            alert("Нет выбранных элементов");
        }
    },
    showTagsManager: function( type )
    {
        var baseID = jQuery("input[type=hidden]#baseID").val();
        
        switch( type )
        {
            case "criteria":
            if( basePostList.countListElements() > 0 && basePostList.countListCheckBox() > 0 )
            {
                corePopup.get({
                    'title' : 'Управление тегами',
                    'popup' : 'bases/tabsPosts/listManager.tagsCriteria',
                    'css' : { 'width' : '900px' }
                });
            }
            else
            {
                alert("Нет выбранных элементов");
            }
            break;
            
            case "group":
            if( basePostList.countListElements() > 0 && basePostList.countListCheckBox() > 0 )
            {
                corePopup.get({
                    'title' : 'Управление тегами',
                    'popup' : 'bases/tabsPosts/listManager.tagsGroup',
                    'baseID' : baseID,
                    'css' : { 'width' : '900px' }
                });
            }
            else
            {
                alert("Нет выбранных элементов");
            }
            break;
        }
    },
    showListManager: function()
    {
        if( basePostList.countListElements() > 0 && basePostList.countListCheckBox() > 0 )
        {
            corePopup.get({
                'title' : 'Управление списком',
                'popup' : 'bases/tabsPosts/listManager',
                'css' : { 'width' : '900px' }
            });
        }
        else
        {
            alert("Нет выбранных элементов");
        }
    },
    countListElements: function()
    {
        return jQuery("ul.postList").find("li.post-itemID").size();
    },
    countListCheckBox: function()
    {
        return jQuery("ul.postList").find("li.post-itemID input[type=checkbox]:checked").size();
    },
    saveListCheckBox: function()
    {
        var arrData = [];
        var i = 0;
        jQuery("ul.postList").find("li.post-itemID input[type=checkbox]:checked").each(function(){
            arrData.push(  parseInt( jQuery(this).attr("id") ) );
        });
        return arrData;
    },
    countListOpen: function()
    {
        return jQuery("ul.postList").find("li.post-itemID.open").size();
    },
    checkAll: function()
    {
        var checkBox = jQuery("ul.postList").find("li.post-itemID input[type=checkbox]");
        if( basePostList.countListElements() > 0 )
        {
            if( basePostList.countListCheckBox() > 0 )
            {
                checkBox.prop("checked",false);
            }
            else
            {
                checkBox.prop("checked",true);
            }
        }
        else
        {
            alert("Ничего не выбрано");
        }
    },
    showAll: function()
    {
        var listData = jQuery("ul.postList").find("li.post-itemID");
        if( basePostList.countListElements() > 0 )
        {
            if( basePostList.countListOpen() > 0 )
            {
                listData.removeClass("open");
            }
            else
            {
                listData.addClass("open");
            }
        }
        else
        {
            alert("Ничего не выбрано");
        }
    },
}