jQuery(document).ready(function(){
    
    jQuery("body").on( "change", ".baseConnectGroupForm #groupSID", function(){
        var block = jQuery(".baseConnectGroupForm");
        var elem = jQuery(this);
        if( elem.val() > 0 )
        {
            jQuery.ajax({
                type: "POST",
                url: "/ajax/base/",
                data: { action : "base.group.loadData", userData : addToArray(block.find(".user-data")) },
                dataType: "json",
                success: function(data)
                {
                    if(data.error == true)
                    {
                        alert(data.response);
                        return false;
                    }
                    if(data.error == false)
                    {
                        block.find("select#userSID").removeAttr("disabled").html( data.response );
                    }
                }
            });
        }
    });
    
    base = {
        active: function( baseID, groupID, element )
        {
            jQuery.ajax({
                type: "POST",
                url: "/ajax/base/",
                data: { action : "base.active", baseID : baseID, groupID : groupID },
                dataType: "json",
                success: function(data)
                {
                    if(data.error == true)
                    {
                        alert(data.response);
                        return false;
                    }
                    if(data.error == false)
                    {
                        jQuery( element ).text( data.response );
                    }
                }
            });
        },
        baseConnectGroup: function( baseID )
        {
            corePopup.get({
                'title' : 'Привязать группу и пользователя',
                'popup' : 'bases/connect',
                'baseID' : baseID,
                //'css' : { 'width' : '800px' }
            });
        },
        createBase: function()
        {
            /*
            corePopup.get({
                'title' : 'Создать базу',
                'popup' : 'ajax/displayBaseCreate',
                //'css' : { 'width' : '800px' }
            });
            */
            popupLite.get({ 'popup' : 'ajax/displayBaseCreate' });
        },
        create: function()
        {
            var block = jQuery(".baseCreateForm");
            if(checkRequired(block.find("input.required")) == true)
            {
                jQuery.ajax({
                    type: "POST",
                    url: "/ajax/base/",
                    data: { action : "base.create", userData : addToArray(block.find("input.user-data")) },
                    dataType: "json",
                    success: function(data)
                    {
                        if(data.error == true)
                        {
                            alert(data.response);
                            return false;
                        }
                        if(data.error == false)
                        {
                            corePopup.close();
                            setTimeout(function(){
                                window.location.href = './';
                            },1000);
                        }
                    }
                });
            }
        },
        connectGroup: function()
        {
            var block = jQuery(".baseConnectGroupForm");
            if(checkRequired(block.find(".required")) == true)
            {
                jQuery.ajax({
                    type: "POST",
                    url: "/ajax/base/",
                    data: { action : "base.group.connect", userData : addToArray(block.find(".user-data")) },
                    dataType: "json",
                    success: function(data)
                    {
                        if(data.error == true)
                        {
                            alert(data.response);
                            return false;
                        }
                        if(data.error == false)
                        {
                            corePopup.close();
                            setTimeout(function(){
                                window.location.href = './';
                            },1000);
                        }
                    }
                });
            }
        }
    }
});