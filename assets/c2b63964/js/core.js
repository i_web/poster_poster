jQuery(document).ready(function(){
    
    window.onload = function() {
        window.setTimeout(function() {
        window.addEventListener("popstate", function(e) {
            loadAsync();
        }, true);
      }, 1);
    }
    
    /*
    jQuery("[data-fancybox").fancybox({
        loop: true,
    });
    */
    
    jQuery("body").on( "click", ".fancybox-close", function(){
        $.fancybox.close();
    });
    
    jQuery( "body" ).on( "click", ".fancybox-ajax-url", function(e){
        jQuery( this ).fancybox({
            type: 'ajax'
        });
    });
    
    jQuery().fancybox({
        selector : '[data-fancybox]',
        loop     : true,
        hash : false,
    });
    
    jQuery().fancybox({
        helpers: { 
            title: null
        }
    });
    
    jQuery(".fancybox.iframe").fancybox({
        type : "image"
    });
    
    jQuery("body").on("click", "ul.checkList li", function(){
        if( jQuery(this).hasClass("checked") )
        {
            jQuery(this).removeClass("checked");
        }
        else
        {
            jQuery(this).addClass("checked");
        }
    });
    
    jQuery("body").on("click", ".postList li.post-itemID .titleBlock", function(event){
        if( event.ctrlKey )
        {
            if( jQuery(this).find("input[type=checkbox]").is(":checked") )
            {
                jQuery(this).find("input[type=checkbox]").prop("checked",false);
            }
            else
            {
                jQuery(this).find("input[type=checkbox]").prop("checked",true);
            }
        }
        else
        {
            if( jQuery(this).parents("li.post-itemID").hasClass("open") )
            {
                jQuery(this).parents("li.post-itemID").removeClass("open");
            }
            else
            {
                jQuery(this).parents("li.post-itemID").addClass("open");
            }
        }
    });
    
    jQuery("body").on("click", ".tabs-nav.tabsLoadSlide li:not(.active)", function(){
        var elem = jQuery(this);
        var nav = jQuery(this).parent(".tabs-nav");
        var content = nav.nextAll(".tabs-descr");
        
        nav.children(".active").removeClass("active");
        content.children(".active").removeClass("active");
        
        elem.addClass("active");
                
        content.children("div:eq("+elem.index()+")").addClass("active");
    });
    
    // TabsAsync
    jQuery("body").on("click", ".tabs-nav.tabsLoadAsync li:not(.active)", function(){
        window.history.pushState(null,null, '?tabs=' + jQuery(this).attr("id") );
        loadAsync();
    });
    // TabsAsync
    
    jQuery("body").on("click", ".tabsLoadPagination li:not(.active) a", function(e){
        e.preventDefault();
        e.stopImmediatePropagation();
        window.history.pushState(null,null, jQuery(this).attr( "href" ) );
        loadAsync();
    });
    
    jQuery("body").on("click", "a.async-link:not(.active)", function(e){
        e.preventDefault();
        e.stopImmediatePropagation();
        window.history.pushState(null,null, jQuery(this).attr("href") );
        loadAsync();
    });
    
    jQuery("body").on("click", "#backToBookMarksIndex", function(){
        window.history.pushState(null,null, urlModify( 'remove', { 'key' : 'page' } ) );
        window.history.pushState(null,null, urlModify( 'remove', { 'key' : 'groupID' } ) );
        loadAsync();
    });
    
    jQuery("body").on("click", ".baseBookMarkView", function(){
        window.history.pushState(null,null, urlModify( 'add', { 'key' : 'groupID', 'value' : jQuery(this).attr("id") } ) );
        loadAsync();
    });
    
    jQuery("body").on("keypress", "#asyncSearchPosts", function(e){
        if(e.keyCode == 13)
        {
            var text = jQuery.trim(jQuery(this).val())
            if( text.length > 0 )
            {
                window.history.pushState(null,null, urlModify( 'remove', { 'key' : 'page' } ) );
                window.history.pushState(null,null, urlModify( 'add', { 'key' : 'stext', 'value' : text } ) );
                loadAsync();
            }
        }
    });
    
    auth = {
        login: function( event )
        {
            event.preventDefault();
            var block = jQuery(".loginForm");
            if(checkRequired(block.find("input.required")) == true)
            {
                jQuery.ajax({
                    type: "POST",
                    url: "/backend/ajax/auth/",
                    data: { action : "login", userData : addToArray(block.find("input.user-data")) },
                    dataType: "json",
                    success: function(data)
                    {
                        if(data.error == true)
                        {
                            alert(data.response);
                            return false;
                        }
                        if(data.error == false)
                        {
                            window.location.href = '/';
                        }
                    }
                });
            }
        },
        logout: function()
        {
            jQuery.ajax({
                type: "POST",
                url: "/ajax/auth/",
                data: { action : "logout" },
                dataType: "json",
                success: function(data)
                {
                    if(data.error == true)
                    {
                        alert(data.response);
                        return false;
                    }
                    if(data.error == false)
                    {
                        window.location.href = '/';
                    }
                }
            });
        }
    }
    vk = {
        getToken: function( call )
        {
            switch( call )
            {
                default:
                var block = jQuery(".vkTokenSaveForm");
                if(checkRequired(block.find("input.required")) == true)
                {
                    jQuery.ajax({
                        type: "POST",
                        url: "/ajax/vk/",
                        data: { action : "api", method : 'access.token', userData : addToArray(block.find("input.user-data")) },
                        dataType: "json",
                        success: function(data)
                        {
                            if(data.error == true)
                            {
                                alert(data.response);
                                return false;
                            }
                            if(data.error == false)
                            {
                                corePopup.close();
                                setTimeout(function(){
                                    window.location.href = './';
                                },1000);
                            }
                        }
                    });
                }
                break;
                
                case "display":
                corePopup.get({
                    'title' : 'getAccessToken',
                    'popup' : '/accounts/getToken',
                    'css' : { 'width' : '500px' }
                });
                break;
            }
        },
        auth: function()
        {
            jQuery.ajax({
                type: "POST",
                url: "/ajax/vk/",
                data: { action : "auth" },
                dataType: "json",
                success: function(data)
                {
                    if(data.error == true)
                    {
                        alert(data.response);
                        return false;
                    }
                    if(data.error == false)
                    {
                        window.open( data.response, '_blank' );
                    }
                }
            });
        },
        api: function( method )
        {
            jQuery.ajax({
                type: "POST",
                url: "/ajax/vk/",
                data: { action : "api", method : method },
                dataType: "json",
                success: function(data)
                {
                    if(data.error == true)
                    {
                        alert(data.response);
                        return false;
                    }
                    if(data.error == false)
                    {
                        //console.log( data.response );
                        window.location.reload();
                    }
                }
            });
        }
    }
});

function loadAsync( Args )
{
    jQuery.ajax({
        type: "POST",
        url: getLocation(),
        dataType: "json",
        beforeSend: function()
        {
            flashLoader("open",0);
        },
        success: function(data)
        {
            flashLoader("close",0);
            jQuery( ".container-fluid.container-fullw" ).html( data );
            left_navi( getLocation() );
        }
    });
}

function left_navi( location ) {
    var loc = location.trim().split( "/" ).filter( item => item );
    if( typeof( loc[0] ) == 'string' ) {
        jQuery( ".main-navigation-menu li.active.open" ).removeClass("active").removeClass("open");
        jQuery( ".main-navigation-menu li#menu_" + loc[0] ).addClass("active").addClass("open");
    }
    else if( typeof( loc[0] ) == 'undefined' ) {
        jQuery( ".main-navigation-menu li.active.open" ).removeClass("active").removeClass("open");
        jQuery( ".main-navigation-menu li#menu_index" ).addClass("active").addClass("open");
    }
}

function coreRequest( action, Args )
{
    Args = Args ? Args : false;
    
    if( corePopup.isVisible() == 1 )
    {
        var userData = addToArray(jQuery(".popupForm .user-data"));
    }
    if( corePopup.isVisible() == 0 )
    {
        var userData = addToArray(jQuery(".itemList.active .user-data"));
    }
    
    jQuery.ajax({
        type : "POST",
        url : "/ajax/base/",
        data : { action : action, Args : Args, userData : userData },
        dataType : "json",
        beforeSend : function()
        {
            flashLoader("open",0);
        },
        error : function()
        {
            flashLoader("close",0);
        },
        success : function( data )
        {
            if( data.error == true )
            {
                flashLoader("close",0);
                alert( data.response );
                return false;
            }
            if( data.error == false )
            {
                corePopup.close();
                flashLoader("close",0);
                loadAsync();
            }
        }
    });
}

function joinArray(obj, sep)
{
    var arr = [];
    var i = 0;
    for(var key in obj)
    {
        arr[i++] = key+"_"+obj[key];
    }
    return arr.join(sep);
}

function addToArray(block)
{
    var arrData = {};
    block.each(function(){
        arrData[this.id] = jQuery.trim(this.value);
    });
    return arrData;
}

function checkRequired2(block)
{
    var totalSize = block.size();
    var enterSize = 0;
    block.each(function(){
        var text = jQuery.trim(jQuery(this).val());
        var itemID = jQuery(this).attr("id");
        if(text.length == 0)
        {
            alert(jQuery(this).attr("tooltip"));
            return false;
        }
        else
        {
            enterSize++;
        }
    });
    
    if(totalSize == enterSize)
    {
        return true;
    }
    else
    {
        return false;
    }
}

function flashLoader(display, lock)
{
    if(display == 'open')
    {
        if(lock == 1)
        {
            jQuery("html,body").addClass("popupLock");
        }
        jQuery(".flashLoader").show();
    }
    if(display == 'close')
    {
        if(lock == 1)
        {
            jQuery("html,body").removeClass("popupLock");
        }
        jQuery(".flashLoader").hide();
    }
}

function flashNotify(content, timer, Args)
{
    if(Args)
    {
        jQuery(".flashNotify").css(Args);
    }
    if( jQuery(".flashNotify").is(":visible") )
    {
        jQuery(".flashNotify .cell").append("<br />"+content);
        setTimeout(function(){
            jQuery(".flashNotify").fadeOut();
        },timer*2);
    }
    else
    {
        jQuery(".flashNotify .cell").html(content);
        jQuery(".flashNotify").fadeIn();
        setTimeout(function(){
            jQuery(".flashNotify").fadeOut();
        },timer);
    }
}

function getLocation()
{
    var loc = window.location;
    var page = loc.hash ? loc.hash.substring(1) : loc.pathname + loc.search;
    return page;
}

function checkList( list )
{
    var arrData = [];
    jQuery( list ).each(function(){
        arrData.push( jQuery(this).attr("value") );
    });
    
    if( arrData.length > 0 )
    {
        return arrData.join(",");
    }
    else
    {
        return '';
    }
}

function urlModify( type, args )
{
    var retVal = '';
    var currUrl = document.location.search.substr(1).split('&');
    if( type == 'add' )
    {
        if( currUrl == '' )
        {
            retVal = '?' + args['key'] + '=' + args['value'];
        }
        else
        {
            var i = currUrl.length;
            var x;
            while(i--)
            {
                var key = currUrl[i].split('=');
                
                if( key[0] == args['key'] )
                {
                    key[1] = args['value'];
                    currUrl[i] = key.join('=');
                    break;
                }
            }
            
            
            if( i < 0 )
            {
                currUrl[currUrl.length] = [args['key'],args['value']].join('=');
            }
            
            retVal = '?' + currUrl.join('&');
        }
    }
    if( type == 'remove' )
    {
        for( i = 0; i < currUrl.length; i++ )
        {
            var key = currUrl[i].split('=');
            
            if( key[0] == args['key'] )
            {
                currUrl.splice( i, 1 );
            }
        }
        retVal = '?' + currUrl.join('&');
    }
    return retVal;
}

function serializeArray( array )
{
    arrData = {};
    jQuery.map(array, function(n, i){
        arrData[n['name']] = n['value'];
    });
    return arrData;
}

function checkRequired(block) {
    var items = block.size();
    var i = 0;
    block.each(function() {
        var val = jQuery.trim(jQuery(this).val());
        var id = jQuery(this).attr("id");
        var type = jQuery(this).attr("type");
        var placeholder = jQuery(this).attr("placeholder");
        if (val.length == 0) {
            alert( "���������� �������� ���� "+placeholder );
            jQuery(this).focus();
            return false
        }
        else {
            i++
        }
    });
    if (items == i) {
        return true
    } else {
        return false
    }
}

function parseURL(url) {
    var a =  document.createElement('a');
    a.href = url;
    return {
        source: url,
        protocol: a.protocol.replace(':',''),
        host: a.hostname,
        port: a.port,
        query: a.search,
        params: (function(){
            var ret = {},
                seg = a.search.replace(/^\?/,'').split('&'),
                len = seg.length, i = 0, s;
            for (;i<len;i++) {
                if (!seg[i]) { continue; }
                s = seg[i].split('=');
                ret[s[0]] = s[1];
            }
            return ret;
        })(),
        file: (a.pathname.match(/\/([^\/?#]+)$/i) || [,''])[1],
        hash: a.hash.replace('#',''),
        path: a.pathname.replace(/^([^\/])/,'/$1'),
        relative: (a.href.match(/tps?:\/\/[^\/]+(.+)/) || [,''])[1],
        segments: a.pathname.replace(/^\//,'').split('/')
    };
}
/*
var myURL = parseURL('http://abc.com:8080/dir/index.html?id=255&m=hello#top');
 
myURL.file;     // = 'index.html'
myURL.hash;     // = 'top'
myURL.host;     // = 'abc.com'
myURL.query;    // = '?id=255&m=hello'
myURL.params;   // = Object = { id: 255, m: hello }
myURL.path;     // = '/dir/index.html'
myURL.segments; // = Array = ['dir', 'index.html']
myURL.port;     // = '8080'
myURL.protocol; // = 'http'
myURL.source;   // = 'http://abc.com:8080/dir/index.html?id=255&m=hello#top'
*/