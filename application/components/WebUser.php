<?php

class WebUser extends CWebUser
{
    private $model;
    
    public function getRole()
    {
        $role = 'guest';
        
        if ($user = $this->getModel()) 
        {
            $role = $user->roles->name;
        }
        
        return $role;
    }
    
    public function getRoles()
    {
        $result = array();
        if ($user = $this->getModel()) 
        {
            $result = $user->roles;
        }
        return $result;
    }
    
    public function getDept()
    {
        $result = null;
        if ($user = $this->getModel()) 
        {
            $result = $user->roles->dept;
        }
        return $result;
    }
    
    public function getProfile()
    {
        $profile = null;
        
        if ($user = $this->getModel()) 
        {
            $profile = $user;
        }

        return $profile;
    }
    
    public function getEmail()
    {
        $email = null;
        
        if ($user = $this->getModel()) 
        {
            $email = $user->email;
        }
        
        return $email;
    }
    
    public function getUserID()
    {
        $userID = 0;
        if( $user = $this->getModel() )
        {
            $userID = $user->id;
        }
        return $userID;
    }
    
    public function getBaseID()
    {
        $baseID = 0;
        if( $user = $this->getModel() )
        {
            $baseID = $user->id_base;
        }
        return $baseID;
    }
    
    public function getFullName()
    {
    
	    if ($user = $this->getModel()) 
	    {
	    	return $user->fullName;
	    }
    }
    
    private function getModel()
    {
        if (!$this->isGuest && null === $this->model) 
        {

            $id = Yii::app()->user->getId();

            $this->model = Users::model()
                ->cache(60, null, 3)
                ->findByPk($id);

        } 
    
        return $this->model;
        
    }
}