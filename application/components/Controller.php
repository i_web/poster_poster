<?php

class Controller extends CController {
    public  $layout	   = 'application.views.layouts.main';
    public  $menu=array();
    public  $breadcrumbs=array();
    public  $assets = array();
    public  $baseData = array();
    public  $baseID = 0;
    public  $userID = 0;
    public  $async;
    public  $uploadDir = '/home/web/poster.dir/upload.poster.web/';
    public  $upload_dir = '/home/web/poster.dir/upload.poster.web/';
    public  $storageDir = '/home/web/poster.dir/storage.poster.web/';
    public  $storage;
    
    public function init()
    {
        $this->assets["path"] = Yii::app()->getAssetManager()->publish( Yii::getPathOfAlias('application.views.assets') );
        $this->userID = Yii::app()->user->userID;
        $this->baseID = Yii::app()->user->baseID;
        $this->baseData = Bases::model()->findByPk( $this->baseID );
        $this->async = Yii::app()->request->isAjaxRequest ? true : false;
        $this->storage = Yii::app()->storage;
    }
    
    public function filters()
    {
        return array(
            'accessControl',
        );
    }
    
    public function accessRules()
	{ 
		return array
		(
            array('deny', 'actions' => array('login'), 'users' => array('@')),
            array('allow', 'actions' => array('login'), 'users' => array('?')),
			array('allow', 'users' => array('@')),
			array('deny', 'users' => array('*')),
		);
	}
    
    public function vk_api( $method, $formData = array() )
    {
        require_once('./application/vendors/autoload.php');
        
        $system = array();
        
        $user_params = UsersSocial::model()->id_user_social( 4803013 )->find();
        
        if( $user_params ) {
            $system[ "access_token" ] = $user_params->access_token;
            $system[ "v" ] = "5.52";
            
            if( strlen( trim( $user_params->captcha_key ) ) > 0 && strlen( trim( $user_params->captcha_sid ) ) > 0 )
            {
                $system[ "captcha_key" ] = $user_params->captcha_key;
                $system[ "captcha_sid" ] = $user_params->captcha_sid;
            }
            $client = new \GuzzleHttp\Client([
                "timeout" => 30,
                "base_uri" => "https://api.vk.com/method/",
                "headers" => [ "content-type" => "application/json", "Accept" => "applicatipon/json", "charset" => "utf-8"]
            ]);
            
            $request = $client->post( $method, array(
                "query" => $system,
                "form_params" => $formData
            ));
            $response = $request->getBody()->getContents();
            $response = json_decode( $response, true );
            return $response;
        }
    }
    
    public function toOneArray( $array, $column )
    {
        $result = array();
        if( count( $array ) )
        {
            foreach( $array as $key => $val )
            {
                if( isset( $val[ $column ] ) )
                {
                    $result[] = $val[ $column ];
                    //array_push( $result, $val[ $column ] );
                }
            }
        }
        return count( $result ) ? $result : false;
    }
    
    function daysRange( $f, $t )
    {
        $format = "d.m.Y";
        $day = 86400;
        $fDay = strtotime( $f );
        $tDay = strtotime( $t );
        $numDays = round(($tDay - $fDay) / $day);
        $days = array();

        for ($i = 0; $i < $numDays + 1; $i++) { 
            $days[] = date($format, ($fDay + ($i * $day)));
        }
        return $days;
    }
    
    function getAssets( $file, $assets = false, $array = array() )
    {
        $path = $assets ? $this->assets['path'] : false;
        
        if( is_array( $file ) )
        {
            foreach( $file as $v )
            {
                $array[] = $this->getAssets( $v, $assets );
            }
            return implode( "\n", $array );
        }
        if( is_string( $file ) )
        {
            if( file_exists( $_SERVER['DOCUMENT_ROOT'].$path.$file ) )
            {
                $v = filemtime( $_SERVER['DOCUMENT_ROOT'].$path.$file );
                $ext = explode( ".", $file );
                $ext = end( $ext );
                switch( $ext )
                {
                    case "js":
                    return '<script type="text/javascript" src="'.$path.$file.'?v='.$v.'"></script>';
                    break;
                    
                    case "css":
                    return '<link rel="stylesheet" type="text/css" href="'.$path.$file.'?v='.$v.'" />';
                    break;
                }
            }
        }
    }
    
    function FileSizeConvert($bytes)
    {
        $bytes = floatval($bytes);
            $arBytes = array(
                0 => array(
                    "UNIT" => "TB",
                    "VALUE" => pow(1024, 4)
                ),
                1 => array(
                    "UNIT" => "GB",
                    "VALUE" => pow(1024, 3)
                ),
                2 => array(
                    "UNIT" => "MB",
                    "VALUE" => pow(1024, 2)
                ),
                3 => array(
                    "UNIT" => "KB",
                    "VALUE" => 1024
                ),
                4 => array(
                    "UNIT" => "B",
                    "VALUE" => 1
                ),
            );
    
        foreach($arBytes as $arItem)
        {
            if($bytes >= $arItem["VALUE"])
            {
                $result = $bytes / $arItem["VALUE"];
                $result = str_replace(".", "," , strval(round($result, 2)))." ".$arItem["UNIT"];
                break;
            }
            else
            {
                $result = 0;
            }
        }
        return $result;
    }
    
    function getPagination($colrows, $numrow, $page)
    {
        $arr = array();
        if ($colrows != 0)
        {
            if ($colrows == $numrow)
            {
                $colrows = 1;
            }
            $colrows = ceil($colrows/$numrow);
        }
        if ($colrows>1)
        {
            $arr = self::getArrPagination($page, $colrows); 
        }
        return $arr;
    }
    
    public function getArrPagination($page, $colrows, $showPages = 5)
    {
        $left = (($colrows - $page) < $showPages) ? ($colrows - $showPages) : $page - ceil($showPages/2);
        
        $start = $left;
        
        $arr = array();
        
        $i = 0;
        $thisPage = 0;
        
        if ($left > 0)
		{
  		  if(($left-1) == 0)
          {
            array_push($arr, array('label' => '&laquo;', 'url' => 0));
          }
          else
          {
            array_push($arr, array('label' => '&laquo;', 'url' => ($left-1)));
          }
		}
        
        while($thisPage < $colrows && $thisPage < $showPages)
		{
			if ($start >= 0)
			{
				$s = $start + 1;
				if ($start == $page)
				{
				    array_push($arr, array('label' => $s, 'url' => 0, 'active' => true));
				}
                elseif($start == 0)
                {
                    array_push($arr, array('label' => $s, 'url' => 0));
                }
				else
				{
				    array_push($arr, array('label' => $s, 'url' => $start));
				}
				$thisPage++;
			}
			$start++;
		}
        
        if ($start < $colrows)
		{
			array_push($arr, array('label' => '&raquo;', 'url' => ($start)));
		}
        
		return $arr;
    }
    
}