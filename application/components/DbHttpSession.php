<?php

class DbHttpSession extends CDbHttpSession {
    
    protected function createSessionTable($db, $tableName)
    {
        
    }
    
    protected function transferData( $id_user, $last_activity)
    {
        $model = Users::model()->findByPk( $id_user );
        if( $model )
        {
            //$model->last_activity = $last_activity;
            //$model->save();
        }
        return true;
    }
    
    protected function clearOldSessions()
    {
        $model = UsersSessions::model()->findAll( array( "condition" => "expire < :time", "params" => array( ":time" => time() ) ) );
        if( $model )
        {
            foreach( $model as $k => $v )
            {
                $id = $v->id;
                $id_user = $v->id_user;
                $last_activity = $v->last_activity;
                $last_ip = $v->last_ip;
                $user = Users::model()->findByPk( $id_user );
                if( $user )
                {
                    //$user->last_activity = $last_activity;
                    //$user->last_ip = $last_ip;
                    //$user->save();
                }
                $v->delete();
            }
        }
    }
    
    public function openSession($savePath, $sessionName)
    {
        //$db = $this->getDbConnection();
        //$db->setActive(true);
        $this->clearOldSessions();
        return true;
    }
    
    public function writeSession($id, $data)
    {
        try {
            $expire = time() + $this->getTimeout();
            $db = $this->getDbConnection();
            if ($db->getDriverName() == 'sqlsrv' || $db->getDriverName() == 'mssql'
                || $db->getDriverName() == 'dblib'
            ) {
                $data = new CDbExpression('CONVERT(VARBINARY(MAX), ' . $db->quoteValue($data) . ')');
            }
            if ($db->createCommand()->select('id')->from($this->sessionTableName)->where('id=:id', array(':id' => $id))
                    ->queryScalar() === false
            ) {
                //Add needed fields to the queries
                $db->createCommand()->insert(
                    $this->sessionTableName, array(
                        'id'            => $id,
                        'data'          => $data,
                        'expire'        => $expire,
                        'id_user'       => 0,
                        //'last_activity' => new CDbExpression('NOW()'),
                        'last_activity' => time(),
                        'last_ip'       => CHttpRequest::getUserHostAddress(),
                    )
                );
            } else {
                $db->createCommand()->update(
                    $this->sessionTableName, array(
                        'data'          => $data,
                        'expire'        => $expire,
                        'id_user'       => Yii::app()->getUser()->getId(),
                        //'last_activity' => new CDbExpression('NOW()'),
                        'last_activity' => time(),
                        'last_ip'       => CHttpRequest::getUserHostAddress(),
                    ), 'id=:id', array(':id' => $id)
                );
            }
        } catch (Exception $e) {
            $this->createSessionTable($db, $this->sessionTableName);
            if (YII_DEBUG) {
                echo $e->getMessage();
            }
            return false;
        }
        return true;
    }
    
    public function destroySession($id)
    {
        $model = UsersSessions::model()->find( array( "condition" => "id = :id", "params" => array( ":id" => $id ) ) );
        if( $model )
        {
            $this->transferData( $model->id_user, $model->last_activity);
            $model->delete();
        }
    }
    public function gcSession($maxLifetime)
    {
        $this->clearOldSessions();
        return true;
    }


}