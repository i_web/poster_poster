<?php

class UserIdentity extends CUserIdentity {
    
    private $_id;
    
	public function authenticate()
	{
		$users = Users::model()->find( "login = :login AND password = :password", array( ":login" => $this->username, "password" => md5($this->password) ) );
        if( $users )
        {
            $this->_id = $users->id;
            $this->setState('login', $users->login);
            $this->errorCode = self::ERROR_NONE;
        }
        else
        {
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        }
        return !$this->errorCode;
	}
    
    public function getId()
    {
        return $this->_id;
    }
}