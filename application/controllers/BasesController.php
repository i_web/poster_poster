<?php

class BasesController extends Controller {
    
    public function actionAsync()
    {
        if( $this->async )
        {
            $errors = array();
            $result = array();
            switch( $_POST[ "action" ] )
            {
                case "bases.groups.get":
                $model[ "groups" ] = UsersGroups::model()->userID( $this->userID )->findAll();
                $result[ "tpl" ] = $this->renderPartial( "groups", array( "model" => $model ), true);
                break;
                
                case "create::save":
                unset( $_POST[ "userData" ][ "itemID" ] );
                $model = new Bases;
                $model->attributes = $_POST[ "userData" ];
                $model->id_user = $this->userID;
                $model->created = time();
                if( $model->save() )
                {
                    $result[ "success" ] = "Данные успешно сохранены";
                }
                else
                {
                    $errors[] = "Во время сохранения произошли ошибки";
                    $errors[] = $model->getErrors();
                }
                break;
                
                case "create::get":
                $model = new Bases;
                if( $model )
                {
                    $result[ "tpl" ] = $this->renderPartial( "edit", array( "model" => $model ), true);
                }
                else
                {
                    $errors[] = "Приложение не найдено";
                }
                break;
                
                case "edit::save":
                $model = Bases::model()->findByPk( $_POST[ "userData" ][ "itemID" ] );
                if( $model )
                {
                    unset( $_POST[ "userData" ][ "itemID" ] );
                    $model->attributes = $_POST[ "userData" ];
                    if( $model->save() )
                    {
                        $result[ "success" ] = "Данные успешно сохранены";
                    }
                    else
                    {
                        $errors[] = "Во время сохранения произошли ошибки";
                        $errors[] = $model->getErrors();
                    }
                }
                else
                {
                    $errors[] = "Данные не найдены";
                }
                break;
                
                case "edit::get":
                $model = Bases::model()->findByPk( $_POST[ "itemID" ] );
                if( $model )
                {
                    $result[ "tpl" ] = $this->renderPartial( "edit", array( "model" => $model ), true);
                }
                else
                {
                    $errors[] = "Приложение не найдено";
                }
                break;
            }
            echo json_encode( array( "errors" => $errors, "result" => $result ), JSON_UNESCAPED_UNICODE);
        }
    }
    
    public function actionAjax()
    {
        $error = false;
        $response = "noMethodSelected";
        if( $this->async )
        {
            switch( $_POST["method"] )
            {
                case "setActive":
                $model = Users::model()->findByPk( $this->userID );
                if( $model )
                {
                    $model->id_base = $_POST["baseID"];
                    if( $model->save() )
                    {
                        $error = false;
                        $response = "База успешно изменена";
                    }
                    else
                    {
                        $error = true;
                        $response = "Ошибка сохранения";
                    }
                }
                else
                {
                    $error = true;
                    $response = "Ошибка выборки";
                }
                break;
            }
        }
        echo json_encode(array( "error" => $error, "response" => $response ), JSON_UNESCAPED_UNICODE);
    }
    
    public function actionList()
    {
        $model = Bases::model()->findAll( array( "order" => "t.id ASC" ) );
        if( $this->async )
        {
            echo json_encode($this->renderPartial("list", array( "model" => $model ), true), JSON_UNESCAPED_UNICODE);
        }
        else
        {
            $this->render("list", array( "model" => $model ));
        }
    }
    
    public function actionCreate()
    {
        $model = new Bases;
        if( isset( $_POST['Bases'] ) )
        {
            $model->created = time();
            $model->id_user = $this->userID;
            $model->attributes = $_POST['Bases'];
            if( $model->save() )
            {
                $this->redirect( '/'.Yii::app()->controller->id.'/list/' );
            }
        }
        $this->render("edit", array( "model" => $model ));
    }
    
    public function actionEdit()
    {
        $model = Bases::model()->findByPk( $_GET['id'] );
        if( $model )
        {
            if( isset( $_POST['Bases'] ) )
            {
                $model->attributes = $_POST['Bases'];
                if( $model->save() )
                {
                    $this->redirect( '/'.Yii::app()->controller->id.'/list/' );
                }
            }
        }
        $this->render("edit", array( "model" => $model ) );
    }
}