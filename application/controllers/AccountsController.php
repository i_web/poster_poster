<?php

class AccountsController extends Controller {
    
    public function actionList()
    {
        $model = Accounts::model()->findAll( array( "order" => "t.created DESC" ) );
        if( $this->async )
        {
            echo json_encode($this->renderPartial( "list", array( "model" => $model ), true), JSON_UNESCAPED_UNICODE);
        }
        else
        {
            $this->render("list", array( "model" => $model ));
        }
    }
}