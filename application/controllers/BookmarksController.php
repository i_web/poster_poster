<?php

class BookmarksController extends Controller {
    
    public function actionList()
    {
        $model = BasesBookmarks::model()->baseID( $this->baseID )->findAll( array( "order" => "t.created DESC" ) );
        if( $this->async )
        {
            echo json_encode($this->renderPartial("list", array( "model" => $model ), true), JSON_UNESCAPED_UNICODE);
        }
        else
        {
            $this->render("list", array( "model" => $model ));
        }
    }
    
    public function actionView()
    {
        $pages = false;
        $page = isset( $_GET[ "page" ] ) ? $_GET[ "page" ] : 1;
        $page = $page - 1;
        $numrow = 50;
        
        $model = $this->vk_api( "wall.get", array(
            "owner_id" => -1 * $_GET[ "id" ],
            "count" => $numrow,
            "offset" => $page * $numrow,
            "extended" => 1,
            "fields" => "counters"
        ));
        
        if( $model ) {
            $pages = new CPagination( $model[ "response" ][ "count" ] );
            $pages->pageSize = $numrow;
        }
        
        if( $this->async ) {
            echo json_encode($this->renderPartial( "view", array( "model" => $model, "pages" => $pages ), true), JSON_UNESCAPED_UNICODE);
        }
        else {
            $this->render( "view", array( "model" => $model, "pages" => $pages ));
        }
    }
}