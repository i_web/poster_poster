<?php

class CronController extends CController {
    
    public function actionRun()
    {
        echo "<pre>";
        $criteria = new CDbCriteria;
        $criteria->with = array( "items" );
        $criteria->addCondition( "t.active = 1" );
        $criteria->addCondition( "DATE_FORMAT(FROM_UNIXTIME(t.runTime),'%Y-%m-%d %H:%i') <= :runTime" );
        $criteria->params = array( ":runTime" => date( "Y-m-d H:i", time() ) );
        $criteria->limit = 1;
        $model = TaskItems::model()->findAll( $criteria );
        if( $model )
        {
            foreach( $model as $key => $val )
            {
                Social::vk()->export( $val );
            }
        }
        echo "</pre>";
    }
    
    public function actionRunForce()
    {
        if( isset( $_GET["baseID"] ) && isset( $_GET["taskID"] ) && isset( $_GET["itemID"] ) )
        {
            $criteria = new CDbCriteria;
            $criteria->with = array( "items" );
            $criteria->addCondition( "t.id_base = :baseID AND t.id_task = :taskID AND t.id_item = :itemID" );
            $criteria->params = array( ":baseID" => $_GET[ "baseID" ], ":taskID" => $_GET[ "taskID" ], ":itemID" => $_GET[ "itemID" ] );
            $criteria->limit = 1;
            $model = TaskItems::model()->findAll( $criteria );
            if( $model )
            {
                foreach( $model as $key => $val )
                {
                    Social::vk()->export( $val );
                }
            }
        }
    }
}