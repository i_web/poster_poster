<?php

class ApplicationsController extends Controller {
    
    public function actionAsync()
    {
        if( $this->async )
        {
            $errors = array();
            $result = array();
            switch( $_POST[ "action" ] )
            {
                case "save":
                $model = Applications::model()->findByPk( $_POST[ "userData" ][ "itemID" ] );
                if( $model )
                {
                    unset( $_POST[ "userData" ][ "itemID" ] );
                    $model->attributes = $_POST[ "userData" ];
                    if( $model->save() )
                    {
                        $result[ "success" ] = "Приложение успешно сохранено";
                    }
                    else
                    {
                        $errors[] = "Во время сохранения произошли ошибки";
                        $errors[] = $model->getErrors();
                    }
                }
                else
                {
                    $errors[] = "Приложение не найдено";
                }
                break;
                
                case "create":
                $model = new Applications;
                if( $model )
                {
                    $result[ "tpl" ] = $this->renderPartial( "edit", array( "model" => $model ), true);
                }
                else
                {
                    $errors[] = "Приложение не найдено";
                }
                break;
                
                case "edit":
                $model = Applications::model()->findByPk( $_POST[ "itemID" ] );
                if( $model )
                {
                    $result[ "tpl" ] = $this->renderPartial( "edit", array( "model" => $model ), true);
                }
                else
                {
                    $errors[] = "Приложение не найдено";
                }
                break;
                
                case "delete":
                $model = Applications::model()->findByPk( $_POST[ "itemID" ] );
                if( $model )
                {
                    if( $model->delete() )
                    {
                        $result[ "success" ] = "Приложение успешно удалено";
                    }
                    else
                    {
                        $errors[ "error_text" ] = "Во время удаления произошли ошибки";
                        $errors[ "error_code" ] = $model->getErrors();
                    }
                }
                else
                {
                    $errors[] = "Приложение не найдено";
                }
                break;
            }
            echo json_encode( array( "errors" => $errors, "result" => $result ), JSON_UNESCAPED_UNICODE);
        }
    }
    
    public function actionList()
    {
        $model = Applications::model()->findAll( array( "order" => "t.created DESC" ) );
        if( $this->async )
        {
            echo json_encode($this->renderPartial("list", array( "model" => $model ), true), JSON_UNESCAPED_UNICODE);
        }
        else
        {
            $this->render("list", array( "model" => $model ));
        }
    }
    
    public function actionEdit()
    {
        $model = Applications::model()->findByPk( $_GET[ "id" ] );
        if( $this->async )
        {
            echo json_encode($this->renderPartial("edit", array( "model" => $model ), true), JSON_UNESCAPED_UNICODE);
        }
        else
        {
            $this->render("edit", array( "model" => $model ));
        }
    }
    
    public function actionCreate()
    {
        $model = new Applications;
        if( isset( $_POST['Applications'] ) )
        {
            $model->created = time();
            $model->attributes = $_POST['Applications'];
            if( $model->save() )
            {
                $this->redirect( '/'.Yii::app()->controller->id.'/list/' );
            }
        }
        $this->render("edit", array( "model" => $model ));
    }
}