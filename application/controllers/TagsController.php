<?php

class TagsController extends Controller {
    
    public function actionEdit()
    {
        $model = Tags::model()->findByPk( $_POST[ "itemID" ] );
        $tagsList = TagsSystem::getList( $this->baseID );
        if( $this->async ) {
            echo json_encode($this->renderPartial("edit", array( "model" => $model, "tagsList" => $tagsList ), true), JSON_UNESCAPED_UNICODE);
        }
        else {
            $this->render("edit", array( "model" => $model, "tagsList" => $tagsList ));
        }
    }
    
    public function actionCreate()
    {
        $model = new Tags;
        $tagsList = TagsSystem::getList( $this->baseID );
        if( $this->async ) {
            echo json_encode($this->renderPartial("create", array( "model" => $model, "tagsList" => $tagsList ), true), JSON_UNESCAPED_UNICODE);
        }
        else {
            $this->render("create", array( "model" => $model, "tagsList" => $tagsList ));
        }
    }
    
    public function actionSave()
    {
        if( $this->async ) {
            $errors = array();
            $result = array();
            
            $userData = $_POST[ "userData" ];
            
            if( $userData[ "itemID" ] > 0 ) {
                $model = Tags::model()->findByPk( $userData[ "itemID" ] );
                $model_parent = Tags::model()->findByPk( $userData[ "parentID" ] );
                if( $model ) {
                    $model->name = $userData[ "name" ];
                    $model->id_parent = $userData[ "parentID" ] > 0 ? $userData[ "parentID" ] : 0;
                    $model->clevel = $model_parent ? $model_parent->clevel + 1 : 0;
                    if( $model->save() ) {
                        $result[] = "Данные успешно сохранены";
                    }
                    else {
                        $errors['log'] = "Во время сохранения произошли ошибки";
                        $errors['code'] = $model->getErrors();
                    }
                }
                else {
                    $errors[] = "Тэг не найден в системе";
                }
            }
            else {
                $model = new Tags;
                $model_parent = Tags::model()->findByPk( $userData[ "parentID" ] );
                $model->id_base = $this->baseID;
                $model->created = time();
                $model->name = $userData[ "name" ];
                $model->id_parent = $userData[ "parentID" ] > 0 ? $userData[ "parentID" ] : 0;
                $model->clevel = $model_parent ? $model_parent->clevel + 1 : 0;
                if( $model->save() ) {
                    $result[] = "Данные успешно сохранены";
                }
                else {
                    $errors['log'] = "Во время сохранения произошли ошибки";
                    $errors['code'] = $model->getErrors();
                }
            }
            echo json_encode( array( "errors" => $errors, "result" => $result ), JSON_UNESCAPED_UNICODE);
        }
    }
    
    public function actionList()
    {
        $model = Tags::listData( $this->baseID );
        if( $this->async )
        {
            echo json_encode($this->renderPartial("list", array( "model" => $model ), true), JSON_UNESCAPED_UNICODE);
        }
        else
        {
            $this->render("list", array( "model" => $model ));
        }
    }
    
    public function actionAsync()
    {
        if( $this->async )
        {
            $errors = array();
            $result = array();
            
            echo json_encode( array( "errors" => $errors, "result" => $result ), JSON_UNESCAPED_UNICODE);
        }
    }
}