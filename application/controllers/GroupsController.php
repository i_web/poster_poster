<?php

class GroupsController extends Controller {
    
    public function actionList()
    {
        $model = false;
        if( $this->async )
        {
            echo json_encode($this->renderPartial("list", array( "model" => $model ), true), JSON_UNESCAPED_UNICODE);
        }
        else
        {
            $this->render("list", array( "model" => $model ));
        }
    }
}