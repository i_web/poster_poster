<?php

class AuthController extends Controller {
    
    public function actionIndex()
	{
		$this->render('index');
	}
    
    public function actionLogin()
    {
        if( Yii::app()->request->isAjaxRequest )
        {
            $model = new LoginForm;
            $model->username = $_POST['userData']['login'];
            $model->password = $_POST['userData']['password'];
            $model->rememberMe = 1;
            if( $model->validate() && $model->login() )
            {
                $array = array( "error" => false, "response" => Yii::app()->user->returnUrl );
            }
            else
            {
                $array = array( "error" => true, "response" => "Error login" );
            }
            echo json_encode($array, JSON_UNESCAPED_UNICODE);
        }
        else
        {
            $this->render('login');
        }
    }
    public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
}