<?php

class TestController extends Controller {
    
    public function actionStorage()
    {
        $this->storage->get();
    }
    
    public function actionFile()
    {
        echo "file";
    }
    
    public function actionTest()
    {
        echo "<pre>";
        $params = array(
            "baseID" => 1,
            "taskID" => 2539,
            "getIDS" => false,
        );
        var_dump( TaskItemsSystem::get( $params ) );
        /*
        $ids = [];
        $model = TaskItems::model()->baseID( 1 )->findAll();
        if( $model )
        {
            foreach( $model as $key => $val )
            {
                $ids[] = $val->id_item;
            }
            var_dump( $ids );
        }
        */
        echo "</pre>";
    }
    
    public function actionIndex()
    {
        echo "<pre>";
        //$ids = array();
        
        //$ids = ItemsHistorySystem::get_ids( 1, 1000 );
        
        $criteria = new CDbCriteria;
        $criteria->select = "t.id_tags, t.id_item, COUNT( t.id_tags ) AS totalTags";
        $criteria->addCondition( "t.id_base = :baseID" );
        $criteria->params = array( ":baseID" => 1 );
        
        $criteria->group = "t.id_tags";
        $criteria->having = "COUNT(t.id_tags) > 1";
        $criteria->order = "t.id_tags ASC";
        $model = TagsItems::model()->findAll( $criteria );
        if( $model ) {
            foreach( $model as $k => $v ) {
                echo $v->id_tags . " " . $v->totalTags . "<br />";
            }
        }
        echo "</pre>";
    }
}