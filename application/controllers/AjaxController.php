<?php

class AjaxController extends Controller {
    
    public function actionIndex()
    {
        $dirs = glob(Yii::getPathOfAlias('webroot.assets') . DIRECTORY_SEPARATOR . '*', GLOB_ONLYDIR);
        foreach( $dirs as $value )
        {
            $this->rmDir( $value );
        }
        return true;
    }
    
    function rmDir( $path )
    {
        static $doNotRemoveBaseDirectory = false, $baseDirectory;
        $path = trim($path);
        if (substr($path, -1) == '*') {
            $doNotRemoveBaseDirectory = true;
            $path = substr($path, 0, -1);
        }
        if (substr($path, -1) == '/') {
            $path = substr($path, 0, -1);
        }
        if ($doNotRemoveBaseDirectory) {
            $baseDirectory = $path;
        }
        if (is_dir($path)) {
            $dirHandle = opendir($path);
            while (false !== ($file = readdir($dirHandle))) {
                if ($file != '.' && $file != '..') {
                    $tmpPath = $path . '/' . $file;
                    if (is_dir($tmpPath)) {
                        self::rmDir($tmpPath);
                    } else {
                        if (file_exists($tmpPath)) {
                            unlink($tmpPath);
                        }
                    }
                }
            }
            closedir($dirHandle);
            // удаляем текущую папку
            if ($doNotRemoveBaseDirectory === true && $baseDirectory == $path) {
                return true;
            }
            return rmdir($path);
        } elseif (is_file($path) || is_link($path)) {
            return unlink($path);
        } else {
            return false;
        }
    }
}