<?php

class ItemsController extends Controller {
    
    public function actionAsync()
    {
        if( $this->async )
        {
            $errors = array();
            $result = array();
            
            switch( $_POST[ "action" ] )
            {
                case "import":
                $user = UsersSocial::model()->id_user_social( 4803013 )->find();
                
                $response = Api::vk_run( 'wall.getById', array(
                    "system" => array(
                        "access_token" => $user->access_token,
                        "v" => "5.41",
                        "captcha_key" => $user->captcha_key,
                        "captcha_sid" => $user->captcha_sid
                    ),
                    "formData" => array(
                        "posts" => "-" . $_POST[ "itemID" ]
                    ),
                ));
                
                if( isset( $response['response'][0] ) )
                {
                    $response = $response[ 'response' ][ 0 ];
                    
                    $text = trim( $response[ "text" ] );
                    $title = explode("\n", $text);
                    
                    $posts = new Items;
                    $posts->id_user = $this->userID;
                    $posts->id_base = $this->baseID;
                    $posts->title = trim( $title[0] );
                    $posts->text = trim( $text );
                    $posts->origin_url = "vk.com/wall" . $response[ "owner_id" ] . "_" . $response[ "id" ];
                    $posts->created = time();
                    if( $posts->save() )
                    {
                        $itemID = $posts->id;
                        $bases_items = new BasesItems;
                        $bases_items->id_base = $this->baseID;
                        $bases_items->id_item = $itemID;
                        $bases_items->save();
                        
                        if( isset( $response['attachments'] ) )
                        {
                            foreach( $response['attachments'] as $key => $file)
                            {
                                if( isset($file['photo']) || isset( $file['doc'] ) )
                                {
                                    $original_path = "null";
                                    if( $file[ "type" ] === "doc" )
                                    {
                                        $original_path = $file["doc"]["url"];
                                    }
                                    if( $file[ "type" ] === "photo" )
                                    {
                                        if( array_key_exists( "photo_2560", $file[ "photo" ] ) )
                                        {
                                            $original_path = $file['photo']['photo_2560'];
                                        }
                                        elseif( array_key_exists( "photo_1280", $file[ "photo" ] ) )
                                        {
                                            $original_path = $file['photo']['photo_1280'];
                                        }
                                        elseif( array_key_exists( "photo_807", $file[ "photo" ] ) )
                                        {
                                            $original_path = $file['photo']['photo_807'];
                                        }
                                        elseif( array_key_exists( "photo_604", $file[ "photo" ] ) )
                                        {
                                            $original_path = $file['photo']['photo_604'];
                                        }
                                        else
                                        {
                                            $original_path = "null";
                                        }
                                    }
                                    Yii::app()->db->CreateCommand()->insert( "m_items_images", array(
                                        "id_item" => $itemID,
                                        "id_base" => $this->baseID,
                                        "keySort" => $key,
                                        "original_path" => $original_path,
                                        "type" => ( $file["type"] === "doc" ? "doc" : ( $file["type"] === "photo" ? "photo" : "null" ) ),
                                        "ext" => ( $file["type"] === "doc" ? $file["doc"]["ext"] : ( $file["type"] === "photo" ? "null" : "null" ) )
                                    ));
                                }
                            }
                            ItemsImagesSystem::photo_upload( $itemID );
                        }
                        $result[ "itemID" ] = $itemID;
                    }
                    else
                    {
                        $errors[] = $posts->getErrors();
                    }
                }
                break;
                
                case "save":
                $model = Items::model()->findByPk( $_POST[ "userData" ][ "itemID" ] );
                if( $model )
                {
                    $model->title = $_POST[ "userData" ][ "title" ];
                    $model->text = $_POST[ "userData" ][ "text" ];
                    $model->origin_url = $_POST[ "userData" ][ "origin_url" ];
                    $model->origin_show = $_POST[ "userData" ][ "origin_show" ];
                    if( $model->save() )
                    {
                        $result[] = "Материал успешно сохранен";
                    }
                    else
                    {
                        $errors[] = "Во время сохранения материала произошла ошибка";
                        $errors[] = $model->getErrors();
                    }
                }
                else
                {
                    $errors = "Ошибка сохранения. Материал не найден";
                }
                break;
                
                case "delete":
                $model = Items::model()->with( "images", "tags", "history", "tasks", "b_items" )->findByPk( $_POST[ "itemID" ] );
                if( $model )
                {
                    if( $model->images )
                    {
                        foreach( $model->images as $val )
                        {
                            if( strlen( trim( $val->local_path ) ) > 0 )
                            {
                                if( file_exists( $this->upload_dir . $val->local_path ) )
                                {
                                    unlink( $this->upload_dir . $val->local_path );
                                }
                            }
                            $val->delete();
                        }
                    }
                    if( $model->tags )
                    {
                        foreach( $model->tags as $val )
                        {
                            $val->delete();
                        }
                    }
                    if( $model->history )
                    {
                        foreach( $model->history as $val )
                        {
                            $val->delete();
                        }
                    }
                    if( $model->tasks )
                    {
                        foreach( $model->tasks as $val )
                        {
                            $val->delete();
                        }
                    }
                    if( $model->b_items )
                    {
                        foreach( $model->b_items as $val )
                        {
                            $val->delete();
                        }
                    }
                    
                    if( $model->delete() )
                    {
                        $result[ 'text' ] = "Материал [ " . $_POST[ "itemID" ] . " ] успешно удален";
                    }
                    else
                    {
                        $errors[] = "Во время удаления произошли ошибки";
                    }
                }
                else
                {
                    $errors[] = "Материал не найден в базе";
                }
                break;
                
                case "images.save_order":
                if( count( $_POST[ "sortData" ] ) > 0 )
                {
                    foreach( $_POST[ "sortData" ] as $key => $photoID )
                    {
                        Yii::app()->db->CreateCommand()->update( "m_items_images", array(
                            "keySort" => $key
                        ), "id = :photoID AND id_item = :itemID AND id_base = :baseID", array(
                            ":photoID" => $photoID, ":itemID" => $_POST[ "itemID" ], ":baseID" => $_POST[ "baseID" ]
                        ));
                    }
                }
                else
                {
                    $errors[] = "Нет данных для сортировки";
                }
                break;
                
                case "images.upload":
                try {
                    ItemsImagesSystem::upload( $_POST[ "userData" ] );
                    $model = Items::model()->with( "images_l" )->findByPk( $_POST[ "userData" ][ "itemID" ] );
                    if( $model )
                    {
                        $result[ "tpl" ] = $this->renderPartial("//items/detail.images", array( "model" => $model ), true);
                    }
                }
                catch( exception $error )
                {
                    $this->error = true;
                    $this->response = $error->getMessage();
                }
                break;
                
                case "images.delete":
                $model = ItemsImages::model()->findByPk( $_POST[ "fileID" ] );
                if( $model )
                {
                    if( strlen( trim( $model->local_path ) ) > 0 )
                    {
                        if( file_exists( $this->upload_dir . $model->local_path ) )
                        {
                            unlink( $this->upload_dir . $model->local_path );
                        }
                    }
                    if( $model->delete() )
                    {
                        $result[ "text" ] = "Файл успешно удален";
                    }
                }
                else
                {
                    $errors[] = "itemID не найден";
                }
                break;
                
                case "list.active":
                $model = Items::model()->findByPk( $_POST[ "itemID" ] );
                if( $model )
                {
                    $published = $model->published == 1 ? 0 : 1;
                    $model->published = $published;
                    if( $model->save() )
                    {
                        $result['active'] = $published;
                        $result['text'] = "Данные успешно обновлены";
                    }
                    else
                    {
                        $errors[] = "Система не смогла сохранить данные";
                        $errors[] = $model->getErrors();
                    }
                }
                else
                {
                    $errors[] = "Система не нашла материал";
                }
                break;
                
                case "tags.save":
                $model = TagsItems::model()->baseID( $_POST[ "baseID" ] )->itemID( $_POST[ "itemID" ] )->tagID( $_POST[ "tagID" ] )->find();
                if( $model ) {
                    $result[ "active" ] = 0;
                    $result[ "text" ] = "Успешно";
                    $model->delete();
                }
                else {
                    $model = new TagsItems;
                    $model->id_base = $_POST[ "baseID" ];
                    $model->id_item = $_POST[ "itemID" ];
                    $model->id_tags = $_POST[ "tagID" ];
                    if( $model->save() ) {
                        $result[ "active" ] = 1;
                        $result[ "text" ] = "Успешно";
                    }
                    else {
                        $errors[] = "Во время сохранения произошли ошибки";
                        $errors[] = $model->getErrors();
                    }
                }
                break;
            }
            echo json_encode( array( "errors" => $errors, "result" => $result ), JSON_UNESCAPED_UNICODE);
        }
    }
    
    public function actionDetail()
    {
        if( $this->async )
        {
            if( isset( $_POST['itemID'] ) )
            {
                $model = Items::model()->with('images_l','tags_l','history_l')->findByPk( $_POST['itemID'] );
                if( $model )
                {
                    echo json_encode($this->renderPartial("detail", array( "model" => $model ), true), JSON_UNESCAPED_UNICODE);
                }
            }
        }
    }
    
    public function actionList()
    {
        $page = isset( $_GET["page"] ) ? $_GET["page"] : 0;
        $numRow = isset( $_GET["numrow"] ) ? $_GET["numrow"] : 50;
        $noTags = isset( $_GET["noTags"] ) ? $_GET["noTags"] : false;
        $stext = isset( $_GET["stext"] ) ? $_GET["stext"] : false;
        
        $criteria = new CdbCriteria();
        $criteria->order = "t.id DESC";
        
        if( $stext )
        {
            if( strstr( $stext, "title:" ) )
            {
                $criteria->addCondition( "t.title LIKE '%".str_replace( "title:","", $stext )."%'" );
            }
            elseif( strstr( $stext, "itemID:" ) )
            {
                $criteria->addCondition( "t.id LIKE '%".str_replace( "itemID:","", $stext )."%'" );
            }
            else
            {
                $criteria->addCondition( "t.text LIKE '%".$stext."%'" );
            }
        }
        
        $allCount = Items::model()->baseID( $this->baseID )->count( $criteria );
        
        $pages = new CPagination( $allCount );
        $pages->pageSize = $numRow;
        $pages->applyLimit( $criteria );
                
        $model = Items::model()->baseID( $this->baseID )->findAll( $criteria );
        
        if( $this->async )
        {
            echo json_encode($this->renderPartial("list", array( "model" => $model, "pages" => $pages ), true), JSON_UNESCAPED_UNICODE);
        }
        else
        {
            $this->render("list", array( "model" => $model, "pages" => $pages ));
        }
    }
}