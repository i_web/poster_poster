<?php

class TasksController extends Controller {
    
    public function actionDetail()
    {
        $model = Task::model()->with("items")->baseID( $this->baseID )->taskID( $_GET["id"] )->find();
        if( $this->async )
        {
            echo json_encode($this->renderPartial("detail", array( "model" => $model ), true), JSON_UNESCAPED_UNICODE);
        }
        else
        {
            $this->render("detail", array( "model" => $model ) );
        }
    }
    
    public function actionAsync()
    {
        if( $this->async )
        {
            $errors = array();
            $result = array();
            switch( $_POST[ "action" ] )
            {
                case "generate":
                $task_system = new TaskPosts( $_POST[ "taskID" ] );
                $task_system->generate(true);
                break;
                
                case "delete":
                $model = Task::model()->baseID( $this->baseID )->taskID( $_POST[ "taskID" ] )->find();
                if( $model )
                {
                    if( $model->delete() )
                    {
                        $result[] = "Задача " . $model->id . " успешно удалена!";
                    }
                    else
                    {
                        $errors[] = "Не удалось удалить " . $_POST[ "taskID" ];
                    }
                }
                else
                {
                    $errors[] = "Задача " . $_POST[ "taskID" ] . " не найдена!";
                }
                break;
                
                case "clear":
                $model = TaskItems::model()->baseID( $this->baseID )->taskID( $_POST[ "taskID" ] )->findAll();
                if( $model )
                {
                    foreach( $model as $key => $val )
                    {
                        $val->delete();
                    }
                }
                else
                {
                    $errors[] = "В задаче не найден материал";
                }
                break;
                
                case "control":
                $model = Task::model()->baseID( $this->baseID )->taskID( $_POST[ "taskID" ] )->find();
                if( $model )
                {
                    $active = $_POST[ "active" ];
                    $model->active = $active;
                    if( $model->save() )
                    {
                        $result[] = "Задание успешно сохранено";
                    }
                    else
                    {
                        $errors[] = "Во время сохранения возникли ошибки";
                        $errors[] = $model->getErrors();
                    }
                }
                else
                {
                    $errors[] = "Задание не найдено";
                }
                break;
            }
            echo json_encode( array( "errors" => $errors, "result" => $result ), JSON_UNESCAPED_UNICODE);
        }
    }
    
    public function actionDelete()
    {
        if( $this->async )
        {
            $errors = array();
            $result = array();
            
            if( count( $_POST[ "arrData" ] ) > 0 )
            {
                foreach( $_POST[ "arrData" ] as $taksID )
                {
                    $model = Task::model()->baseID( $this->baseID )->taskID( $taksID )->find();
                    if( $model )
                    {
                        if( $model->delete() )
                        {
                            $result[] = $model->id;
                        }
                        else
                        {
                            $errors[] = "Не удалось удалить " . $taksID;
                        }
                    }
                }
            }
            else
            {
                $errors[] = "Ничего не выбрано";
            }
            
            echo json_encode( array( "errors" => $errors, "result" => $result ), JSON_UNESCAPED_UNICODE);
        }
    }
    
    public function actionList()
    {
        $model = Task::model()->baseID( $this->baseID )->findAll( array( "order" => "t.runTime ASC" ) );
        if( $this->async )
        {
            echo json_encode($this->renderPartial("list", array( "model" => $model ), true), JSON_UNESCAPED_UNICODE);
        }
        else
        {
            $this->render("list", array( "model" => $model ));
        }
    }
    
    public function actionCreate()
    {
        if( $this->async )
        {
            if( isset( $_POST['arrData'] ) )
            {
                $daysRange = explode( " - ", $_POST["arrData"]["runDay"] );
                $runTime = $_POST["arrData"]["runTime"];
                $daysRange = $this->daysRange( $daysRange[0], $daysRange[1] );
                if( count( $daysRange ) )
                {
                    foreach( $daysRange as $day )
                    {
                        $model = new Task;
                        $model->isNewRecord = true;
                        $model->id_base = $this->baseID;
                        $model->active = 0;
                        $model->runTime = strtotime( $day."T".$runTime );
                        $model->created = time();
                        $model->save();
                    }
                }
            }
            else
            {
                echo json_encode($this->renderPartial( "//tasks/create", array(  ), true), JSON_UNESCAPED_UNICODE);
            }
        }
        else
        {
            $this->render( "create" );
        }
    }
}