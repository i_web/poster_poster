<?php

class HistoryController extends Controller {
    
    public function actionList()
    {
        $page = isset( $_GET["page"] ) ? $_GET["page"] : 0;
        $numRow = isset( $_GET["numrow"] ) ? $_GET["numrow"] : 50;
        
        $criteria = new CdbCriteria();
        $criteria->order = "t.created DESC";
        
        $allCount = ItemsHistory::model()->baseID( $this->baseID )->count( $criteria );
        
        $pages = new CPagination( $allCount );
        $pages->pageSize = $numRow;
        $pages->applyLimit( $criteria );
        
        $model = ItemsHistory::model()->with('items','groups')->baseID( $this->baseID )->findAll( $criteria );
        
        if( $this->async )
        {
            echo json_encode($this->renderPartial("list", array( "model" => $model, "pages" => $pages ), true), JSON_UNESCAPED_UNICODE);
        }
        else
        {
            $this->render("list", array( "model" => $model, "pages" => $pages ));
        }
    }
}