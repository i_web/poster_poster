<?php

class LogsController extends Controller {
    
    public function actionIndex()
    {
        $file = "./application/runtime/application.log";
        if( isset( $_GET[ 'clean' ] ) )
        {
            file_put_contents( $file, "" );
        }
        $fileOpen = file_get_contents( $file );
        if( $fileOpen )
        {
            echo "<pre>";
            echo date( "H:i:s d-m-Y", filemtime( $file ) );
            echo "<br /><br />";
            echo $fileOpen;
            echo "</pre>";
        }
        else
        {
            echo "0";
        }
    }
}