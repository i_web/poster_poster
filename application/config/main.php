<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Панель Управления Постингом',
    'sourceLanguage' => 'ru',
    'language' => 'ru',
    'charset' => 'UTF-8',
    'timeZone' => 'Europe/Moscow',

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'1',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('*'),
		),
	),

	// application components
	'components'=>array(
        'storage' => array( 'class' => 'Storage' ),
        'viewRenderer'=>array(
          'class'=>'application.extensions.Smarty.ESmartyViewRenderer',
            'fileExtension' => '.tpl',
            //'pluginsDir' => 'application.smartyPlugins',
            //'configDir' => 'application.smartyConfig',
            //'prefilters' => array(array('MyClass','filterMethod')),
            //'postfilters' => array(),
            //'config'=>array(
            //    'force_compile' => YII_DEBUG,
            //   ... any Smarty object parameter
            //)
        ),
		'user'=>array(
			// enable cookie-based authentication
            'class' => 'WebUser',
			'allowAutoLogin' => true,
            'loginUrl' => array('/auth/login/'),
		),
        'session' => array(
            //'class' => 'CDbHttpSession',
            'class' => 'application.components.DbHttpSession',
            'autoCreateSessionTable' => true,
            'connectionID' => 'db',
            'timeout' => 3600,
            'sessionTableName' => 'm_users_sessions',
        ),
		// uncomment the following to enable URLs in path-format
        
		'urlManager'=>array(
            'urlFormat'=>'path',
            'showScriptName' => false,
			'rules'=>array(
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),

		// database settings are configured in database.php
		'db'=>require(dirname(__FILE__).'/database.php'),
        'authManager' 	   => array(
	        'class' 	   => 'PhpAuthManager',
	        'defaultRoles' => array('guest'),
	    ),
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>YII_DEBUG ? null : 'site/error',
		),

		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),

	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
        'site_url' => 'http://www.poster.web/',
		// this is used in contact page
        'upload_dir' => '/home/web/poster.dir/upload.poster.web/',
        'upload_url' => 'http://upload.poster.web/',
		'adminEmail'=>'webmaster@example.com',
        'script_version' => '20170324',
	),
);
