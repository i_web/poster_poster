<?php

function smarty_function_getClientScript($params = array())
{
    $params["path"] = isset( $params["path"] ) ? $params["path"] : false;
    $params["file"] = isset( $params["file"] ) ? $params["file"] : false;
    $params["vers"] = "?v=".filemtime( ".".$params["path"].$params["file"] );
    $params["media"] = isset( $params["media"] ) ? $params["media"] : "screen";
    $params["options"] = isset( $params["options"] ) ? $params["options"] : array();
    if( $params["file"] == false )
    {
        throw new CException("Function 'file' parameter should be specified.");
    }
    $ext = explode( ".", $params["file"] );
    $ext = end( $ext );
    $file = $params["path"].$params["file"].$params["vers"];
    switch( $ext )
    {
        case "js":
        $result =  Yii::app()->getClientScript()->registerScriptFile( $file );
        break;
        
        case "css":
        $result = Yii::app()->clientScript->registerLinkTag( "stylesheet", "text/css", $file, $params["media"], $params["options"] );
        break;
        return $result;
    }
}