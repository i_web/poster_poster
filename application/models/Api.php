<?php

class Api {
    public static function vk_run( $method, $params = array() )
    {
        require_once('./application/vendors/autoload.php');
        
        $client = new \GuzzleHttp\Client([
            "timeout" => 30,
            "base_uri" => "https://api.vk.com/method/",
            "headers" => [ "content-type" => "application/json", "Accept" => "applicatipon/json", "charset" => "utf-8"]
        ]);
        
        $request = $client->post( $method, array(
            "query" => $params[ "system" ],
            "form_params" => $params[ "formData" ]
        ));
        $response = $request->getBody()->getContents();
        $response = json_decode( $response, true );
        
        return $response;
    }
}