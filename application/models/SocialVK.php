<?php

ini_set('max_execution_time', 0);
ini_set('memory_limit', '2048M');

class SocialVK {
    
    public $uploadDir;
    public $itemID;
    public $groupID;
    public $baseID;
    public $accessToken;
    public $APIVersion = "5.52";
    public $sysName = "SocialVK";
    public $try = 5;
    public $try_default = 5;
    public $captcha_sid = false;
    public $captcha_key = false;
    
    public function __construct()
    {
        $this->uploadDir = Yii::app()->params["upload_dir"];
    }
    
    public function getTokenGroupID()
    {
        return Yii::app()->db->CreateCommand()
        ->select("mus.access_token")
        ->from("m_bases_groups mbg")
        ->join("m_users_social mus", "mus.id_user_social = mbg.id_user_social")
        ->where("mbg.id_group = :groupID", array( ":groupID" => $this->groupID ))
        ->queryScalar();
    }
    
    public function getUserParams()
    {
        return Yii::app()->db->CreateCommand()
        ->select("mus.*")
        ->from("m_bases_groups mbg")
        ->join("m_users_social mus", "mus.id_user_social = mbg.id_user_social")
        ->where("mbg.id_group = :groupID", array( ":groupID" => $this->groupID ))
        ->queryRow();
    }
    
    public function export( $postData = array() )
    {
        if( $postData )
        {
            $this->itemID = intval( $postData->id_item );
            $this->groupID = intval( $postData->id_social_group );
            $this->baseID = intval( $postData->id_base );
            
            $user_params = $this->getUserParams();
            
            $this->accessToken = $user_params['access_token'];
            if( strlen( trim( $user_params[ "captcha_sid" ] ) ) > 0 && strlen( trim( $user_params[ "captcha_key" ] ) ) > 0 )
            {
                $this->captcha_sid = $this->getUserParams()['captcha_sid'];
                $this->captcha_key = $this->getUserParams()['captcha_key'];
            }
            
            $baseData = Bases::model()->findByPk( $postData->id_base );
            if( strlen( trim( $baseData->tags ) ) > 3 )
            {
                $postData_text = $postData->items->text."\n".$baseData->tags;
            }
            else
            {
                $postData_text = $postData->items->text;
            }
            
            // ATTACHMENTS
            $imagesArray = array();
            if( $postData->items->images )
            {
                foreach( $postData->items->images as $file )
                {
                    if( $file->type == "photo" )
                    {
                        if( $file->url )
                        {
                            array_push( $imagesArray, $file->url->url );
                        }
                        else
                        {
                            $upload_url = $this->wallUploadPhoto( $this->uploadDir.$file["local_path"] );
                            if( $upload_url != false )
                            {
                                ItemsImagesSystem::set_url( $file->id, $file->id_base, $file->id_item, $upload_url );
                                array_push( $imagesArray, $upload_url );
                            }
                        }
                    }
                    if( $file->type == "doc" )
                    {
                        if( $file->url )
                        {
                            array_push( $imagesArray, $file->url->url );
                        }
                        else
                        {
                            $upload_url = $this->wallUploadDoc( $this->uploadDir.$file["local_path"] );
                            if( $upload_url != false )
                            {
                                ItemsImagesSystem::set_url( $file->id, $file->id_base, $file->id_item, $upload_url );
                                array_push( $imagesArray, $upload_url );
                            }
                        }
                    }
                }
            }
            // ATTACHMENTS
            
            $imagesArray = array_diff($imagesArray, array(''));
            
            if( count( $imagesArray ) == count( $postData->items->images ) )
            {
                $responseData = $this->wallPost( $postData_text, $imagesArray );
                
                if( isset( $responseData["response"]["post_id"] ) )
                {
                    Core::tasks_set_history( array(
                        "itemID" => $postData->id_item,
                        "itemIDSocial" => $responseData["response"]["post_id"],
                        "groupID" => $postData->id_social_group,
                        "baseID" => $postData->id_base,
                        "runTime" => $postData->runTime
                    ));
                    
                    Core::delete_task_post(array(
                        "baseID" => $postData->id_base,
                        "taskID" => $postData->id_task,
                        "itemID" => $postData->id_item
                    ));
                }
            }
        }
    }
    
    public function wallPost( $message, $attachments = null, $publish_date = null )
    {
        return $this->run_api('wall.post', array(
            'owner_id' => -1 * $this->groupID,
            'friends_only' => 0,
            'from_group' => 1,
            'message' => $message,
            'attachments' => $attachments,
            'publish_date' => $publish_date
        ));
    }
    
    public function wallUploadDoc( $file, $try = false )
    {
        if( $this->try > 0 )
        {
            if( $try )
            {
                $this->try--;
            }
            else
            {
                $this->try = $this->try_default;
            }
            $server = $this->run_api('docs.getWallUploadServer', array(
                'group_id' => $this->groupID
            ));
            
            if( isset($server['response']['upload_url']) )
            {
                $serverData = $this->upload( $server['response']['upload_url'], realpath( $file ) );
                
                $fileData = $this->run_api('docs.save', array(
                    'file' => $serverData['file']
                ));
                
                if( isset ($fileData['response']) )
                {
                    return "doc".$fileData['response'][0]['owner_id']."_".$fileData['response'][0]['id'];
                }
                else
                {
                    $this->wallUploadDoc( $file, true );
                }
            }
            else
            {
                $this->wallUploadDoc( $file, true );
            }
        }
        else
        {
            die;
        }
    }
    
    public function wallUploadPhoto( $file, $try = false )
    {
        if( $this->try > 0 )
        {
            if( $try )
            {
                $this->try--;
            }
            else
            {
                $this->try = $this->try_default;
            }
            $server = $this->run_api('photos.getWallUploadServer', array(
                'group_id' => $this->groupID
            ));
            
            if( isset($server['response']['upload_url']) )
            {
                $serverData = $this->upload( $server['response']['upload_url'], realpath( $file ) );
                
                if( isset( $serverData[ 'server' ] ) && isset( $serverData[ 'photo' ] ) && isset( $serverData[ 'hash' ] ) )
                {
                    $fileData = $this->run_api('photos.saveWallPhoto', array(
                        'group_id' => $this->groupID,
                        'server' => $serverData['server'],
                        'photo' => $serverData['photo'],
                        'hash' => $serverData['hash']
                    ));
                    
                    if( isset ($fileData['response']) )
                    {
                        return "photo".$fileData['response'][0]['owner_id']."_".$fileData['response'][0]['id'];
                    }
                    else
                    {
                        $this->wallUploadPhoto( $file, true );
                    }
                }
                else
                {
                    $this->wallUploadPhoto( $file, true );
                }
            }
            else
            {
                $this->wallUploadPhoto( $file, true );
            }
        }
        else
        {
            die;
        }
    }
    
    public function upload( $url, $file )
    {
        //sleep( 1 );
        usleep( 500000 );
        require_once('./application/vendors/autoload.php');
        $client = new \GuzzleHttp\Client([
            'timeout' => 30
        ]);
        $request = $client->request( "POST", $url, [
            'multipart' => [
            [
                'name' => 'file',
                'contents' => fopen( $file, 'r' ),
            ],
            ],
        ]);
        $response = $request->getBody()->getContents();
        $response = json_decode( $response, true );
        return $response;
    }
    
    public function run_api( $method, $formData = array() )
    {
        //sleep( 1 );
        usleep( 500000 );
        require_once('./application/vendors/autoload.php');
        
        $system = array();
        
        $system[ "access_token" ] = $this->accessToken;
        $system[ "v" ] = $this->APIVersion;
        
        if( $this->captcha_key == true && $this->captcha_sid == true )
        {
            $system[ "captcha_key" ] = $this->captcha_key;
            $system[ "captcha_sid" ] = $this->captcha_sid;
        }
        $client = new \GuzzleHttp\Client([
            "timeout" => 30,
            "base_uri" => "https://api.vk.com/method/",
            "headers" => [ "content-type" => "application/json", "Accept" => "applicatipon/json", "charset" => "utf-8"]
        ]);
        
        $request = $client->post( $method, array(
            "query" => $system,
            "form_params" => $formData
        ));
        $response = $request->getBody()->getContents();
        $response = json_decode( $response, true );
        
        if( isset( $response[ 'response' ] ) )
        {
            Core::setLogs( array(
                "itemID" => $this->itemID,
                "baseID" => $this->baseID,
                "groupID" => $this->groupID,
                "method" => $method,
                "params" => $formData,
                "type" => "success",
                "title" => "",
                "text" => $response
            ));
        }
        if( isset( $response[ 'error' ] ) )
        {
            Core::setLogs( array(
                "itemID" => $this->itemID,
                "baseID" => $this->baseID,
                "groupID" => $this->groupID,
                "method" => $method,
                "params" => $formData,
                "type" => "error",
                "title" => $response[ 'error' ][ 'error_msg' ],
                "text" => $response
            ));
        }
        return $response;
    }
}