<?php

/**
 * This is the model class for table "{{items_images_hash}}".
 *
 * The followings are the available columns in table '{{items_images_hash}}':
 * @property integer $id
 * @property integer $id_image
 * @property integer $id_item
 * @property integer $id_base
 * @property string $hashType
 * @property string $hashCode
 * @property integer $run
 */
class ItemsImagesHash extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{items_images_hash}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_image', 'required'),
			array('id_image, id_item, id_base, run', 'numerical', 'integerOnly'=>true),
			array('hashType', 'length', 'max'=>4),
			array('hashCode', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_image, id_item, id_base, hashType, hashCode, run', 'safe', 'on'=>'search'),
		);
	}
    
    public function scopes()
    {
        return array( 'baseID', 'fileID', 'itemID', 'hashCode' );
    }
    
    public function hashCode( $hashCode = array() )
    {
        $this->getDbCriteria()->mergeWith(array(
            'condition' => 't.hashCode IN (:hashCode)',
            'params' => array( ':hashCode' => implode( ",", $hashCode ) ),
            'group' => 't.id_item'
        ));
        return $this;
    }
    
    public function baseID( $baseID = 0 )
    {
        $this->getDbCriteria()->mergeWith(array(
            'condition' => 't.id_base = :baseID',
            'params' => array( ':baseID' => $baseID )
        ));
        return $this;
    }
    
    public function fileID( $fileID = 0 )
    {
        $this->getDbCriteria()->mergeWith(array(
            'condition' => 't.id_image = :fileID',
            'params' => array( ':fileID' => $fileID )
        ));
        return $this;
    }
    
    public function itemID( $itemID = 0, $pos = true )
    {
        $this->getDbCriteria()->mergeWith(array(
            'condition' => $pos ? 't.id_item = :itemID' : 't.id_item <> :itemID',
            'params' => array( ':itemID' => $itemID )
        ));
        return $this;
    }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'items' => array( self::BELONGS_TO, 'Items', array( 'id_item' => 'id' ) ),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
            'id' => 'ID',
			'id_image' => 'Id Image',
			'id_item' => 'Id Item',
			'id_base' => 'Id Base',
			'hashType' => 'Hash Type',
			'hashCode' => 'Hash Code',
			'run' => 'Run',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
        
        $criteria->compare('id',$this->id);
		$criteria->compare('id_image',$this->id_image);
		$criteria->compare('id_item',$this->id_item);
		$criteria->compare('id_base',$this->id_base);
		$criteria->compare('hashType',$this->hashType,true);
		$criteria->compare('hashCode',$this->hashCode,true);
		$criteria->compare('run',$this->run);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ItemsImagesHash the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
