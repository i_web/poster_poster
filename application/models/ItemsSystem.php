<?php

class ItemsSystem {
    
    public static function toOneArray( $array, $column )
    {
        $result = array();
        if( count( $array ) )
        {
            foreach( $array as $key => $val )
            {
                if( isset( $val[ $column ] ) )
                {
                    $result[] = $val[ $column ];
                }
            }
        }
        return count( $result ) ? $result : false;
    }
    
    public static function get_history_interval( $baseID )
    {
        $criteria = new CDbCriteria();
        $criteria->condition = "t.id_base = :baseID AND FROM_UNIXTIME( t.created) > ( NOW() - INTERVAL 30 DAY )";
        $criteria->group = "t.id_item";
        $criteria->order = "t.id_item ASC";
        $criteria->params = array( ":baseID" => $baseID );
        return ItemsHistory::model()->findAll( $criteria );
    }
    
    public static function get_task_items( $baseID )
    {
        return TaskItems::model()->baseID( $baseID )->findAll();
    }
    
    public static function generate( $baseID, $limit )
    {
        $criteria = new CDbCriteria();
        $criteria->condition = "t.id_base = :baseID AND t.published = 1";
        
        $hids = self::get_history_interval( $baseID );
        if( $hids )
        {
            $hids = self::toOneArray( $hids, "id_item" );
            $criteria->addCondition( "t.id NOT IN( ".implode( ",", $hids )." )" );
        }
        
        $tids = self::get_task_items( $baseID );
        if( $tids )
        {
            $tids = self::toOneArray( $tids, "id_item" );
            $criteria->addCondition( "t.id NOT IN( ".implode( ",", $tids )." )" );
        }
        
        $criteria->join = "LEFT JOIN m_items_history mih ON mih.id_item = t.id";
        $criteria->order = "COUNT( mih.id_item ) ASC, RAND()";
        $criteria->limit = $limit;
        $criteria->group = "t.id";
        $criteria->params = array( ":baseID" => $baseID );
        $model = Items::model()->findAll( $criteria );
        return $model;
    }
}