<?php

/**
 * This is the model class for table "{{users_groups}}".
 *
 * The followings are the available columns in table '{{users_groups}}':
 * @property integer $id
 * @property integer $id_user
 * @property integer $id_group
 * @property string $name
 * @property string $arrData
 * @property integer $created
 * @property integer $updated
 */
class UsersGroups extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{users_groups}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, id_user, id_group, name, arrData, created, updated', 'required'),
			array('id, id_user, id_group, created, updated', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_user, id_group, name, arrData, created, updated', 'safe', 'on'=>'search'),
		);
	}
    
    public function scopes()
    {
        return array( 'userID' );
    }
    
    public function userID( $userID = false )
    {
        $this->getDbCriteria()->mergeWith(array(
            'condition' => 't.id_user = :userID',
            'params' => array( ':userID' => $userID )
        ));
        return $this;
    }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_user' => 'Id User',
			'id_group' => 'Id Group',
			'name' => 'Name',
			'arrData' => 'Arr Data',
			'created' => 'Created',
			'updated' => 'Updated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_user',$this->id_user);
		$criteria->compare('id_group',$this->id_group);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('arrData',$this->arrData,true);
		$criteria->compare('created',$this->created);
		$criteria->compare('updated',$this->updated);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return UsersGroups the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
