<?php

/**
 * This is the model class for table "{{task}}".
 *
 * The followings are the available columns in table '{{task}}':
 * @property integer $id
 * @property integer $id_base
 * @property integer $active
 * @property integer $runTime
 * @property integer $created
 */
class Task extends CActiveRecord
{
    public $isNewRecord = false;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{task}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('id_base', 'required'),
			array('id_base, active, runTime, created', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('isNewRecord, id, id_base, active, runTime, created', 'safe', 'on'=>'search'),
		);
	}
    
    public function scopes()
    {
        return array( 'baseID', 'taskID', 'runTime' );
    }
    
    public function baseID( $baseID = 0 )
    {
        $this->getDbCriteria()->mergeWith(array(
            'condition' => 't.id_base = :baseID',
            'params' => array( ':baseID' => $baseID )
        ));
        return $this;
    }
    
    public function taskID( $taskID = 0 )
    {
        $this->getDbCriteria()->mergeWith(array(
            'condition' => 't.id = :taskID',
            'params' => array( ':taskID' => $taskID )
        ));
        return $this;
    }
    
    public function runTime( $runTime )
    {
        $this->getDbCriteria()->mergeWith(array(
            'condition' => 't.runTime = :runTime',
            'params' => array( ':runTime' => $runTime )
        ));
        return $this;
    }
    
    protected function beforeSave()
    {
        parent::beforeSave();
        if( $this->isNewRecord == true )
        {
            $model = self::model()->baseID( $this->id_base )->runTime( $this->runTime )->findAll();
            if( count( $model ) )
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        else
        {
            return true;
        }
    }
    
    protected function afterSave()
    {
        parent::afterSave();
        $model = TaskItems::model()->baseID( $this->id_base )->taskID( $this->id )->findAll();
        if( $model )
        {
            foreach( $model as $key => $val )
            {
                $val->active = $this->active;
                $val->save();                
            }
        }
    }
    
    protected function afterDelete()
    {
        parent::afterDelete();
        $model = TaskItems::model()->baseID( $this->id_base )->taskID( $this->id )->findAll();
        if( $model )
        {
            foreach( $model as $key => $val )
            {
                $val->delete();
            }
        }
    }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'itemsCount' => array( self::STAT, 'TaskItems', 'id_task', 'select'=> 'COUNT(*)' ),
            'items' => array( self::HAS_MANY, 'TaskItems', 'id_task', 'order' => 'keySort ASC' ),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_base' => 'Id Base',
			'active' => 'Active',
			'runTime' => 'Run Time',
			'created' => 'Created',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_base',$this->id_base);
		$criteria->compare('active',$this->active);
		$criteria->compare('runTime',$this->runTime);
		$criteria->compare('created',$this->created);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Task the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
