<?php

class ItemsImagesHashSystem {
    
    public static function count( $baseID, $countable = true )
    {
        
    }
    
    public static function search( $itemID )
    {
        $result = false;
        $hash_array = ItemsImagesHash::model()->itemID( $itemID )->findAll();
        if( $hash_array ) {
            $hash_array = Core::toOneArray( $hash_array, "hashCode" );
            if( count( $hash_array ) ) {
                $result = ItemsImagesHash::model()->hashCode( $hash_array )->findAll();
            }
        }
        return $result;
    }
}