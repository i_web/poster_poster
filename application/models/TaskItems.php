<?php

/**
 * This is the model class for table "{{task_items}}".
 *
 * The followings are the available columns in table '{{task_items}}':
 * @property integer $id_task
 * @property integer $id_base
 * @property integer $id_item
 * @property integer $id_social_user
 * @property integer $id_social_group
 * @property integer $id_social_item
 * @property integer $active
 * @property string $exportSystem
 * @property integer $keySort
 * @property integer $runTime
 */
class TaskItems extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{task_items}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_task, id_base, id_item, exportSystem, keySort', 'required'),
			array('id_task, id_base, id_item, id_social_user, id_social_group, id_social_item, active, keySort, runTime', 'numerical', 'integerOnly'=>true),
			array('exportSystem', 'length', 'max'=>7),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_task, id_base, id_item, id_social_user, id_social_group, id_social_item, active, exportSystem, keySort, runTime', 'safe', 'on'=>'search'),
		);
	}
    
    public function scopes()
    {
        return array( 'baseID', 'taskID' );
    }
    
    public function baseID( $baseID = 0 )
    {
        $this->getDbCriteria()->mergeWith(array(
            'condition' => 't.id_base = :baseID',
            'params' => array( ':baseID' => $baseID )
        ));
        return $this;
    }
    
    public function taskID( $taskID = 0 )
    {
        $this->getDbCriteria()->mergeWith(array(
            'condition' => 't.id_task = :taskID',
            'params' => array( ':taskID' => $taskID )
        ));
        return $this;
    }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'item' => array( self::BELONGS_TO, 'Items', array( 'id_item' => 'id' ) ),
            'items' => array( self::BELONGS_TO, 'Items', array( 'id_item' => 'id' ) ),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_task' => 'Id Task',
			'id_base' => 'Id Base',
			'id_item' => 'Id Item',
			'id_social_user' => 'Id Social User',
			'id_social_group' => 'Id Social Group',
			'id_social_item' => 'Id Social Item',
			'active' => 'Active',
			'exportSystem' => 'Export System',
			'keySort' => 'Key Sort',
			'runTime' => 'Run Time',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_task',$this->id_task);
		$criteria->compare('id_base',$this->id_base);
		$criteria->compare('id_item',$this->id_item);
		$criteria->compare('id_social_user',$this->id_social_user);
		$criteria->compare('id_social_group',$this->id_social_group);
		$criteria->compare('id_social_item',$this->id_social_item);
		$criteria->compare('active',$this->active);
		$criteria->compare('exportSystem',$this->exportSystem,true);
		$criteria->compare('keySort',$this->keySort);
		$criteria->compare('runTime',$this->runTime);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TaskItems the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
