<?php

class ItemsDublicates {
    
    public static function search( $baseID, $itemID )
    {
        $result = false;
        $hash_array = array();
        $criteria = new CDbCriteria;
        $criteria->select = "t.hashCode";
        $criteria->condition = "t.id_item = :itemID";
        $criteria->params = array( ":itemID" => $itemID );
        $model = ItemsImagesHash::model()->findAll( $criteria );
        if( $model ) {
            $hash_array = Core::toOneArray( $model, "hashCode" );
            if( count( $hash_array ) ) {
                $criteria = new CDbCriteria;
                $criteria->condition = "t.id_base = :baseID AND t.id_item <> :itemID";
                $criteria->params = array( ":baseID" => $baseID, ":itemID" => $itemID );
                $criteria->addInCondition( "t.hashCode", $hash_array );
                $criteria->group = "t.id_item";
                $criteria->order = "t.id_item DESC";
                $result = ItemsImagesHash::model()->with( "items" )->findAll( $criteria );
            }
        }
        return $result;
    }
    
    public static function count( $baseID, $count = true )
    {
        $hash_array = array();
        $criteria = new CDbCriteria;
        $criteria->select = "t.hashCode";
        $criteria->condition = "t.id_base = :baseID";
        $criteria->params = array( ":baseID" => $baseID );
        $criteria->group = "t.hashCode";
        $criteria->having = "COUNT(t.hashCode) > 1";
        $criteria->order = "COUNT(t.hashCode) DESC";
        $model = ItemsImagesHash::model()->findAll( $criteria );
        if( $model ) {
            foreach( $model as $k => $v ) {
                $hash_array[] = $v->hashCode;
            }
        }
        if( count( $hash_array ) ) {
            $criteria = new CDbCriteria;
            $criteria->condition = "t.id_base = :baseID";
            $criteria->params = array( ":baseID" => $baseID );
            $criteria->addInCondition( "t.hashCode", $hash_array );
            $criteria->group = "t.id_item";
            $criteria->order = "t.id_item DESC";
            if( $count ) {
                return ItemsImagesHash::model()->count( $criteria );
            }
            else {
                return ItemsImagesHash::model()->with("items")->findAll( $criteria );
            }
        }
        else {
            if( $count ) {
                return 0;
            }
            else {
                return false;
            }
        }
    }
    
}