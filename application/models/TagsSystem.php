<?php

class TagsSystem {
    
    public static $arrList = array();
    
    public static function math( $arrData = array(), $maxSize = 45 )
    {
        $result = array();
        if( count( $arrData ) > 0 )
        {
            $dbSize = count( $arrData );
            $nowSize = 0;
            
            while( count($result) < $maxSize )
            {
                if( $arrData[$nowSize]["count"] > 1 )
                {
                    array_push( $result, $arrData[$nowSize] );
                    $arrData[$nowSize]["count"] = $arrData[$nowSize]["count"] - 1;
                }
                $nowSize = $nowSize == $dbSize - 1 ? 0 : $nowSize + 1;
            }
        }
        return $result;
    }
    
    public static function getAll( $baseID, $type = 1 )
    {
        switch( $type )
        {
            case 1:
            return Yii::app()->db->CreateCommand("
            SELECT mtagi.id_tags, COUNT(mtagi.id_tags) AS count
            FROM m_tags_items mtagi
            WHERE mtagi.id_base = '".$baseID."'
            AND
            mtagi.id_item not in
            (
            SELECT miht.id_item
            FROM m_items_history miht
            WHERE miht.id_base = mtagi.id_base
            AND
            FROM_UNIXTIME(miht.created) > (NOW() - INTERVAL 30 DAY) GROUP BY miht.id_item
            )
            AND
            mtagi.id_item not in
            (
            SELECT mti.id_item
            FROM m_task_items mti
            WHERE mti.id_base = mtagi.id_base
            )
            GROUP BY mtagi.id_tags
            HAVING COUNT(mtagi.id_tags) > 1
            ORDER BY COUNT(mtagi.id_tags) DESC
            ")->queryAll();
            break;
            
            case 2:
            return Yii::app()->db->CreateCommand("
            SELECT mtagi.id_tags, COUNT(mtagi.id_tags) AS count
            FROM m_tags_items mtagi
            INNER JOIN m_tags mt ON mt.id = mtagi.id_tags AND mt.id_base = mtagi.id_base AND mt.id_parent = 0
            WHERE mtagi.id_base = '".$baseID."'
            AND
            mtagi.id_item not in
            (
            SELECT miht.id_item
            FROM m_items_history miht
            WHERE miht.id_base = mtagi.id_base
            AND
            FROM_UNIXTIME(miht.created) > (NOW() - INTERVAL 30 DAY) GROUP BY miht.id_item
            )
            AND
            mtagi.id_item not in
            (
            SELECT mti.id_item
            FROM m_task_items mti
            WHERE mti.id_base = mtagi.id_base
            )
            GROUP BY mtagi.id_tags
            HAVING COUNT(mtagi.id_tags) > 1
            ORDER BY COUNT(mtagi.id_tags) DESC
            ")->queryAll();
            break;
        }
    }
    
    public static function getList( $baseID, $parentID = 0 )
    {
        $model = Tags::model()->baseID( $baseID )->parentID( $parentID )->findAll();
        if( $model ) {
            foreach( $model as $k => $v ) {
                $nbsp = html_entity_decode('&nbsp;');
                $v->name = str_repeat( $nbsp, $v->clevel * 10 ) . $v->name;
                array_push( self::$arrList, $v );
                self::getList( $baseID, $v->id );
            }
        }
        return self::$arrList;
    }
}