<?php

class ItemsImagesSystem {
    
    public static function set_hash( $imageID, $arrHash = array() )
    {
        if( count( $arrHash ) > 0 )
        {
            $photo = ItemsImages::model()->findByPk( $imageID );
            if( $photo )
            {
                foreach( $arrHash as $hashCode )
                {
                    $model = new ItemsImagesHash;
                    $model->id_image = $photo->id;
                    $model->id_item = $photo->id_item;
                    $model->id_base = $photo->id_base;
                    $model->hashType = $hashCode;
                    $model->hashCode = Core::imgHash( $hashCode )->hash( Yii::app()->params[ "upload_dir" ] . $photo->local_path );
                    $model->save();
                }
            }
        }
    }
    
    public static function get_file_data( $file )
    {
        $fileData['size'] = getimagesize( $file );
        $fileData['width'] = $fileData['size'][0];
        $fileData['height'] = $fileData['size'][1];
        $fileData['ext'] = str_replace( ".", "", image_type_to_extension( $fileData['size'][2] ) );
        $fileData['weight'] = filesize( $file );
        $fileData['hashFile'] = hash_file( 'md5', $file );
        return $fileData;
    }
    
    public static function photo_upload( $itemID )
    {
        $model = ItemsImages::model()->itemID( $itemID )->findAll();
        if( $model )
        {
            foreach( $model as $key => $val )
            {
                if( $val->type == "doc" )
                {
                    $file = Yii::app()->params[ "upload_dir" ] . $val->id . "." . $val->ext;
                }
                if( $val->type == "photo" )
                {
                    $image_info = getimagesize( $val->original_path );
                    $file_ext = str_replace( ".", "", image_type_to_extension( $image_info[ 2 ] ) );
                    
                    $file = Yii::app()->params[ "upload_dir" ] . $val->id . "." . $file_ext;
                }
                if( copy( $val->original_path, $file ) )
                {
                    $fileData = self::get_file_data( $file );
                    
                    $model = ItemsImages::model()->findByPk( $val->id );
                    $model->local_path = str_replace( Yii::app()->params[ "upload_dir" ], "", $file );
                    $model->width = $fileData[ "width" ];
                    $model->height = $fileData[ "height" ];
                    $model->weight = $fileData[ "weight" ];
                    $model->ext = $fileData[ "ext" ];
                    $model->save();
                    
                    self::set_hash( $val->id, [ "ph" ] );
                }
            }
        }
    }
    
    public static function upload( $params = array() )
    {
        $image_info = getimagesize( $params[ "url" ] );
        $file_ext = str_replace( ".", "", image_type_to_extension( $image_info[ 2 ] ) );
        
        $model = new ItemsImages;
        $model->id_item = $params[ "itemID" ];
        $model->id_base = $params[ "baseID" ];
        $model->keySort = isset( $params[ "keySort" ] ) ? $params[ "keySort" ] : 0;
        $model->original_path = $params[ "url" ];
        $model->type = "photo";
        $model->ext = $file_ext;
        $model->save();
        
        $insertID = $model->id;
        
        $file = Yii::app()->params['upload_dir'] . $insertID . "." . $file_ext;
        
        if(copy( $params[ "url" ] , $file))
        {
            $fileData = self::get_file_data( $file );
            
            $model = ItemsImages::model()->findByPk( $insertID );
            $model->local_path = str_replace( Yii::app()->params[ "upload_dir" ], "", $file );
            $model->width = $fileData[ "width" ];
            $model->height = $fileData[ "height" ];
            $model->weight = $fileData[ "weight" ];
            $model->ext = $fileData[ "ext" ];
            $model->save();
            
            self::set_hash( $insertID, [ "ph" ] );
        }
    }
    
    public static function set_url( $imageID, $baseID, $itemID, $url )
    {
        $model = new ItemsImagesUrl;
        $model->id_image = $imageID;
        $model->id_base = $baseID;
        $model->id_item = $itemID;
        $model->url = $url;
        $model->save();
    }
    
    public static function get_double( $baseID )
    {
        
    }
}