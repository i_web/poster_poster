<?php

/**
 * This is the model class for table "{{bases}}".
 *
 * The followings are the available columns in table '{{bases}}':
 * @property integer $id
 * @property integer $id_user
 * @property string $name
 * @property string $tags
 * @property string $tagsCriteria
 * @property integer $groupByTags
 * @property string $exportSystem
 * @property integer $runTimeCount
 * @property string $runTimeStart
 * @property integer $runTimeRotation
 * @property integer $created
 */
class Bases extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{bases}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_user, name, created', 'required'),
			array('id_user, groupByTags, runTimeCount, runTimeRotation, created', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>255),
			array('exportSystem', 'length', 'max'=>7),
			array('runTimeStart', 'length', 'max'=>10),
			array('tags, tagsCriteria', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_user, name, tags, tagsCriteria, groupByTags, exportSystem, runTimeCount, runTimeStart, runTimeRotation, created', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'groupsCount' => array( self::STAT, 'BasesGroups', 'id_base', 'select'=> 'COUNT(*)' ),
            'itemsCount' => array( self::STAT, 'BasesItems', 'id_base', 'select'=> 'COUNT(*)' ),
            'mediaCount' => array( self::STAT, 'ItemsImages', 'id_base', 'select'=> 'COUNT(*)' ),
            'mediaSize' => array( self::STAT, 'ItemsImages', 'id_base', 'select'=> 'IFNULL(SUM(weight),0)' ),
            'tagsCount' => array( self::STAT, 'TagsItems', 'id_base', 'select'=> 'COUNT(DISTINCT id_item)' ),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_user' => 'Id User',
			'name' => 'Name',
			'tags' => 'Tags',
			'tagsCriteria' => 'Tags Criteria',
			'groupByTags' => 'Group By Tags',
			'exportSystem' => 'Export System',
			'runTimeCount' => 'Run Time Count',
			'runTimeStart' => 'Run Time Start',
			'runTimeRotation' => 'Run Time Rotation',
			'created' => 'Created',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_user',$this->id_user);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('tags',$this->tags,true);
		$criteria->compare('tagsCriteria',$this->tagsCriteria,true);
		$criteria->compare('groupByTags',$this->groupByTags);
		$criteria->compare('exportSystem',$this->exportSystem,true);
		$criteria->compare('runTimeCount',$this->runTimeCount);
		$criteria->compare('runTimeStart',$this->runTimeStart,true);
		$criteria->compare('runTimeRotation',$this->runTimeRotation);
		$criteria->compare('created',$this->created);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Bases the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
