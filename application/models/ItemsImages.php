<?php

/**
 * This is the model class for table "{{items_images}}".
 *
 * The followings are the available columns in table '{{items_images}}':
 * @property integer $id
 * @property integer $id_item
 * @property integer $id_base
 * @property integer $keySort
 * @property string $original_path
 * @property string $local_path
 * @property integer $width
 * @property integer $height
 * @property integer $weight
 * @property string $type
 * @property string $ext
 * @property integer $run
 */
class ItemsImages extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{items_images}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_item, id_base', 'required'),
			array('id_item, id_base, keySort, width, height, weight, run', 'numerical', 'integerOnly'=>true),
			array('original_path, local_path', 'length', 'max'=>255),
			array('type', 'length', 'max'=>5),
			array('ext', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_item, id_base, keySort, original_path, local_path, width, height, weight, type, ext, run', 'safe', 'on'=>'search'),
		);
	}
    
    public function scopes()
    {
        return array( 'baseID', 'fileID', 'itemID' );
    }
    
    public function baseID( $baseID = 0 )
    {
        $this->getDbCriteria()->mergeWith(array(
            'condition' => 't.id_base = :baseID',
            'params' => array( ':baseID' => $baseID )
        ));
        return $this;
    }
    
    public function fileID( $fileID = 0 )
    {
        $this->getDbCriteria()->mergeWith(array(
            'condition' => 't.id_image = :fileID',
            'params' => array( ':fileID' => $fileID )
        ));
        return $this;
    }
    
    public function itemID( $itemID = 0 )
    {
        $this->getDbCriteria()->mergeWith(array(
            'condition' => 't.id_item = :itemID',
            'params' => array( ':itemID' => $itemID )
        ));
        return $this;
    }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'url' => array( self::BELONGS_TO, 'ItemsImagesUrl', array( 'id' => 'id_image' ) ),
            'hash' => array( self::BELONGS_TO, 'ItemsImagesHash', array( 'id' => 'id_image' ) ),
		);
	}
    
    protected function afterDelete()
    {
        parent::afterDelete();
        $model = ItemsImagesHash::model()->find([ "condition" => "t.id_image = :imageID", "params" => [ ":imageID" => $this->id ] ]);
        if( $model )
        {
            $model->delete();
        }
        $model = ItemsImagesUrl::model()->find([ "condition" => "t.id_image = :imageID", "params" => [ ":imageID" => $this->id ] ]);
        if( $model )
        {
            $model->delete();
        }
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_item' => 'Id Item',
			'id_base' => 'Id Base',
			'keySort' => 'Key Sort',
			'original_path' => 'Original Path',
			'local_path' => 'Local Path',
			'width' => 'Width',
			'height' => 'Height',
			'weight' => 'Weight',
			'type' => 'Type',
			'ext' => 'Ext',
			'run' => 'Run',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_item',$this->id_item);
		$criteria->compare('id_base',$this->id_base);
		$criteria->compare('keySort',$this->keySort);
		$criteria->compare('original_path',$this->original_path,true);
		$criteria->compare('local_path',$this->local_path,true);
		$criteria->compare('width',$this->width);
		$criteria->compare('height',$this->height);
		$criteria->compare('weight',$this->weight);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('ext',$this->ext,true);
		$criteria->compare('run',$this->run);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ItemsImages the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
