<?php

class ItemsHistorySystem {
    
    public static function get_ids( $baseID, $inverval = 30 )
    {
        $ids = array();
        $criteria = new CDbCriteria;
        $criteria->addCondition( "t.id_base = :baseID" );
        $criteria->addCondition( "FROM_UNIXTIME(t.created) > (NOW() - INTERVAL :days DAY)" );
        $criteria->params = array( ":baseID" => $baseID, ":days" => $inverval );
        $criteria->order = "t.id_item ASC";
        $criteria->group = "t.id_item";
        
        $model = ItemsHistory::model()->findAll( $criteria );
        if( $model ) {
            foreach( $model as $k => $v ) {
                //$ids[] = $v->id_item;
                array_push( $ids, $v->id_item );
            }
        }
        return $ids;
    }
}