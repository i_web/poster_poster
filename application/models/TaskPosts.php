<?php

class TaskPosts {
    
    private $baseID;
    private $taskID;
    private $baseData = array();
    private $taskData = array();
    
    public function __construct( $taskID )
    {
        $this->taskID = $taskID;
        $this->taskData = Task::model()->findByPk( $this->taskID );
        
        $this->baseID = $this->taskData->id_base;
        $this->baseData = Bases::model()->findByPk( $this->baseID );
    }
    
    public function saveOrder( $sortData = array() )
    {
        if( count( $sortData ) > 0 )
        {
            foreach( $sortData as $key => $itemID )
            {
                Yii::app()->db->CreateCommand()->update('m_task_items', array(
                    'keySort' => $key
                ), 'id_item = :itemID', array( ':itemID' => $itemID ) );
            }
            $this->generateTime();
            return $this->getIDs();
        }
    }
    
    public function delete( $itemID = false )
    {
        if( $itemID )
        {
            $where[0] = "id_task = :taskID AND id_base = :baseID AND id_item = :itemID";
            $where[1] = array( ":taskID" => $this->taskID, ":baseID" => $this->baseID, ":itemID" => $itemID );
        }
        elseif( $itemID == false )
        {
            $where[0] = "id_task = :taskID AND id_base = :baseID";
            $where[1] = array( ":taskID" => $this->taskID, ":baseID" => $this->baseID );
        }
        return Yii::app()->db->CreateCommand()->delete( "m_task_items", $where[0], $where[1] );
    }
    
    public function generate( $clear = false )
    {
        if( $clear )
        {
            $this->delete();
        }
        $this->generatePost();
    }
    
    public function generatePost()
    {
        $params['limit'] = $this->baseData['runTimeCount'] > 0 ? $this->baseData['runTimeCount'] : 35;
        $params['groupByTags'] = $this->baseData['groupByTags'];
        
        switch( $params['groupByTags'] )
        {
            case 0:
            $db = Yii::app()->db->CreateCommand("
            SELECT t.* FROM (
            SELECT mi.*, COUNT(mih.id_item) AS historyCount
            FROM m_items mi
            LEFT JOIN m_items_history mih ON mih.id_item = mi.id
            WHERE mi.published = 1 AND mi.id_base = '".$this->baseID."'
            AND
            mi.id not in
            (
            SELECT miht.id_item
            FROM m_items_history miht
            WHERE miht.id_base = mi.id_base
            AND
            FROM_UNIXTIME(miht.created) > (NOW() - INTERVAL 30 DAY) GROUP BY miht.id_item
            )
            AND
            mi.id not in
            (
            SELECT mti.id_item
            FROM m_task_items mti
            WHERE mti.id_base = mi.id_base
            )
            GROUP BY mi.id
            ORDER BY historyCount ASC LIMIT ".$params['limit']."
            ) AS t ORDER BY RAND()
            ")->queryAll();
            $this->savePosts( $db, true );
            break;
            
            case 1:
            $tagsData = TagsSystem::math( TagsSystem::getAll( $this->baseID, 1 ), $params["limit"] );
            if( count( $tagsData ) > 0 )
            {
                $i = 0;
                foreach( $tagsData as $key => $val )
                {
                    $db = Yii::app()->db->CreateCommand("
                    SELECT t.* FROM (
                    SELECT mi.*, COUNT(mih.id_item) AS historyCount
                    FROM m_tags_items mtagi
                    INNER JOIN m_items mi ON mi.id = mtagi.id_item AND mi.published = 1
                    LEFT JOIN m_items_history mih ON mih.id_item = mtagi.id_item
                    WHERE mtagi.id_base = '".$this->baseID."' AND mtagi.id_tags = '".$tagsData[$key]["id_tags"]."'
                    AND
                    mtagi.id_item not in
                    (
                    SELECT miht.id_item
                    FROM m_items_history miht
                    WHERE miht.id_base = mtagi.id_base
                    AND
                    FROM_UNIXTIME(miht.created) > (NOW() - INTERVAL 30 DAY) GROUP BY miht.id_item
                    )
                    AND
                    mtagi.id_item not in
                    (
                    SELECT mti.id_item
                    FROM m_task_items mti
                    WHERE mti.id_base = mtagi.id_base
                    )
                    GROUP BY mtagi.id_item
                    ORDER BY historyCount ASC
                    LIMIT 50
                    ) as t ORDER BY RAND() LIMIT 1
                    ")->queryAll();
                    $this->savePosts( $db, false, $i );
                    $i++;
                }
                $this->generateTime();
            }
            break;
            
            case 2:
            //$tagsData = Base::tags()->math( Base::tags()->getAll( $this->baseID, 2 ), $params["limit"] );
            $tagsData = TagsSystem::math( TagsSystem::getAll( $this->baseID, 2 ), $params["limit"] );
            if( count( $tagsData ) > 0 )
            {
                $i = 0;
                foreach( $tagsData as $key => $val )
                {
                    $db = Yii::app()->db->CreateCommand("
                    SELECT t.* FROM (
                    SELECT mi.*, COUNT(mih.id_item) AS historyCount
                    FROM m_tags_items mtagi
                    INNER JOIN m_items mi ON mi.id = mtagi.id_item AND mi.published = 1
                    LEFT JOIN m_items_history mih ON mih.id_item = mtagi.id_item
                    WHERE mtagi.id_base = '".$this->baseID."' AND mtagi.id_tags = '".$tagsData[$key]["id_tags"]."'
                    AND
                    mtagi.id_item not in
                    (
                    SELECT miht.id_item
                    FROM m_items_history miht
                    WHERE miht.id_base = mtagi.id_base
                    AND
                    FROM_UNIXTIME(miht.created) > (NOW() - INTERVAL 30 DAY) GROUP BY miht.id_item
                    )
                    AND
                    mtagi.id_item not in
                    (
                    SELECT mti.id_item
                    FROM m_task_items mti
                    WHERE mti.id_base = mtagi.id_base
                    )
                    GROUP BY mtagi.id_item
                    ORDER BY historyCount ASC
                    LIMIT 50
                    ) as t ORDER BY RAND() LIMIT 1
                    ")->queryAll();
                    $this->savePosts( $db, false, $i );
                    $i++;
                }
                $this->generateTime();
            }
            break;
        }
    }
    
    public function getIDs()
    {
        return Yii::app()->db->CreateCommand()
        ->select("id_item AS id, FROM_UNIXTIME(runTime,'%d-%m-%Y %H:%i:%s') AS runTime")
        ->from("m_task_items")
        ->where("id_task = :taskID AND id_base = :baseID", array( ":taskID" => $this->taskID, ":baseID" => $this->baseID ))
        ->order("keySort ASC")
        ->queryAll();
    }
    
    public function generateTime()
    {
        $DBResult = $this->getIDs();
        if( count( $DBResult ) > 0 )
        {
            $datetime = new DateTime( date( "Y-m-d H:i", $this->taskData['runTime'] ) );
            
            foreach($DBResult as $item)
            {
                Yii::app()->db->CreateCommand()->update('m_task_items', array(
                    'runTime' => $datetime->format('U')
                ), 'id_item = :itemID', array( ':itemID' => $item['id'] ) );
                
                $datetime->modify('+'.$this->baseData['runTimeRotation'].' minutes');
            }
        }
    }
    
    public function savePosts( $DBResult, $time = false, $keySort = false )
    {
        if( count( $DBResult ) > 0 )
        {
            foreach( $DBResult as $key => $val )
            {
                Yii::app()->db->CreateCommand()->insert('m_task_items', array(
                    'id_task' => $this->taskID,
                    'id_base' => $this->baseID,
                    'id_item' => $DBResult[$key]['id'],
                    'id_social_group' => $this->getIDGroupActive( $this->baseID ),
                    'exportSystem' => $this->baseData['exportSystem'],
                    'keySort' => $keySort == true ? $keySort : $key
                ));
            }
            if( $time )
            {
                $this->generateTime();
            }
        }
    }
    
    public function getIDGroupActive( $baseID )
    {
        return Yii::app()->db->CreateCommand()
        ->select("id_group")
        ->from("m_bases_groups")
        ->where("id_base = :baseID AND is_active = 1", array( ":baseID" => $baseID ))
        ->queryScalar();
    }
}