<?php

/**
 * This is the model class for table "{{items}}".
 *
 * The followings are the available columns in table '{{items}}':
 * @property integer $id
 * @property integer $id_user
 * @property integer $id_base
 * @property string $title
 * @property string $text
 * @property integer $published
 * @property string $origin_url
 * @property integer $origin_show
 * @property integer $created
 */
class Items extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{items}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_user, id_base, created', 'required'),
			array('id_user, id_base, published, origin_show, created', 'numerical', 'integerOnly'=>true),
			array('origin_url', 'length', 'max'=>255),
            array('title, text', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_user, id_base, title, text, published, origin_url, origin_show, created', 'safe', 'on'=>'search'),
		);
	}
    
    public function scopes()
    {
        return array( 'baseID' );
    }
    
    public function baseID( $baseID = 0 )
    {
        $this->getDbCriteria()->mergeWith(array(
            'condition' => 't.id_base = :baseID',
            'params' => array( ':baseID' => $baseID )
        ));
        return $this;
    }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'images' => array( self::HAS_MANY, 'ItemsImages', 'id_item', 'order'=> 'images.keySort ASC' ),
            'images_l' => array( self::HAS_MANY, 'ItemsImages', 'id_item', 'joinType' => 'LEFT JOIN', 'order'=> 'keySort ASC' ),
            'tags' => array( self::HAS_MANY, 'TagsItems', 'id_item' ),
            'tags_l' => array( self::HAS_MANY, 'TagsItems', 'id_item', 'joinType' => 'LEFT JOIN' ),
            'history' => array( self::HAS_MANY, 'ItemsHistory', 'id_item', 'order' => 'history.created DESC' ),
            'history_l' => array( self::HAS_MANY, 'ItemsHistory', 'id_item', 'joinType' => 'LEFT JOIN', 'order' => 'history_l.created DESC' ),
            'history_count' => array( self::STAT, 'ItemsHistory', 'id_item', 'select'=> 'COUNT(*)' ),
            'tasks' => array( self::HAS_MANY, 'TaskItems', 'id_item' ),
            'b_items' => array( self::HAS_MANY, 'BasesItems', 'id_item' ),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_user' => 'Id User',
			'id_base' => 'Id Base',
			'title' => 'Title',
			'text' => 'Text',
			'published' => 'Published',
			'origin_url' => 'Origin Url',
			'origin_show' => 'Origin Show',
            'created' => 'Created',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_user',$this->id_user);
		$criteria->compare('id_base',$this->id_base);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('text',$this->text,true);
		$criteria->compare('published',$this->published);
		$criteria->compare('origin_url',$this->origin_url,true);
		$criteria->compare('origin_show',$this->origin_show);
        $criteria->compare('created',$this->created);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Items the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
