<?php

/**
 * This is the model class for table "{{tags_items}}".
 *
 * The followings are the available columns in table '{{tags_items}}':
 * @property integer $id_tags
 * @property integer $id_item
 * @property integer $id_base
 */
class TagsItems extends CActiveRecord
{
    public $totalTags = 0;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{tags_items}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_tags, id_item', 'required'),
			array('id_tags, id_item, id_base', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_tags, totalTags, id_item, id_base', 'safe', 'on'=>'search'),
		);
	}
    
    public function scopes()
    {
        return array( 'baseID', 'tagID', 'itemID' );
    }
    
    public function itemID( $itemID = 0 )
    {
        $this->getDbCriteria()->mergeWith(array(
            'condition' => 't.id_item = :itemID',
            'params' => array( ':itemID' => $itemID )
        ));
        return $this;
    }
    
    public function tagID( $tagID = 0 )
    {
        $this->getDbCriteria()->mergeWith(array(
            'condition' => 't.id_tags = :tagID',
            'params' => array( ':tagID' => $tagID )
        ));
        return $this;
    }
    
    public function baseID( $baseID = 0 )
    {
        $this->getDbCriteria()->mergeWith(array(
            'condition' => 't.id_base = :baseID',
            'params' => array( ':baseID' => $baseID )
        ));
        return $this;
    }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_tags' => 'Id Tags',
			'id_item' => 'Id Item',
			'id_base' => 'Id Base',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_tags',$this->id_tags);
		$criteria->compare('id_item',$this->id_item);
		$criteria->compare('id_base',$this->id_base);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TagsItems the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
