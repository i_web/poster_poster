<?php

/**
 * This is the model class for table "{{bases_bookmarks}}".
 *
 * The followings are the available columns in table '{{bases_bookmarks}}':
 * @property integer $id
 * @property integer $id_base
 * @property integer $created
 * @property integer $updated
 * @property integer $id_group
 * @property string $name
 * @property string $arrData
 */
class BasesBookmarks extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{bases_bookmarks}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, id_base, created, updated, id_group, name, arrData', 'required'),
			array('id, id_base, created, updated, id_group', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_base, created, updated, id_group, name, arrData', 'safe', 'on'=>'search'),
		);
	}
    
    public function scopes()
    {
        return array( 'baseID' );
    }
    
    public function baseID( $baseID = 0 )
    {
        $this->getDbCriteria()->mergeWith(array(
            'condition' => 't.id_base = :baseID',
            'params' => array( ':baseID' => $baseID )
        ));
        return $this;
    }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_base' => 'Id Base',
			'created' => 'Created',
			'updated' => 'Updated',
			'id_group' => 'Id Group',
			'name' => 'Name',
			'arrData' => 'Arr Data',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_base',$this->id_base);
		$criteria->compare('created',$this->created);
		$criteria->compare('updated',$this->updated);
		$criteria->compare('id_group',$this->id_group);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('arrData',$this->arrData,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BasesBookmarks the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
