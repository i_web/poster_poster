<?php

class TaskItemsSystem {
    
    public static function model_to_result( $model, $column )
    {
        $result = false;
        if( count( $model ) > 0 )
        {
            foreach( $model as $key => $val )
            {
                $result[] = $val[ $column ];
            }
        }
        return $result;
    }
    
    public static function get( $params = array() )
    {
        $params[ "baseID" ] = isset( $params[ "baseID" ] ) ? $params[ "baseID" ] : false;
        $params[ "taskID" ] = isset( $params[ "taskID" ] ) ? $params[ "taskID" ] : false;
        $params[ "getIDS" ] = isset( $params[ "getIDS" ] ) ? $params[ "getIDS" ] : ( in_array( "getIDS", $params ) ? true : false );
        
        $result = false;
        
        $criteria = new CDbCriteria();
        $criteria_params = array();
        
        if( $params[ "baseID" ] )
        {
            $criteria->addCondition( "t.id_base = :baseID" );
            $criteria_params[ ":baseID" ] = $params[ "baseID" ];
        }
        if( $params[ "taskID" ] )
        {
            $criteria->addCondition( "t.id_task = :taskID" );
            $criteria_params[ ":taskID" ] = $params[ "taskID" ];
        }
        
        if( count( $criteria_params ) > 0 )
        {
            $criteria->params = $criteria_params;
        }
        
        $model = TaskItems::model()->findAll( $criteria );
        
        if( $params[ "getIDS" ] )
        {
            $ids = self::model_to_result( $model, "id_item" );
            if( $params[ "getIDS" ] === "implode" )
            {
                $ids = implode( ",", $ids );
            }
            $result = $ids;
        }
        else
        {
            $result = $model;
        }
        return $result;
    }
}