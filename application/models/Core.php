<?php

class Core {
    
    function implode( $array, $key, $delimiter )
    {
        $arrayResult = array();
        if(count($array) > 0) {
            foreach($array as $item) {
                array_push( $arrayResult, $item[$key] );
            }
        }
        return implode( $delimiter, $arrayResult );
    }
    
    public function toOneArray( $array, $column )
    {
        $result = array();
        if( count( $array ) ) {
            foreach( $array as $key => $val ) {
                if( isset( $val[ $column ] ) ) {
                    $result[] = $val[ $column ];
                    //array_push( $result, $val[ $column ] );
                }
            }
        }
        return count( $result ) ? $result : false;
    }
    
    public function imgHash( $implementation = null, $mode = null )
    {
        return new CoreIMGHash( $implementation, $mode );
    }
    
    public function explode_end( $delimiter, $string )
    {
        $result = explode( $delimiter, $string );
        if( count( $result ) > 0 )
        {
            $result = end( $result );
        }
        else
        {
            $result = false;
        }
        return $result;
    }
    
    function FileSizeConvert($bytes)
    {
        $bytes = floatval($bytes);
            $arBytes = array(
                0 => array(
                    "UNIT" => "TB",
                    "VALUE" => pow(1024, 4)
                ),
                1 => array(
                    "UNIT" => "GB",
                    "VALUE" => pow(1024, 3)
                ),
                2 => array(
                    "UNIT" => "MB",
                    "VALUE" => pow(1024, 2)
                ),
                3 => array(
                    "UNIT" => "KB",
                    "VALUE" => 1024
                ),
                4 => array(
                    "UNIT" => "B",
                    "VALUE" => 1
                ),
            );
    
        foreach($arBytes as $arItem)
        {
            if($bytes >= $arItem["VALUE"])
            {
                $result = $bytes / $arItem["VALUE"];
                $result = str_replace(".", "," , strval(round($result, 2)))." ".$arItem["UNIT"];
                break;
            }
            else
            {
                $result = 0;
            }
        }
        return $result;
    }
    
    public static function delete_task_post( $arrData = array() )
    {
        $where[0] = "id_base = :baseID AND id_task = :taskID AND id_item = :itemID";
        $where[1] = array( ":baseID" => $arrData[ "baseID" ], ":taskID" => $arrData[ "taskID" ], ":itemID" => $arrData[ "itemID" ] );
        Yii::app()->db->CreateCommand()->delete( "m_task_items", $where[0], $where[1] );
    }
        
    public static function setLogs( $arrData = array() )
    {
        return Yii::app()->db->CreateCommand()->insert("m_task_logs", array(
            "id_item" => isset( $arrData[ "itemID" ] ) ? $arrData[ "itemID" ] : 0,
            "id_base" => isset( $arrData[ "baseID" ] ) ? $arrData[ "baseID" ] : 0,
            "id_group" => isset( $arrData[ "groupID" ] ) ? $arrData[ "groupID" ] : 0,
            "method" => isset( $arrData[ "method" ] ) ? $arrData[ "method" ] : null,
            "params" => isset( $arrData[ "params" ] ) ? serialize( $arrData[ "params" ] ) : null,
            "type" => isset( $arrData[ "type" ] ) ? $arrData[ "type" ] : null,
            "title" => isset( $arrData[ "title" ] ) ? $arrData[ "title" ] : null,
            "text" => isset( $arrData[ "text" ] ) ? serialize( $arrData[ "text" ] ) : null,
            "created" => time()
        ));
    }
    
    public static function tasks_set_history( $arrData = array() )
    {
        return Yii::app()->db->CreateCommand()->insert("m_items_history", array(
            "id_item" => isset( $arrData[ "itemID" ] ) ? $arrData[ "itemID" ] : 0,
            "id_item_social" => isset( $arrData[ "itemIDSocial" ] ) ? $arrData[ "itemIDSocial" ] : 0,
            "id_group" => isset( $arrData[ "groupID" ] ) ? $arrData[ "groupID" ] : 0,
            "id_base" => isset( $arrData[ "baseID" ] ) ? $arrData[ "baseID" ] : 0,
            "runTime" => isset( $arrData[ "runTime" ] ) ? $arrData[ "runTime" ] : 0,
            "created" => time()
        ));
    }
}