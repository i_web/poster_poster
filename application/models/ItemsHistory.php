<?php

/**
 * This is the model class for table "{{items_history}}".
 *
 * The followings are the available columns in table '{{items_history}}':
 * @property integer $id_item
 * @property integer $id_item_social
 * @property integer $id_group
 * @property integer $id_base
 * @property integer $runTime
 * @property integer $created
 * @property integer $num_like
 * @property integer $num_repost
 */
class ItemsHistory extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{items_history}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_item, id_item_social, id_group, id_base, runTime, created', 'required'),
			array('id_item, id_item_social, id_group, id_base, runTime, created, num_like, num_repost', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_item, id_item_social, id_group, id_base, runTime, created, num_like, num_repost', 'safe', 'on'=>'search'),
		);
	}
    
    public function scopes()
    {
        return array(
            'baseID',
            'created_interval',
            'group' => array(
                'order' => 't.id_item ASC',
                'group' => 't.id_item',
            ),
        );
    }
    
    public function created_interval( $days = 30 )
    {
        $this->getDbCriteria()->mergeWith(array(
            'condition' => 'FROM_UNIXTIME(t.created) > (NOW() - INTERVAL :days DAY)',
            'params' => array( ':days' => $days )
        ));
        return $this;
    }
    
    public function baseID( $baseID = 0 )
    {
        $this->getDbCriteria()->mergeWith(array(
            'condition' => 't.id_base = :baseID',
            'params' => array( ':baseID' => $baseID )
        ));
        return $this;
    }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'items' => array( self::BELONGS_TO, 'Items', array( 'id_item' => 'id' )  ),
            'groups' => array( self::BELONGS_TO, 'UsersGroups', array( 'id_group' => 'id_group' )  ),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_item' => 'Id Item',
			'id_item_social' => 'Id Item Social',
			'id_group' => 'Id Group',
			'id_base' => 'Id Base',
			'runTime' => 'Run Time',
			'created' => 'Created',
			'num_like' => 'Num Like',
			'num_repost' => 'Num Repost',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_item',$this->id_item);
		$criteria->compare('id_item_social',$this->id_item_social);
		$criteria->compare('id_group',$this->id_group);
		$criteria->compare('id_base',$this->id_base);
		$criteria->compare('runTime',$this->runTime);
		$criteria->compare('created',$this->created);
		$criteria->compare('num_like',$this->num_like);
		$criteria->compare('num_repost',$this->num_repost);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ItemsHistory the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
