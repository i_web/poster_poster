<div class="item">
<input type="button" class="btn btn-primary" value="Create" onclick="core.application.create()"/>
<br /><br />

<table class="items-list">
<thead>
<tr>
<th>ID</th>
<th>appID</th>
<th>Name</th>
<th>Type</th>
<th>Created</th>
<th>&nbsp;</th>
</tr>
</thead>
<tbody>
<?php if( $model ): ?>
    <?php foreach( $model as $k => $v ): ?>
    <tr class="itemid" id="<?=$v->id?>">
    <td><?=$v->id?></td>
    <td><a href="javascript: void( 0 )" onclick="core.application.edit( <?=$v->id?> )"><?=$v->appID?></a></td>
    <td style="text-align: left; padding-left: 15px;"><a href="javascript: void( 0 )" onclick="core.application.edit( <?=$v->id?> )"><?=$v->name?></a></td>
    <td><?=$v->type?></td>
    <td><?=date( "Y-m-d H:i:s", $v->created )?></td>
    <td><a href="javascript: void( 0 )" onclick="core.application.delete( <?=$v->id?> )">Удалить</a></td>
    </tr>
    <?php endforeach; ?>
<?php else: ?>
<tr class="itemid">
<td style="text-align: center;" colspan="6">Данных нет</td>
</tr>
<?php endif; ?>
</tbody>
</table>
</div>