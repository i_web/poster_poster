<div class="simple-table popupForm tasks-save-form" style="width: 500px; height: auto !important;">

<div class="table">

<div class="row header">
<div class="cell">
<span style="font-weight: bold; font-size: 14px;">Редактируем приложение:</span>
</div>
</div>

<div class="row content">
<div class="cell a-center v-top">
<?php echo CHtml::activeHiddenField( $model, 'id', array( 'class' => 'user-data', 'style' => 'width: 100%;', 'placeholder' => 'appID', 'id' => 'itemID' ) ); ?>
<?php echo CHtml::activeTextField( $model, 'appID', array( 'class' => 'user-data', 'style' => 'width: 100%;', 'placeholder' => 'appID', 'id' => 'appID' ) ); ?>
<br />
<?php echo CHtml::activeDropDownList( $model, 'type', array(
    'apiLoader' => 'apiLoader',
    'webForcer' => 'webForcer'
), array( 'class' => 'user-data', 'style' => 'width: 100%', 'id' => 'type' ) ); ?>
<br />
<?php echo CHtml::activeTextField( $model, 'name', array( 'class' => 'user-data', 'style' => 'width: 100%;', 'placeholder' => 'Name', 'id' => 'name' ) ); ?>
<br />
<?php echo CHtml::activeTextField( $model, 'appSecret', array( 'class' => 'user-data', 'style' => 'width: 100%;', 'placeholder' => 'appSecret', 'id' => 'appSecret' ) ); ?>
<br />
<?php echo CHtml::activeTextField( $model, 'serviceToken', array( 'class' => 'user-data', 'style' => 'width: 100%;', 'placeholder' => 'serviceToken', 'id' => 'serviceToken' ) ); ?>

</div>
</div>


<div class="row bottom">
<div class="cell a-center v-middle">
<input type="button" style="width: 200px;" value="Закрыть" class="btn btn-primary fancybox-close" />
<input type="button" style="width: 200px;" value="Сохранить" onclick="core.application.save()" class="btn btn-primary" />
</div>
</div>

</div>
</div>