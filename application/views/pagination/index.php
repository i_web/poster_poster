<ul class="tabs-nav <?php echo isset($position) ? $position : ''; ?> <?php echo isset($class) ? $class : ''; ?>">
<?php

if(count($pages) > 0)
{
    foreach($pages as $item)
    {
        ?>
        <li class="<?php echo isset($item['active']) ? 'active' : ''; ?>" id="<?php echo $item['url']; ?>"><?php echo $item['label']; ?></li>
        <?php
    }
}
else
{
    ?>
    <li class="active" id="0">0</li>
    <?php
}

?>
</ul>