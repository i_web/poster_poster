{getClientScript path=$this->assets["path"] file="/css/core.litestrap.css"}
<script type="text/javascript">
function login2( event )
{
    event.preventDefault();
    var block = jQuery(".loginForm");
    if(checkRequired(block.find("input.required")) == true)
    {
        jQuery.ajax({
            type: "POST",
            url: "/auth/login/",
            data: { action : "login", userData : addToArray(block.find("input.user-data")) },
            dataType: "json",
            success: function(data)
            {
                if(data.error == true)
                {
                    alert(data.response);
                    return false;
                }
                if(data.error == false)
                {
                    window.location.href = data.response;
                }
            }
        });
    }
}
</script>
<div class="loginForm">
<form method="post" onsubmit="return login2(event)">
<div class="table">
<div class="row header">
<div class="cell">
<span style="font-weight: bold; font-size: 14px;">Панель администратора</span>
</div>
</div>
<div class="row content">
<div class="cell a-center v-middle">
<input class="user-data required" tooltip="Нужно ввести логин" placeholder="Login" type="text" id="login" />

<input class="user-data required" tooltip="Нужно ввести пароль" placeholder="Password" type="password" id="password" />
</div>
</div>
<div class="row bottom">
<div class="cell a-center v-middle">
<input type="submit" value="Войти" class="btn btn-primary" />
</div>
</div>
</div>
</form>
</div>