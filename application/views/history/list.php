<div class="col-sm-12">
<?php if( isset( $this->baseID ) && $this->baseID > 0 ): ?>
<h3>История публикаций</h3>

<div id="asyncLoadPosts">
<?php if( $model ): ?>
<?php $this->renderPartial( "//history/list.items", array( "model" => $model, "pages" => $pages ) ); ?>
<?php else: ?>
Данных нет
<?php endif; ?>
</div>

<?php else: ?>
<div class="item"><strong>Не выбрана база</strong></div>
<?php endif; ?>
</div>