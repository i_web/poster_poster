<div class="col-sm-12">
<script type="text/javascript">
jQuery(document).ready(function(){
    
    jQuery("tr.itemid td:not(:first-child)").click(function(){
        var row = jQuery(this).parents("tr.itemid");
        var baseID = row.attr("id");
        if( !row.hasClass("active") )
        {
            jQuery.post("/bases/ajax/", { method : "setActive", baseID }, function(data){
                if( data.error == true )
                {
                    alert( data.response );
                }
                if( data.error == false )
                {
                    jQuery( "table tr.itemid.active" ).removeClass("active");
                    row.addClass("active");
                }
            }, "json");
        }
    });
    
});
</script>
<style type="text/css">
.myTable td,
.myTable th {
    background-color: white;
}
.itemid.active td {
    background-color: #cccccc;
    color: white !important;
}
.itemid.active td a {
    color: white !important;
    text-decoration: none;
}
.itemid.active td a:hover {
    text-decoration: underline;
}
</style>
<input type="button" class="btn btn-primary" value="Создать базу" onclick="bases.create.get()"/>
<br /><br />

<table class="table table-bordered table-hover myTable">
<thead>
<tr>
<th>Название</th>
<th>Подключенные группы</th>
<th>Кол-во материала в базе</th>
<th>Кол-во медиа-файлов</th>
<th>Суммарный вес медиа-файлов</th>
<th>Tags / No Tags</th>
<th>Создана</th>
</tr>
</thead>
<tbody>
<?php if( $model ): ?>
    <?php foreach( $model as $k => $v ): ?>
    <?php $active = $v->id == $this->baseID ? true : false; ?>
    <tr class="itemid <?=$active ? 'active' : ''?>" id="<?=$v->id?>">
    <td style="text-align: left; padding-left: 15px;"><a href="javascript: void( 0 )" onclick="bases.edit.get( <?=$v->id?> )"><?=$v->name?></a></td>
    <td><a href="javascript: void( 0 )" onclick="bases.groups.get( <?=$v->id?> )"><?=$v->groupsCount?></a></td>
    <td><?=$v->itemsCount?></td>
    <td><?=$v->mediaCount?></td>
    <td><?=$this->FileSizeConvert( $v->mediaSize )?></td>
    <td><?=$v->tagsCount?> / <?=$v->itemsCount - $v->tagsCount?></td>
    <td><?=date( "Y-m-d H:i:s", $v->created )?></td>
    </tr>
    <?php endforeach; ?>
<?php else: ?>
<tr class="itemid">
<td style="text-align: center;" colspan="7">Данных нет</td>
</tr>
<?php endif; ?>
</tbody>
</table>
</div>
