<div class="simple-table popupForm" style="width: 500px; height: auto !important;">


<h3 style="text-align: center;">Редактируем базу:</h3>

<div class="container-fluid container-fullw bg-white">

    <div class="row">
		<div class="col-md-12">
            <div class="form-group">
            <?php echo CHtml::activeHiddenField( $model, 'id', array( 'class' => 'user-data form-control', 'id' => 'itemID', 'placeholder' => 'itemID' ) ); ?>
            <br />
            <?php echo CHtml::activeTextField( $model, 'name', array( 'class' => 'user-data form-control', 'id' => 'name', 'placeholder' => 'Name' ) ); ?>
            <br />
            <?php echo CHtml::activeTextField( $model, 'tags', array( 'class' => 'user-data form-control', 'id' => 'tags', 'placeholder' => 'Tags' ) ); ?>
            <br />
            <?php echo CHtml::activeDropDownList( $model, 'groupByTags', array(
                    0 => 'Не группировать по тэгам',
                    1 => 'Группировать по тегам ( 1 - Все уровни )',
                    2 => 'Группировать по тегам ( 2 - Верхний уровень )'
            ), array( 'class' => 'user-data form-control', 'id' => 'groupByTags' ) ); ?>
            <br />
            <?php echo CHtml::activeDropDownList( $model, 'exportSystem', array( 'online' => 'Export.Online', 'reserve' => 'Export.Reserve' ), array( 'class' => 'user-data form-control', 'id' => 'exportSystem' ) ); ?>
            <br />
            <?php echo CHtml::activeTextField( $model, 'runTimeCount', array( 'class' => 'user-data form-control', 'id' => 'runTimeCount', 'placeholder' => 'runTimeCount' ) ); ?>
            <br />
            <?php echo CHtml::activeTextField( $model, 'runTimeStart', array( 'class' => 'user-data form-control', 'id' => 'runTimeStart', 'placeholder' => 'runTimeStart' ) ); ?>
            <br />
            <?php echo CHtml::activeTextField( $model, 'runTimeRotation', array( 'class' => 'user-data form-control', 'id' => 'runTimeRotation', 'placeholder' => 'runTimeRotation' ) ); ?>
            </div>
        </div>
    </div>

</div>

<div class="container-fluid container-fullw bg-white">

<div class="row">
		<div class="col-md-12" style="text-align: center;">
        
        <input type="button" style="width: 45%;" value="Закрыть" class="btn btn-warning btn-wide fancybox-close" />
        <?php if( $model->id > 0 ) :?>
        <input type="button" style="width: 45%;" value="Сохранить" onclick="bases.edit.save()" class="btn btn-wide btn-success" />
        <?php else: ?>
        <input type="button" style="width: 45%;" value="Сохранить" onclick="bases.create.save()" class="btn btn-wide btn-success" />
        <?php endif; ?>
        
        </div>
        
</div>

</div>

</div>