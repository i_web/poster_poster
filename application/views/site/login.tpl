<div class="loginForm">
<form method="post" onsubmit="return auth.login(event)">
<div class="table">
<div class="row header">
<div class="cell">
<span style="font-weight: bold; font-size: 14px;">Панель администратора</span>
</div>
</div>
<div class="row content">
<div class="cell a-center v-middle">
<input class="user-data required" tooltip="Нужно ввести email" placeholder="Email" type="text" id="email" />

<input class="user-data required" tooltip="Нужно ввести пароль" placeholder="Password" type="password" id="password" />
</div>
</div>
<div class="row bottom">
<div class="cell a-center v-middle">
<input type="submit" value="Войти" class="btn btn-primary" />
</div>
</div>
</div>
</form>
</div>