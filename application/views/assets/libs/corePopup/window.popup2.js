jQuery(function() {
	var window = jQuery(".window");
	//Draggable
    /*
	window.draggable({ 
		cancel: '.window_inner, .buttonrow',
		containment: 'html',
		scroll: false,
		stack: '.window'
	});
    */
	//Resizeable
    /*
	window.resizable({
		handles: 'n, e, s, w, ne, se, sw, nw',
		containment: 'body',
		minHeight: 80,
		minWidth: 138,
		maxHeight: jQuery('body').height()+330,
		maxWidth: jQuery('body').width()
	});
    */
});
//Sortable
jQuery(function() {
	var programlist = jQuery("#programlist");
	programlist.sortable({
		axis: 'x',
		containment: '#programlist',
		tolerance: 'pointer',
		revert: true
	});
	programlist.disableSelection();
});
//Time
function checkTime(i) {
	if (i < 10) { i="0" + i; }
	return i;
};

jQuery(document).ready(function() {
	var cur_h;
	var cur_w;
	var cur_offset;
	var window = jQuery(".window");
	var start = jQuery("#start");
	var startmenu = jQuery("#startmenu");
	var aeropeek = jQuery("#aero_peek");
	var windowall = jQuery(".window > *");
	//Start Position
	
	// Close a Window
    
	//jQuery(".close").click(function () {
		//jQuery(this).parents(".window").removeClass("open").fadeOut();
	//});
	// Minimize a Window
    /*
	jQuery(".min").click(function () { 
		jQuery(this).parents(".window").fadeOut("fast");
	});
    */
	// Aero Peek
	aeropeek.hover(
		function () { 
			windowall.fadeOut("normal");
			window.addClass("trans_win");
		},
		function() { 
			windowall.fadeIn("slow");
			window.removeClass("trans_win");
		}
	);
	aeropeek.click(function () { 
		if ( jQuery(".window:visible").length == 0 ) {
			window.fadeIn("slow");
		} else {
			window.fadeOut("fast");
		};
	});
	// Doubleclick Maximize
    /*
	jQuery(".title").dblclick(function () {
		var windowbox = jQuery(this).parent(".window");
		var maxHeight = windowbox.resizable('option', 'maxHeight');
		var maxWidth = windowbox.resizable('option', 'maxWidth');
		if ( windowbox.hasClass("maximized") == false ) {
			cur_h = windowbox.height();
			cur_w = windowbox.width();
			cur_offset = windowbox.position()
			windowbox.addClass("maximized");
			windowbox.height(maxHeight);
			windowbox.width(maxWidth);
			windowbox.animate({top:0,left:0},0);
		} else {
			windowbox.removeClass("maximized");
			windowbox.height(cur_h);
			windowbox.width(cur_w);
			windowbox.animate({
				top: cur_offset.top,
				left: cur_offset.left
			},0);
		};
	});
    */
	// Maximize Button
    /*
	window.find(".max").click(function () {
		var windowbox = jQuery(this).parent().parent(".window");
		var maxHeight = windowbox.resizable('option', 'maxHeight');
		var maxWidth = windowbox.resizable('option', 'maxWidth');
		if ( windowbox.hasClass("maximized") == false ) {
			cur_h = windowbox.height();
			cur_w = windowbox.width();
			cur_offset = windowbox.position()
			windowbox.addClass("maximized");
			windowbox.height(maxHeight);
			windowbox.width(maxWidth);
			windowbox.animate({top:0,left:0},0);
		} else {
			windowbox.removeClass("maximized");
			windowbox.height(cur_h);
			windowbox.width(cur_w);
			windowbox.animate({
				top: cur_offset.top,
				left: cur_offset.left
			},0);
		};
	});
    */
	// Add Focus
	window.last().addClass("focus_win");
	window.mousedown(function () { 
		jQuery(".focus_win").removeClass("focus_win");
		jQuery(this).addClass("focus_win");
	});
	// Remove Focus
	jQuery("#taskbar,#handle_area").not("#handle_area *").mousedown(function () { 
		window.removeClass("focus_win");
	});
	jQuery("body *").not("#start,#startmenu,#startmenu *,#taskbar,#page").mousedown( function () {
		if ( start.hasClass("active") ) {
			start.removeClass("active");
			startmenu.hide();
		};
	});
	// Startmenu
	start.mousedown( function () {
		if ( jQuery(this).hasClass("active") ) {
			jQuery(this).removeClass("active");
			startmenu.hide();
		} else {
			jQuery(this).addClass("active");
			startmenu.show();
		};
	});
	// Clock
	setInterval( function() {
		var d = new Date();
		var m = d.getMonth()+1;
		jQuery("#clock").html(checkTime(d.getHours())+":"+checkTime(d.getMinutes())+"<br/>"+checkTime(d.getDate())+"."+checkTime(m)+"."+d.getFullYear());
	}, 1000 );
});