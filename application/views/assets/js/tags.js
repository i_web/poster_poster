tags = {
    create: function()
    {
        jQuery.post("/tags/create/", { itemID : 0 }, function(data){
            jQuery.fancybox.open( data, { touch: false } );
        }, "json");
    },
    edit: function( itemID )
    {
        jQuery.post("/tags/edit/", { itemID }, function(data){
            jQuery.fancybox.open( data, { touch: false } );
        }, "json");
    },
    save: function()
    {
        var block = jQuery( ".popupForm" );
        jQuery.post("/tags/save/", { userData : addToArray(block.find(".user-data")) }, function(json){
            if( Object.keys( json.errors ).length > 0 )
            {
                if( Object.keys( json.errors.code ).length > 0 ) {
                    jQuery.each( json.errors.code, function( index, value ){
                        block.find( "input#" + index ).attr( "placeholder", value );
                    });
                }
                return false;                
            }
            else
            {
                if( typeof json.result == 'object' )
                {
                    jQuery.fancybox.close();
                    setTimeout(function(){
                        loadAsync();
                    }, 1000);
                }
            }
        }, "json");
    },
}