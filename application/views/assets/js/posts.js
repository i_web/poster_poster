posts = {
    import: {
        url: function()
        {
            var url = prompt( "Введите урл сообщения", "" );
            if( url )
            {
                var itemID = url.split( "wall-" ).pop();
                posts.import.get( itemID );
            }
        },
        get: function( itemID )
        {
            flashLoader('open',0);
            jQuery.post("/items/async/", { action : "import", itemID }, function(json){
                if( json.errors.length > 0 )
                {
                    flashLoader('close',0);
                    for( i = 0; i < json.errors.length; i++ )
                    {
                        console.log( "posts.import" + json.errors[ i ] + " )" );
                    }
                    return false;                
                }
                else
                {
                    if( typeof json.result == 'object' )
                    {
                        flashLoader('close',0);
                        posts.edit( json.result.itemID );
                    }
                }
            },"json");
        },
    },
    list: {
        active: function( itemID, element )
        {
            jQuery.post("/items/async/", { action : "list.active", itemID }, function(json){
                if( json.errors.length > 0 )
                {
                    for( i = 0; i < json.errors.length; i++ )
                    {
                        console.log( "posts.list::active" + json.errors[ i ] + " )" );
                    }
                    return false;                
                }
                else
                {
                    if( typeof json.result == 'object' )
                    {
                        if( json.result.active == 1 ) {
                            jQuery( element ).val( 'Включено' ).removeClass( "btn-danger" ).addClass( "btn-success" );
                        }
                        else if( json.result.active == 0 ) {
                            jQuery( element ).val( 'Отключено' ).addClass( "btn-danger" ).removeClass( "btn-success" );
                        }
                    }
                }
            },"json");
        },
    },
    photos : {
        upload: function( itemID, baseID )
        {
            var url = prompt( "Введите прямой url к новому фото", "" );
            
            if( url )
            {
                jQuery.post("/items/async/", { action : "images.upload", userData : { itemID, baseID, url : url.trim() } }, function(json){
                    if( json.errors.length > 0 )
                    {
                        for( i = 0; i < json.errors.length; i++ )
                        {
                            console.log( "posts.images.upload:" + json.errors[ i ] + " )" );
                        }
                        return false;                
                    }
                    else
                    {
                        if( typeof json.result == 'object' )
                        {
                            //jQuery( element ).parents( ".photosListPhoto" ).remove();
                            jQuery( ".popupForm" ).find( "#tabs_photos" ).html( json.result.tpl );
                        }
                    }
                },"json");
            }
        },
        delete: function( fileID, element )
        {
            if( confirm( "Вы действительно хотите удалить это фото?" ) )
            {
                jQuery.post("/items/async/", { action : "images.delete", fileID }, function(json){
                    if( json.errors.length > 0 )
                    {
                        for( i = 0; i < json.errors.length; i++ )
                        {
                            console.log( "posts.photos::active" + json.errors[ i ] + " )" );
                        }
                        return false;                
                    }
                    else
                    {
                        if( typeof json.result == 'object' )
                        {
                            jQuery( element ).parents( ".photosListPhoto" ).remove();
                        }
                    }
                },"json");
            }
        },
    },
    delete: function( itemID, element, update = true )
    {
        if( confirm( "Вы действительно хотите удалить этот материал?" ) )
        {
            jQuery.post("/items/async/", { action : "delete", itemID }, function(json){
                if( json.errors.length > 0 )
                {
                    for( i = 0; i < json.errors.length; i++ )
                    {
                        console.log( "posts::delete" + json.errors[ i ] + " )" );
                    }
                    return false;                
                }
                else
                {
                    if( typeof json.result == 'object' )
                    {
                        setTimeout(function(){
                            jQuery( element ).parents( ".block-w-material" ).remove();
                            if( update ) {
                                loadAsync();
                            }
                        }, 1000);
                    }
                }
            }, "json");
        }
    },
    edit: function( itemID )
    {
        jQuery.post("/items/detail/", { itemID }, function(data){
            jQuery.fancybox.open( data, { touch: false } );
        }, "json");
    },
    save: function()
    {
        var block = jQuery(".popupForm");
        jQuery.post("/items/async/", { action : "save", userData : addToArray(block.find(".user-data")) }, function(json){
            if( json.errors.length > 0 )
            {
                for( i = 0; i < json.errors.length; i++ )
                {
                    console.log( "posts::delete" + json.errors[ i ] + " )" );
                }
                return false;                
            }
            else
            {
                if( typeof json.result == 'object' )
                {
                    jQuery.fancybox.close();
                    setTimeout(function(){
                        loadAsync();
                    }, 1000);
                }
            }
        }, "json");
    },
}