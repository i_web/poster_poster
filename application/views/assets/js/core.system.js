core = {
    console_log: function( action = "default_action", result = "default_result" )
    {
        console.log( action + "( " + JSON.stringify( result ) + " )" );
    },
    application: {
        create: function()
        {
            jQuery.post("/applications/async/", { action : "create" }, function(json){
                if( json.errors.length > 0 )
                {
                    for( i = 0; i < json.errors.length; i++ )
                    {
                        console.log( "applications.create( " + json.errors[ i ] + " )" );
                    }
                    return false;                
                }
                else
                {
                    if( typeof json.result == 'object' )
                    {
                        jQuery.fancybox.open( json.result.tpl, { touch: false } );
                    }
                }
            },"json");
        },
        save: function()
        {
            var block = jQuery(".popupForm");
            jQuery.post("/applications/async/", { action : "save", userData : addToArray( block.find( ".user-data" ) ) }, function(json){
                if( json.errors.length > 0 )
                {
                    for( i = 0; i < json.errors.length; i++ )
                    {
                        console.log( "applications:save( " + JSON.stringify( json.errors[ i ] ) + " )" );
                        //alert( "applications:save( " + JSON.stringify( json.errors[ i ] ) + " )" );
                    }
                    return false;                
                }
                else
                {
                    if( typeof json.result == 'object' )
                    {
                        jQuery.fancybox.close();
                        setTimeout(function(){
                            loadAsync();
                        }, 1000);
                    }
                }
            }, "json");
        },
        edit: function( itemID )
        {
            jQuery.post("/applications/async/", { action : "edit", itemID }, function(json){
                if( json.errors.length > 0 )
                {
                    for( i = 0; i < json.errors.length; i++ )
                    {
                        console.log( "applications.edit:" + json.errors[ i ] + " )" );
                    }
                    return false;                
                }
                else
                {
                    if( typeof json.result == 'object' )
                    {
                        jQuery.fancybox.open( json.result.tpl, { touch: false } );
                    }
                }
            },"json");
        },
        delete: function( itemID )
        {
            if( confirm( "Вы действительно хотите удалить приложение?" ) )
            {
                jQuery.post("/applications/async/", { action : "delete", itemID }, function(json){
                    if( json.errors.length > 0 )
                    {
                        for( i = 0; i < json.errors.length; i++ )
                        {
                            console.log( "posts:delete:" + json.errors[ i ] + " )" );
                        }
                        return false;                
                    }
                    else
                    {
                        if( typeof json.result == 'object' )
                        {
                            loadAsync();
                        }
                    }
                }, "json");
            }
        },
    },
}