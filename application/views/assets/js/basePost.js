basePost = {
    baseID: function()
    {
        return jQuery("input[type=hidden]#baseID").val();
    },
    getPhotos: function( itemID )
    {
        jQuery.post("/ajax/base/", { action : "base.posts.photo.get", userData : { "key" : "itemID", "value" : itemID } }, function(data){
        if(data.error == true)
        {
            alert(data.response);
            return false;
        }
        else
        {
            return data.response;
        }
        },"json");
    },
    uploadPhoto: function( itemID )
    {
        var photoURL = prompt( "Введите прямой url к новому фото", itemID );
        
        jQuery.post("/ajax/base/", { action : "base.posts.photo.upload", userData : {
            "baseID" : this.baseID(), "itemID" : itemID, "photoURL" : photoURL } }, function(data){
        if(data.error == true)
        {
            alert(data.response);
            return false;
        }
        else
        {
            console.log( data.response );
            jQuery("ul.photosList").append("\
            <li class='photosListPhoto fl-left' id='"+data.response[0].imageID+"'>\
            <img src='http://upload.poster.web/"+data.response[0].local_path+"' width='405' height='300' />\
            <div>\
            <a href='javascript: void(0)'>"+data.response[0].width+"x"+data.response[0].height+"</a>\
            <br />\
            <a href='javascript: void(0)' onclick='basePost.deletePhoto( "+data.response[0].imageID+" )'>Удалить изображение</a>\
            </div>\
            </li>\
            ");
        }
        },"json");
    },
    deletePhoto: function( photoID )
    {
        if( confirm( "Вы действительно хотите удалить это фото?" ) )
        {
            jQuery.post("/ajax/base/", { action : "base.posts.photo.delete", photoID : photoID }, function(data){
                if(data.error == true)
                {
                    alert(data.response);
                    return false;
                }
                if(data.error == false)
                {
                    jQuery("li.photosListPhoto#"+photoID).remove();
                }
            },"json");
        }
    },
    reload: function()
    {
        alert( this.baseID() );
    },
    clearSearchResults: function()
    {
        window.history.pushState(null,null, urlModify( 'remove', { 'key' : 'page' } ) );
        window.history.pushState(null,null, urlModify( 'remove', { 'key' : 'stext' } ) );
        loadAsync();
    },
    active: function( itemID, element )
    {
        jQuery.post("/items/async/", { action : "active", itemID : itemID }, function(json){
            if( json.errors.length > 0 )
            {
                for( i = 0; i < json.errors.length; i++ )
                {
                    console.log( "basePosts::active" + json.errors[ i ] + " )" );
                }
                return false;                
            }
            else
            {
                if( typeof json.result == 'object' )
                {
                    jQuery( element ).val( json.result.active == 1 ? 'Включено' : 'Отключено' );
                }
            }
        },"json");
    },
    edit: function( itemID )
    {
        corePopup.get({
            'title' : 'Редактируем материал',
            'popup' : 'bases/tabsPosts/post.edit',
            'itemID' : itemID,
            'css' : { 'width' : '1000px' }
        });
    },
    save: function()
    {
        var block = jQuery(".basePostEditForm");
        if(checkRequired(block.find(".required")) == true)
        {
            jQuery.ajax({
                type: "POST",
                url: "/ajax/base/",
                data: { action : "base.post.save", userData : addToArray(block.find(".user-data")) },
                dataType: "json",
                success: function(data)
                {
                    if(data.error == true)
                    {
                        alert(data.response);
                        return false;
                    }
                    if(data.error == false)
                    {
                        corePopup.close();
                    }
                }
            });
        }
    },
    delete: function( itemID )
    {
        if( confirm( "Вы действительно хотите удалить данный материал?" ) )
        {
            jQuery.ajax({
                type: "POST",
                url: "/ajax/base/",
                data: { action : "base.post.delete", itemID : itemID },
                dataType: "json",
                success: function(data)
                {
                    if(data.error == true)
                    {
                        alert(data.response);
                        return false;
                    }
                    if(data.error == false)
                    {
                        jQuery( 'li.post-itemID#'+itemID ).remove();
                        flashNotify('#'+itemID+' Пост успешно удален',2000);
                    }
                }
            });
        }
    },
}