bookmarks = {
    view: function( itemID )
    {
        window.history.pushState(null,null, "/bookmarks/view/" + itemID + "/" );
        loadAsync();
    },
    index: function()
    {
        window.history.pushState(null,null, "/bookmarks/list/" );
        loadAsync();
    },
}