baseConfig = {
    save: function()
    {
        coreRequest( "base.config.save" );
    },
    showTags: function()
    {
        corePopup.get({
            'title' : 'Выбрать теги',
            'popup' : 'bases/tabsIndex/showTags',
            'baseID' : jQuery("input[type=hidden]#baseID").val(),
            'css' : { 'width' : '800px' }
        });
    },
    saveTags: function()
    {
        corePopup.close();
        jQuery("#baseConfig-tagsCriteria").val( checkList( "ul.checkList li.checked" ) );
    },
}