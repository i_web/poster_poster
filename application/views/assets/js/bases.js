bases = {
    groups: {
        get: function( baseID )
        {
            jQuery.post("/bases/async/", { action : "bases.groups.get", baseID }, function(json){
                if( json.errors.length > 0 )
                {
                    for( i = 0; i < json.errors.length; i++ )
                    {
                        console.log( "bases.groups.get( " + JSON.stringify( json.errors[ i ] ) + " )" );
                    }
                    return false;                
                }
                else
                {
                    if( typeof json.result == 'object' )
                    {
                        jQuery.fancybox.open( json.result.tpl, { touch: false } );
                    }
                }
            },"json");
        },
        save: function()
        {
            
        },
    },
    edit: {
        save: function()
        {
            var block = jQuery(".popupForm");
            jQuery.post("/bases/async/", { action : "edit::save", userData : addToArray( block.find( ".user-data" ) ) }, function(json){
                if( json.errors.length > 0 )
                {
                    for( i = 0; i < json.errors.length; i++ )
                    {
                        console.log( "bases:edit.save( " + JSON.stringify( json.errors[ i ] ) + " )" );
                    }
                    return false;                
                }
                else
                {
                    if( typeof json.result == 'object' )
                    {
                        jQuery.fancybox.close();
                        setTimeout(function(){
                            loadAsync();
                        }, 1000);
                    }
                }
            }, "json");
        },
        get: function( itemID )
        {
            jQuery.post("/bases/async/", { action : "edit::get", itemID }, function(json){
                if( json.errors.length > 0 )
                {
                    for( i = 0; i < json.errors.length; i++ )
                    {
                        console.log( "bases.edit.get( " + JSON.stringify( json.errors[ i ] ) + " )" );
                    }
                    return false;                
                }
                else
                {
                    if( typeof json.result == 'object' )
                    {
                        jQuery.fancybox.open( json.result.tpl, { touch: false } );
                    }
                }
            },"json");
        },
    },
    create: {
        save: function()
        {
            var block = jQuery(".popupForm");
            jQuery.post("/bases/async/", { action : "create::save", userData : addToArray( block.find( ".user-data" ) ) }, function(json){
                if( json.errors.length > 0 )
                {
                    for( i = 0; i < json.errors.length; i++ )
                    {
                        console.log( "bases:create.save( " + JSON.stringify( json.errors[ i ] ) + " )" );
                    }
                    return false;                
                }
                else
                {
                    if( typeof json.result == 'object' )
                    {
                        jQuery.fancybox.close();
                        setTimeout(function(){
                            loadAsync();
                        }, 1000);
                    }
                }
            }, "json");
        },
        get: function()
        {
            jQuery.post("/bases/async/", { action : "create::get" }, function(json){
                if( json.errors.length > 0 )
                {
                    for( i = 0; i < json.errors.length; i++ )
                    {
                        console.log( "bases.create.get( " + json.errors[ i ] + " )" );
                    }
                    return false;                
                }
                else
                {
                    if( typeof json.result == 'object' )
                    {
                        jQuery.fancybox.open( json.result.tpl, { touch: false } );
                    }
                }
            },"json");
        },
    },
}