baseBookMark = {
    create: function()
    {
        var block = jQuery(".baseAddBookMarkForm");
        if(checkRequired(block.find("input.required")) == true)
        {
            jQuery.ajax({
                type: "POST",
                url: "/ajax/base/",
                data: { action : "base.bookmark.create", userData : addToArray(block.find("input.user-data")) },
                dataType: "json",
                success: function(data)
                {
                    if(data.error == true)
                    {
                        alert(data.response);
                        return false;
                    }
                    if(data.error == false)
                    {
                        corePopup.close();
                        setTimeout(function(){
                            window.location.reload();
                        },1000);
                    }
                }
            });
        }
    },
    update: function( baseID )
    {
        jQuery.ajax({
            type: "POST",
            url: "/ajax/base/",
            data: { action : "base.bookmark.update", baseID : baseID },
            dataType: "json",
            beforeSend: function()
            {
                flashLoader('open',0);
            },
            success: function(data)
            {
                if(data.error == true)
                {
                    flashLoader('close',0);
                    alert(data.response);
                    return false;
                }
                if(data.error == false)
                {
                    flashLoader('close',0);
                    loadAsync();
                }
            }
        });
    },
    add: function( baseID )
    {
        corePopup.get({
            'title' : 'Добавить закладку к базе',
            'popup' : '/bases/tabsBookMarks/add',
            'baseID' : baseID,
            'css' : { 'width' : '500px' }
        
        });
    },
    import: function( itemID, baseID )
    {
        jQuery.ajax({
            type: "POST",
            url: "/ajax/base/",
            data: { action : "base.bookmark.import", itemID : itemID, baseID : baseID },
            dataType: "json",
            beforeSend: function()
            {
                flashLoader('open',0);
            },
            success: function(data)
            {
                if(data.error == true)
                {
                    flashLoader('close',0);
                    alert(data.response);
                    return false;
                }
                if(data.error == false)
                {
                    flashLoader('close',0);
                    basePost.edit( data.response );
                }
            }
        });
    }
}