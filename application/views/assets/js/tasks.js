tasks = {
    detail: {
        time: function()
        {
            
        },
        generate: function( taskID )
        {
            flashLoader("open", "");
            jQuery.post("/tasks/async/", { action : "generate", taskID }, function(data){
                flashLoader("close", "");
                loadAsync();
            }, "json");
        },
        clear: function( taskID )
        {
            jQuery.post("/tasks/async/", { action : "clear", taskID }, function(data){
                loadAsync();
            }, "json");
        },
        delete: function( taskID )
        {
            if( confirm( "Вы действительно хотите удалить задачу [ " + taskID + " ] ?" ) )
            {
                jQuery.post("/tasks/async/", { action : "delete", taskID }, function(data){
                    window.history.pushState(null,null, '/tasks/list/' );
                    loadAsync();
                }, "json");
            }
        },
        control: function( active, taskID )
        {
            jQuery.post("/tasks/async/", { action : "control", taskID, active }, function(data){
                loadAsync();
            }, "json");
        },
    },
    create: function( popup = true, save = false )
    {
        if( popup )
        {
            jQuery.post("/tasks/create/", { popup }, function(data){
                jQuery.fancybox.open( data, { touch: false } );
            }, "json");
        }
        else
        {
            window.history.pushState(null,null, '/tasks/create/' );
            loadAsync();
        }
    },
    save: function()
    {
        var arrData = serializeArray( jQuery( ".tasks-save-form" ).find( "input,textarea" ).serializeArray() );
        jQuery.post("/tasks/create/", { arrData }, function() {
            jQuery.fancybox.close();
            //window.history.pushState(null,null, '/tasks/list/' );
            setTimeout(function(){
                loadAsync();
            },500);
        });
    },
    delete: function()
    {
        var arrData = [];
        var items = jQuery( ".tasksList .taskListElement.ui-selectee.ui-selected" );
        if( items.size() > 0 )
        {
            items.each(function(){
                arrData.push( this.id );
            });
            jQuery.post("/tasks/delete/", { arrData }, function( json ) {
                if( typeof json.errors == 'object' )
                {
                    if( json.errors.length > 0 )
                    {
                        for( i = 0; i < json.errors.length; i++ )
                        {
                            console.log( json.errors[ i ] );
                        }
                    }
                }
                if( typeof json.result == 'object' )
                {
                    for( i = 0; i < json.result.length; i++ )
                    {
                        jQuery( ".tasksList .taskListElement.ui-selectee.ui-selected#" + json.result[ i ] ).remove();
                    }
                }
            }, "json" );
        }
        else
        {
            alert( "Ничего не выбрано" );
            return false;
        }
    },
    viewTask: function( taskID )
    {
        window.history.pushState(null,null, '/tasks/detail/' + taskID + '/' );
        loadAsync();
    },
    viewList: function()
    {
        window.history.pushState(null,null, '/tasks/list/' );
        loadAsync();
    },
}