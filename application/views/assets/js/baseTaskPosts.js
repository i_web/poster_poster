baseTaskPosts = {
    exportForce: function( baseID, taskID, itemID )
    {
        coreRequest( "base.task.posts.exportForce", { baseID : baseID, taskID : taskID, itemID : itemID } );
    },
    delete: function( itemID )
    {
        var arrData = addToArray( jQuery(".itemList.active input.user-data") );
        if( confirm( "Вы действительно хотите удалить материал из очереди?" ) )
        {
            coreRequest( "base.task.posts.delete", { itemID : itemID });            
        }
    },
    export: function( call = false )
    {
        coreRequest( "base.task.export", { call : call });        
    },
    generate: function( call = false )
    {
        switch( call )
        {
            default:
            coreRequest( "base.task.posts.generate" );
            break;
            
            case 'time':
            corePopup.get({
                'title' : 'Рассчитать время',
                'popup' : '/bases/tabsTask/generateTime',
                arrData : addToArray( jQuery(".itemList.active input.user-data") ),
                'css' : { 'width' : '800px' }
            });
            break;
        }
    },
    clear: function()
    {
        coreRequest( "base.task.posts.delete" );        
    }
}