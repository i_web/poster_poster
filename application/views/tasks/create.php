<script type="text/javascript">
jQuery(document).ready(function(){
    
    jQuery( ".date-range-picker" ).daterangepicker({
        autoUpdateInput: false,
        autoApply: true,
        alwaysShowCalendars: true,
        locale: {
            format: 'DD.MM.YYYY',
            separator: ' - ',
            applyLabel: 'Применить',
            cancelLabel: 'Отмена',
            fromLabel: 'От',
            toLabel: 'До',
            customRangeLabel: 'Ручной',
            daysOfWeek: [
            "Вс",
            "Пн",
            "Вт",
            "Ср",
            "Чт",
            "Пт",
            "Сб"
            ],
            "monthNames": [
            "Январь",
            "Февраль",
            "Март",
            "Апрель",
            "Май",
            "Июнь",
            "Июль",
            "Август",
            "Сентябрь",
            "Октябрь",
            "Ноябрь",
            "Декабрь"
            ],
            firstDay: 1
        },
        ranges: {
            'Сегодня': [moment(), moment()],
            'Вчера': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Последние 7 Days': [moment().subtract(6, 'days'), moment()],
            'Последние 30 Days': [moment().subtract(29, 'days'), moment()],
            'Этот месяц': [moment().startOf('month'), moment().endOf('month')],
            'Прошедший месяц': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
    }, function(start_date, end_date) {
        this.element.val(start_date.format('DD.MM.YYYY')+' - '+end_date.format('DD.MM.YYYY'));
    });
    
    /*
    jQuery( ".time-picker" ).datetimepicker({
    	datepicker : false,
        dayOfWeekStart: 1,
    	format : 'H:i',
    	step : 5,
        mask : '29:59'
    });
    */
    jQuery( ".time-picker" ).timepicker({
        timeFormat: 'HH:mm',
        interval: 5,
        minTime: '00:00',
        maxTime: '23:55',
        //defaultTime: '04:00',
        //startTime: '04:00',
        dynamic: false,
        dropdown: true,
        scrollbar: true
    });
    
    jQuery( ".date-time-picker" ).datetimepicker({
    	timepicker : false,
    	format : 'd-m-Y',
        dayOfWeekStart: 1,
        mask : '00-00-0000',
        //minDate: '+1970/01/01'
    });
    
    /*
    jQuery( ".datepicker" ).datepicker({
        autoSize: false,
        altFormat: 'dd.mm.yy',
        dateFormat : 'dd.mm.yy',
        firstDay: 1,
        language : 'ru-RU',
        monthNames : ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
        dayNamesMin : ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
    });
    */
    
});
</script>
<style type="text/css">
.ui-timepicker-container.ui-timepicker-standard {
    z-index: 100000 !important;
}
.daterangepicker {
    z-index: 100000 !important;
}
</style>
<div class="simple-table popupForm tasks-save-form" style="width: 500px;">

<h3 style="text-align: center;">Создать задачу:</h3>


<div class="container-fluid container-fullw bg-white">
    <div class="row">
		<div class="col-md-12">
            <div class="form-group">
                <input class="user-data form-control date-range-picker" name="runDay" type="text" readonly="true" required="true" placeholder="Диапазон дат" />
            </div>
            
            <div class="form-group">
                <input class="user-data form-control time-picker" name="runTime" type="text" value="<?php echo !empty( $this->baseData['runTimeStart'] ) ? $this->baseData['runTimeStart'] : Date("H:i"); ?>" required="true" placeholder="Время старта" />
            </div>
        </div>
    </div>
</div>

<div class="container-fluid container-fullw bg-white">
    <div class="row">
        <div class="col-md-12" style="text-align: center;">
            <input type="button" value="Сохранить" onclick="tasks.save()" class="btn btn-wide btn-success" style="width: 100%;" />
        </div>
    </div>
</div>