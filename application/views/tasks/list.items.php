<input type="button" class="btn btn-primary" value="Создать задачу" onclick="tasks.create()" />
<input type="button" class="btn btn-primary" value="Удалить выбранные" onclick="tasks.delete()" />

<style type="text/css">
/*
.tasksItems.isRunning {
    border: 1px #0088cc solid;
    color: #ffffff;
    background-color: #00a0d0;
    background-image: -moz-linear-gradient(top,#00aad6,#0090c7);
    background-image: -webkit-gradient(linear,0 0,0 100%,from(#00aad6),to(#0090c7));
    background-image: -webkit-linear-gradient(top,#00aad6,#0090c7);
    background-image: -o-linear-gradient(top,#00aad6,#0090c7);
    background-image: linear-gradient(to bottom,#00aad6,#0090c7);
    background-repeat: repeat-x;
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff00aad6', endColorstr='#ff0090c7', GradientType=0);
}
.tasksList .isRunning:hover {
    -moz-box-shadow:0 0 1px 1px rgba(255,255,255,0.4) inset, 0 0 6px 4px rgba(90,210,255,0.5);
	-webkit-box-shadow:0 0 1px 1px rgba(255,255,255,0.4) inset, 0 0 6px 4px rgba(90,210,255,0.5);
	box-shadow:0 0 1px 1px rgba(255,255,255,0.4) inset, 0 0 6px 4px rgba(90,210,255,0.5);
	text-decoration:none;
}
*/
.tasksList .ui-selected.taskListElement {
    -moz-box-shadow:0 0 1px 1px rgba(255,255,255,0.4) inset, 0 0 6px 4px rgba(90,210,255,0.5);
	-webkit-box-shadow:0 0 1px 1px rgba(255,255,255,0.4) inset, 0 0 6px 4px rgba(90,210,255,0.5);
	box-shadow:0 0 1px 1px rgba(255,255,255,0.4) inset, 0 0 6px 4px rgba(90,210,255,0.5);
	text-decoration:none;
}
</style>
<script type="text/javascript">
jQuery( document ).ready(function(){
    jQuery( ".tasksList" ).selectable({
        distance: 0,
        cancel: "a"
    });
});
</script>
<br /><br />

<div class="row tasksList">
<?php if( $model ): ?>

    <?php foreach( $model as $k => $v ): ?>
    
    <div class="col-sm-6 col-md-2 taskListElement" id="<?=$v->id?>">
		<div class="thumbnail">
			<div class="caption">
				<h4 style="text-align: center;"><?=date( "d-m-Y H:i", $v->runTime )?></h4>
                <h4 style="text-align: center;"><?=$v->itemsCount?></h4>
				<p>
                    <?php if( $v->active == 1 ): ?>
                    <a href="javascript: void(0)" onclick="tasks.viewTask( <?=$v->id?> )" style="width: 100%;" class="btn btn-o btn-wide btn-success">В работе</a>
                    <?php elseif( $v->active == 0 ): ?>
                    <a href="javascript: void(0)" onclick="tasks.viewTask( <?=$v->id?> )" style="width: 100%;" class="btn btn-o btn-wide btn-danger">Вне работы</a>
                    <?php endif; ?>
				</p>
			</div>
		</div>
	</div>
    
    <?php endforeach; ?>
    
<?php else: ?>
<div style="text-align: center;"><h3>Список задач пуст</h3></div>
<?php endif; ?>
</div>