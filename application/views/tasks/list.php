<div class="col-sm-12">
<?php if( isset( $this->baseID ) && $this->baseID > 0 ): ?>
<div class="item">
<div id="asyncLoadPosts">
<?php $this->renderPartial( "//tasks/list.items", array( "model" => $model ) ); ?>
</div>

</div>
<?php else: ?>
<div class="item"><strong>Не выбрана база</strong></div>
<?php endif; ?>
</div>