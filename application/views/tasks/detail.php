<div class="col-sm-12">

<h3 style="text-align: right;" class="mainTitle"><?=date( "d.m.Y H:i", $model->runTime )?></h3>

<?php if( isset( $this->baseID ) && $this->baseID > 0 ): ?>
<div class="btn-group">
	<a class="btn btn-wide btn-info" href="javascript: void(0)" onclick="tasks.viewList()">
		Назад
	</a>
    
    <?php if( $model->active == 1 ): ?>
	<a class="btn btn-wide btn-success" href="javascript: void(0)">
		Сгенерировать
	</a>
    <?php elseif( $model->active == 0 ): ?>
    <a class="btn btn-wide btn-success" href="javascript: void(0)" onclick="tasks.detail.generate( <?=$_GET[ "id" ]?> )">
		Сгенерировать
	</a>
    <?php endif; ?>
    
    <!-- CONTROL -->
    <?php if( $model->active == 1 ): ?>
    <a class="btn btn-wide btn-danger" href="javascript: void(0)" onclick="tasks.detail.control( 0, <?=$_GET[ "id" ]?> )">
		Остановить
	</a>
    <?php elseif( $model->active == 0 ): ?>
    <a class="btn btn-wide btn-success" href="javascript: void(0)" onclick="tasks.detail.control( 1, <?=$_GET[ "id" ]?> )">
		Запустить
	</a>
    <?php endif; ?>
    <!-- CONTROL -->
    
    <!-- CLEAR TASK ITEMS -->
    <?php if( $model->active == 1 ): ?>
    <a class="btn btn-wide btn-azure btn-disabled" href="javascript: void(0)">
		Очистить
	</a>
    <?php elseif( $model->active == 0 ): ?>
    <a class="btn btn-wide btn-azure" href="javascript: void(0)" onclick="tasks.detail.clear( <?=$_GET[ "id" ]?> )">
		Очистить
	</a>
    <?php endif; ?>
    <!-- CLEAR TASK ITEMS -->
    
    <!-- DELETE TASK -->
    <?php if( $model->active == 1 ): ?>
    <a class="btn btn-wide btn-warning" href="javascript: void(0)">
		Удалить
	</a>
    <?php elseif( $model->active == 0 ): ?>
    <a class="btn btn-wide btn-warning" href="javascript: void(0)" onclick="tasks.detail.delete( <?=$_GET[ "id" ]?> )">
		Удалить
	</a>
    <?php endif; ?>
    <!-- DELETE TASK -->
    
</div>

<br /><br />

<div class="col-sm-12">
<?php if( count( $model->items ) ): ?>
<?php $this->renderPartial('//tasks/detail.items', array( 'model' => $model )); ?>
<?php else: ?>
<div class="alert alert-danger" style="text-align: center;">
	<strong>Нет материала для отображения</strong>
</div>
<?php endif; ?>
</div>
<?php else: ?>
<div class="alert alert-danger" style="text-align: center;">
	<strong>Не выбрана база</strong>
</div>
<?php endif; ?>
</div>