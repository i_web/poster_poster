<br /><br />

<div class="row">
<?php $i = 1; ?>
<?php foreach( $model->items as $k => $v ): ?>
    <div class="col-sm-12">
		<div class="panel panel-white">
			<div class="panel-heading border-light">
				<h4 class="panel-title"><a href="javascript: void( 0 )" onclick="posts.edit( <?=$v->item->id?> )"><?=$v->item->title?></a></h4>
				<ul class="panel-heading-tabs border-light">
					<li>
						<strong><?=date("d-m-Y H:i:s", $v->runTime)?></strong>
					</li>
                    <li>
                    <?php if( $v->item->published == 1 ): ?>
                        <input type="button" class="btn btn-wide btn-success" value="Включено" onclick="posts.list.active( <?=$v->item->id?>, this )" />
                    <?php elseif( $v->item->published == 0 ): ?>
                        <input type="button" class="btn btn-wide btn-danger" value="Отключено" onclick="posts.list.active( <?=$v->item->id?>, this )" />
                    <?php endif; ?>
                    </li>
					<li class="panel-tools">
                        <a data-original-title="Удалить из очереди" id="<?=$v->item->id?>" data-toggle="tooltip" data-placement="top" class="btn btn-transparent btn-sm panel-refresh" href="javascript: void(0)">
                            <i class="ti-close"></i>
                        </a>
						<a data-original-title="Редактировать" data-toggle="tooltip" data-placement="top" class="btn btn-transparent btn-sm panel-refresh" href="javascript: void(0)" onclick="posts.edit( <?=$v->item->id?> )">
                            <i class="ti-pencil"></i>
                        </a>
					</li>
				</ul>
			</div>
            
			<div class="panel-body">
                <div class="col-sm-12">
                    <div class="panel panel-white collapses" id="panel5" style="margin-bottom: 0px;">
						<div class="panel-heading">
							<h4 class="panel-title text-primary"><a class="panel-collapse" href="#" style="display: block;">Описание</a></h4>
							<div class="panel-tools">
								<a data-original-title="Collapse" data-toggle="tooltip" data-placement="top" class="btn btn-transparent btn-sm panel-collapse" href="#"><i class="ti-minus collapse-off"></i><i class="ti-plus collapse-on"></i></a>
							</div>
						</div>
						<div class="panel-body" style="display: none;">
                            <p style="unicode-bidi: embed; font-family: monospace; white-space: pre-line;">
                            <?=trim($v->item->text)?>
                            </p>
						</div>
					</div>
                </div>
			</div>
            
            <?php if( isset( $v->item->images ) && count( $v->item->images )): ?>
            <div class="panel-body">
                <div class="col-sm-12">
					<div class="panel panel-white collapses" id="panel5" style="margin-bottom: 0px;">
						<div class="panel-heading">
							<h4 class="panel-title text-primary"><a class="panel-collapse" href="#" style="display: block;">Медиа ( <?=count( $v->item->images )?> )</a></h4>
							<div class="panel-tools">
								<a data-original-title="Collapse" data-toggle="tooltip" data-placement="top" class="btn btn-transparent btn-sm panel-collapse" href="#"><i class="ti-minus collapse-off"></i><i class="ti-plus collapse-on"></i></a>
							</div>
						</div>
						<div class="panel-body" style="display: none;">
                            <?php foreach( $v->item->images as $k2 => $v2 ): ?>
                                <div class="col-sm-6 col-md-3" id="<?=$v2->id?>">
                                    <a data-fancybox="gallery_<?=$v->item->id?>" style="text-decoration: none;" href="<?php echo Yii::app()->params['upload_url'].$v2->local_path; ?>">
                                        <div class="thumbnail" style="background: url(<?php echo Yii::app()->params['upload_url'].$v2->local_path; ?>); background-size: cover; width: 250px; height: 250px;">
                                        </div>
                                    </a>
                                </div>
                            <?php endforeach; ?>
						</div>
					</div>
				</div>
            </div>
            <?php endif; ?>
		</div>
	</div>
<?php $i++; ?>
<?php endforeach; ?>
</div>