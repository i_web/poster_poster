<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!--[if IE 8]><html class="ie8" lang="ru"><![endif]-->
<!--[if IE 9]><html class="ie9" lang="ru"><![endif]-->
<!--[if !IE]><!-->
<html xmlns="http://www.w3.org/1999/xhtml">
<!--<![endif]-->
<!-- start: HEAD -->
<head>
<title>{CHtml::encode($this->pageTitle)}</title>
<!-- start: META -->
<!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="black" />
<meta content="" name="description" />
<meta content="" name="author" />


<!-- end: META -->

<!-- start: MAIN CSS -->
<input type="hidden" value="{$this->assets["path"]}" id="assetsPath" />

{getClientScript path=$this->assets["path"] file="/bootstrap/vendor/bootstrap/css/bootstrap.min.css"}
{getClientScript path=$this->assets["path"] file="/bootstrap/vendor/fontawesome/css/font-awesome.min.css"}
{getClientScript path=$this->assets["path"] file="/bootstrap/vendor/themify-icons/themify-icons.min.css"}
{getClientScript path=$this->assets["path"] file="/bootstrap/vendor/animate.css/animate.min.css"}
{getClientScript path=$this->assets["path"] file="/bootstrap/vendor/perfect-scrollbar/perfect-scrollbar.min.css"}
{getClientScript path=$this->assets["path"] file="/bootstrap/vendor/switchery/switchery.min.css"}
{getClientScript path=$this->assets["path"] file="/bootstrap/assets/css/styles.css"}
{getClientScript path=$this->assets["path"] file="/bootstrap/assets/css/plugins.css"}
{getClientScript path=$this->assets["path"] file="/bootstrap/assets/css/themes/theme-1.css" options=[ 'id' => 'skin_color' ]}

{getClientScript path=$this->assets["path"] file="/bootstrap/vendor/jquery/jquery-1.10.2.min.js"}
{getClientScript path=$this->assets["path"] file="/bootstrap/vendor/bootstrap/js/bootstrap.min.js"}

{getClientScript path=$this->assets["path"] file="/bootstrap/vendor/jquery-ui/jquery-ui-1.10.1.custom.min.js"}
{getClientScript path=$this->assets["path"] file="/bootstrap/vendor/jquery-ui/jquery-ui-1.10.1.custom.min.css"}

{getClientScript path=$this->assets["path"] file="/bootstrap/vendor/Chart.js/Chart.min.js"}
{getClientScript path=$this->assets["path"] file="/bootstrap/vendor/jquery.sparkline/jquery.sparkline.min.js"}
{*getClientScript path=$this->assets["path"] file="/bootstrap/assets/js/index.js"*}

{getClientScript path=$this->assets["path"] file="/bootstrap/vendor/bootstrap-datepicker/bootstrap-datepicker.css"}
{getClientScript path=$this->assets["path"] file="/bootstrap/vendor/bootstrap-datepicker/bootstrap-datepicker.js"}
{getClientScript path=$this->assets["path"] file="/bootstrap/vendor/bootstrap-datepicker/locales/bootstrap-datepicker.ru.min.js"}

{getClientScript path=$this->assets["path"] file="/libs/fancybox3/jquery.fancybox.js"}
{getClientScript path=$this->assets["path"] file="/libs/fancybox3/jquery.fancybox.css"}

{getClientScript path=$this->assets["path"] file="/libs/jquery/jquery.maskedinput.js"}

{getClientScript path=$this->assets["path"] file="/bootstrap/vendor/modernizr/modernizr.js"}
{getClientScript path=$this->assets["path"] file="/bootstrap/vendor/jquery-cookie/jquery.cookie.js"}
{getClientScript path=$this->assets["path"] file="/bootstrap/vendor/perfect-scrollbar/perfect-scrollbar.min.js"}
{getClientScript path=$this->assets["path"] file="/bootstrap/vendor/switchery/switchery.min.js"}
{getClientScript path=$this->assets["path"] file="/bootstrap/vendor/select2/select2.min.js"}


{getClientScript path=$this->assets["path"] file="/bootstrap/assets/js/main.js"}
{getClientScript path=$this->assets["path"] file="/bootstrap/assets/js/ui-elements.js"}

{getClientScript path=$this->assets["path"] file="/bootstrap/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js"}
{getClientScript path=$this->assets["path"] file="/bootstrap/vendor/twbs-pagination/jquery.twbsPagination.min.js"}
{getClientScript path=$this->assets["path"] file="/bootstrap/vendor/bootstrap-rating/bootstrap-rating.min.js"}

{getClientScript path=$this->assets["path"] file="/libs/jquery/jquery.slimscroll.js"}

{getClientScript path=$this->assets["path"] file="/libs/jquery/jquery.tipsy.js"}
{getClientScript path=$this->assets["path"] file="/libs/jquery/jquery.tipsy.css"}

{$this->getAssets([
    "/js/core.js",
    "/js/core.system.js",
    "/js/core.request.js",
    "/js/basePost.js",
    "/js/tasks.js",
    "/js/posts.js",
    "/js/bases.js",
    "/js/bookmarks.js",
    "/js/tags.js",
    "/libs/corePopup/corePopup.css",
    "/libs/corePopup/corePopup.js"
], true )}

<!-- DateTimePicker -->
{$this->getAssets([
    "/libs/jquery/datetimepicker/jquery.datetimepicker.full.js",
    "/libs/jquery/datetimepicker/jquery.datetimepicker.css"
], true )}
<!-- // DateTimePicker -->

<!-- TimePicker -->
{$this->getAssets([
    "/libs/timePicker/jquery.timepicker.js",
    "/libs/timePicker/jquery.timepicker.css"
], true )}
<!-- // TimePicker -->

<!-- DateRangePicker -->
{$this->getAssets([
    "/libs/dateRangePicker/moment.min.js",
    "/libs/dateRangePicker/daterangepicker.js",
    "/libs/dateRangePicker/daterangepicker.css"
], true )}
<!-- // DateRangePicker -->

</head>
<!-- end: HEAD -->
<body>
{literal}
<script>
	jQuery(document).ready(function() {
		Main.init();
		/*Index.init();*/
        jQuery('.tipsy-tooltip').tipsy();
	});
</script>
{/literal}
{if Yii::app()->user->isGuest}
{$content}
{else}
{include file="application.views.layouts.index" }
{/if}
</body>
</html>