<input type="button" value="Загрузить фото" onclick="posts.photos.upload( <?=$model->id?>, <?=$model->id_base?> )" class="btn btn-primary" />
<br /><br />
<?php if( count( $model->images_l ) ): ?>

    <div class="row photosList">
    
    <?php foreach( $model->images_l as $k => $v ): ?>
        <div class="col-sm-6 col-md-3 photosListPhoto" id="<?=$v->id?>">
			<div class="thumbnail">
				<img src="<?php echo Yii::app()->params['upload_url'].$v->local_path; ?>"  style="max-height: 150px" />
				<div class="caption">
					<p>
                    <code>
                    Размеры: <?=$v->width."x".$v->height?>
                    <br />
                    Hash: <?=$v->hash->hashCode?>
                    <br />
                    Вес: <?=$this->FileSizeConvert( $v->weight )?>
                    </code>
					</p>
					<p>
					<a style="width: 100%;" href="javascript: void(0)" onclick="posts.photos.delete( <?=$v->id?>, this )" class="btn btn-o btn-wide btn-warning">
						Удалить изображение
					</a>
					</p>
				</div>
			</div>
		</div>
    <?php endforeach; ?>
    
    </div>
    
<br /><br />
<?php endif; ?>