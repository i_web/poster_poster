<div class="col-sm-12">
<?php if( isset( $this->baseID ) && $this->baseID > 0 ): ?>
<div class="row">

    <div class="btn-group">
        <a class="btn btn-wide btn-primary" href="javascript: void(0)" onclick="loadAsync()">
    		Обновить страницу
    	</a>
    	<a class="btn btn-wide btn-primary" href="javascript: void(0)" onclick="basePost.clearSearchResults()">
    		Очистить результат поиска
    	</a>
        <a class="btn btn-wide btn-primary" href="javascript: void(0)" onclick="posts.import.url()">
    		Импорт УРЛ
    	</a>
    </div>
    
    <br /><br />
    
    <div class="form-group">
        <input class="form-control" value='<?php echo isset($_GET["stext"]) ? $_GET["stext"] : ""; ?>' type="text" placeholder="Поиск" id="asyncSearchPosts" />
    </div>

<br /><br />

<div id="asyncLoadPosts">
<?php if( $model ): ?>
<?php $this->renderPartial( "//items/list.items", array( "model" => $model, "pages" => $pages ) ); ?>
<?php else: ?>
<div class="alert alert-danger" style="text-align: center;">
	<strong>Данных нет</strong>
</div>
<?php endif; ?>
</div>



</div>
<?php else: ?>
<div class="alert alert-danger" style="text-align: center;">
	<strong>Не выбрана база</strong>
</div>
<?php endif; ?>
</div>