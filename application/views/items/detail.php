<script type="text/javascript">
jQuery(document).ready(function(){
    
    jQuery( ".tags-list a" ).click(function(){
        var elem = jQuery(this);
        var itemID = jQuery(".popupForm input[type=hidden]#itemID").val();
        var baseID = jQuery(".popupForm input[type=hidden]#baseID").val();
        var tagID = jQuery(this).attr( "id" );
        var active = jQuery(this).hasClass( "active" ) ? 1 : 0;
        jQuery.post( "/items/async/", { action : "tags.save", baseID, itemID, tagID }, function(data){
            elem.toggleClass( "active" );
        }, "json" );
    });
    
    jQuery(".photosList").sortable({
        cursor: 'move',
        axis: 'x,y',
        //cancel: '.photosList div',
        update: function(){
            var itemID = jQuery(".popupForm input[type=hidden]#itemID").val();
            var baseID = jQuery(".popupForm input[type=hidden]#baseID").val();
            var sortData = jQuery(this).sortable("toArray");
            jQuery.ajax({
                type: "POST",
                url : "/items/async/",
                data : { action : "images.save_order", itemID : itemID, baseID : baseID, sortData : sortData },
                dataType : "json",
                success : function(data)
                {
                    if( data.error == true )
                    {
                        alert( data.response );
                        return false;
                    }
                }
            });
        }
    });
});
</script>
<div class="simple-table popupForm tasks-save-form" style="width: 90%; min-height: 95% !important; height: auto !important;">
<h3 style="text-align: center;">Редактируем материал:</h3>

<hr />

<div class="container-fluid container-fullw bg-white">
	<div class="row">
		<div class="col-md-12">
			<div class="tabbable">
				<ul class="nav nav-tabs tab-padding tab-space-3 tab-blue" id="myTab4">
					<li class="active">
						<a data-toggle="tab" href="#tabs_index" aria-expanded="true">
							Общее
						</a>
					</li>
                    <li>
						<a data-toggle="tab" href="#tabs_photos" aria-expanded="false">
							Фотографии ( <?=count( $model->images_l )?> )
						</a>
					</li>
                    <li>
						<a data-toggle="tab" href="#tabs_tags" aria-expanded="false">
							Тэги ( <?=count( $model->tags_l )?> )
						</a>
					</li>
                    <li>
						<a data-toggle="tab" href="#tabs_history" aria-expanded="false">
							История публикаций ( <?=count( $model->history_l )?> )
						</a>
					</li>
                    <li>
						<a data-toggle="tab" href="#tabs_dublicates" aria-expanded="false">
							PhotosDublicates ( <?=count( ItemsDublicates::search( $model->id_base, $model->id ) )?> )
						</a>
					</li>
				</ul>
				<div class="tab-content">
					<div id="tabs_index" class="tab-pane fade active in">
					<?php $this->renderPartial("//items/detail.index", array( "model" => $model )); ?>
					</div>
                    <div id="tabs_photos" class="tab-pane fade">
					<?php $this->renderPartial("//items/detail.images", array( "model" => $model )); ?>
					</div>
                    <div id="tabs_tags" class="tab-pane fade">
					<?php $this->renderPartial("//items/detail.tags", array( "model" => $model )); ?>
					</div>
                    <div id="tabs_history" class="tab-pane fade">
					<?php $this->renderPartial("//items/detail.history", array( "model" => $model )); ?>
					</div>
                    <div id="tabs_dublicates" class="tab-pane fade">
					<?php $this->renderPartial("//items/detail.dublicates", array( "model" => $model )); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<hr />

<div class="container-fluid container-fullw bg-white">

<div class="row">
		<div class="col-md-12" style="text-align: center;">
        
        <input style="width: 30%;" type="button" class="btn btn-warning btn-wide" value="Закрыть" onclick="$.fancybox.close()" />
        
        <input style="width: 30%;" type="button" class="btn btn-success btn-wide" value="Сохранить" onclick="posts.save()" />
        
        </div>
        
</div>

</div>

</div>