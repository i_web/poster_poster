<div class="row" style="padding: 20px;">
<?php
$dublicates = ItemsDublicates::search( $model->id_base, $model->id );
?>
<?php if( $dublicates ): ?>
    <?php foreach( $dublicates as $k => $v ): ?>
    <div class="col-sm-12 block-w-material">
		<div class="panel panel-white">
			<div class="panel-heading border-light">
				<h4 class="panel-title"><a href="javascript: void( 0 )" onclick="posts.edit( <?=$v->items->id?> )"><?=$v->items->title?></a></h4>
				<ul class="panel-heading-tabs border-light">
					<li>
						<?=date("d-m-Y H:i:s", $v->items->created)?>
					</li>
                    <li>
                    <?php if( $v->items->published == 1 ): ?>
                        <input type="button" class="btn btn-wide btn-success" value="Включено" onclick="posts.list.active( <?=$v->items->id?>, this )" />
                    <?php elseif( $v->items->published == 0 ): ?>
                        <input type="button" class="btn btn-wide btn-danger" value="Отключено" onclick="posts.list.active( <?=$v->items->id?>, this )" />
                    <?php endif; ?>
                    </li>
					<li class="panel-tools">
                        <a data-original-title="Удалить" data-toggle="tooltip" data-placement="top" class="btn btn-transparent btn-sm panel-refresh" href="javascript: void(0)" onclick="posts.delete( <?=$v->items->id?>, this, false )">
                            <i class="ti-close"></i>
                        </a>
						<a data-original-title="Редактировать" data-toggle="tooltip" data-placement="top" class="btn btn-transparent btn-sm panel-refresh" href="javascript: void(0)" onclick="posts.edit( <?=$v->items->id?> )">
                            <i class="ti-pencil"></i>
                        </a>
					</li>
				</ul>
			</div>
			<div class="panel-body">
				<p style="unicode-bidi: embed; font-family: monospace; white-space: pre-line;">
                <?=trim($v->items->text)?>
                </p>
			</div>
            
            <?php if( isset( $v->items->images ) && count( $v->items->images )): ?>
            <div class="panel-body">
                <div class="col-sm-12">
					<div class="panel panel-white collapses" id="panel5">
						<div class="panel-heading">
							<h4 class="panel-title text-primary"><a class="panel-collapse" href="#" style="display: block;">Медиа ( <?=count( $v->items->images )?> )</a></h4>
							<div class="panel-tools">
								<a data-original-title="Collapse" data-toggle="tooltip" data-placement="top" class="btn btn-transparent btn-sm panel-collapse" href="#"><i class="ti-minus collapse-off"></i><i class="ti-plus collapse-on"></i></a>
							</div>
						</div>
						<div class="panel-body" style="display: none;">
                            <?php foreach( $v->items->images as $k2 => $v2 ): ?>
                                <div class="col-sm-6 col-md-3" id="<?=$v2->id?>">
                                    <a data-fancybox="gallery_<?=$v->id?>" style="text-decoration: none;" href="<?php echo Yii::app()->params['upload_url'].$v2->local_path; ?>">
                                        <div class="thumbnail" style="background: url(<?php echo Yii::app()->params['upload_url'].$v2->local_path; ?>); background-size: cover; width: 250px; height: 250px;">
                                        </div>
                                    </a>
                                </div>
                            <?php endforeach; ?>
						</div>
					</div>
				</div>
            </div>
            <?php endif; ?>
		</div>
	</div>
    <?php endforeach; ?>
<?php else: ?>
<div class="alert alert-danger" style="text-align: center;">
	<strong>Дубликатов нет</strong>
</div>
<?php endif; ?>
</div>