<style type="text/css">
.checkList {
    padding: 0px;
    margin: 0px;
    list-style: none;
    text-align: left;
    height: 480px;
    overflow-y: scroll;
}
</style>
<div class="checkList row" style="padding: 10px;">
<?php $tagsPost = CHtml::listData($model->tags_l, 'id_tags', 'id_item')?>
<?php $tagsList = Tags::listData( $this->baseID ) ?>
<?php $tagsList = is_array( $tagsList ) ? $tagsList : array(); ?>
<?php if( count( $tagsList ) ): ?>
    <div class="list-group tags-list">
        <?php foreach( $tagsList as $k => $v ): ?>
        <a class="list-group-item <?=isset( $tagsPost[ $v->id ] ) ? "active" : ""?>" id="<?=$v->id?>" href="javascript: void(0)">
            <span class="badge"><?=$v->tagsCount?></span>
    		<?php echo $v->clevel > 0 ? str_repeat( "&nbsp;", $v->clevel * 6 ).$v->name : $v->name; ?> ( <?=$v->tagsCount?> )
    	</a>
        <?php endforeach; ?>
    </div>
<?php else: ?>
<div class="alert alert-danger" style="text-align: center;">
	<strong>Не выбрана база</strong>
</div>
<?php endif; ?>
</div>