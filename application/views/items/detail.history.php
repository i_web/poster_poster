
<div class="itemList">
<div style="height: 480px; overflow-y: scroll;">
<table class="table table-bordered table-hover">
<thead>
<th>socialID</th>
<th>runTime</th>
<th>created</th>
</thead>
<tbody>
<?php if( count( $model->history_l ) ): ?>
    <?php foreach( $model->history_l as $k => $v ): ?>
        <tr class="">
        <td><?=$v->id_item_social?></td>
        <td><?=date( "d-m-Y H:i:s", $v->runTime )?></td>
        <td><?=date( "d-m-Y H:i:s", $v->created )?></td>
        </tr>
    <?php endforeach; ?>
<?php else: ?>
<tr>
<td colspan="3">Данных нет</td>
</tr>
<?php endif; ?>
</tbody>
</table>
</div>
</div>