<div class="form-group">

<input class="user-data required" id="itemID" type="hidden" value="<?=$model->id?>" />
<input class="user-data required" id="baseID" type="hidden" value="<?=$model->id_base?>" />
<input class="user-data form-control" id="title" type="text" placeholder="Заголовок" value='<?=$model->title?>' />

<br />

<select class="user-data form-control" id="origin_show" >
<option value="1" <?php echo ( $model->origin_show == 1 ) ? 'selected=true' : ''; ?> >Показывать ссылку</option>
<option value="0" <?php echo ( $model->origin_show == 0 ) ? 'selected=true' : ''; ?> >Не показывать ссылку</option>
</select>

<br />

<input class="user-data form-control" id="origin_url" type="text" placeholder="Ссылка на оригинал" value='<?=$model->origin_url?>' />

<br />

<textarea class="user-data form-control" id="text" style="text-align: left !important; height: 400px; overflow-x: auto; resize: none;"><?=$model->text?></textarea>
</div>