<div class="item">
<input type="button" class="btn btn-primary" value="Авторизация" onclick=""/>
<input type="button" class="btn btn-primary" value="Ключ доступа" onclick=""/>
<br /><br />

<table class="items-list">
<thead>
<tr>
<th>&nbsp;</th>
<th>Username</th>
<th>UserID</th>
<th>Created</th>
<th>Updated</th>
</tr>
</thead>
<tbody>
<?php if( $model ): ?>
    <?php foreach( $model as $k => $v ): ?>
    <?php $active = $v->id == $this->baseID ? true : false; ?>
    <tr class="itemid <?=$active ? 'active' : ''?>" id="<?=$v->id?>">
    <td style="text-align: left; padding-left: 15px;"><a href="/<?=$this->id?>/edit/<?=$v->id?>/"><?=$v->name?></a></td>
    <td><?=$v->groupsCount?></td>
    <td><?=$v->itemsCount?></td>
    <td><?=$v->mediaCount?></td>
    <td><?=$this->FileSizeConvert( $v->mediaSize )?></td>
    <td><?=$v->tagsCount?> / <?=$v->itemsCount - $v->tagsCount?></td>
    <td><?=date( "Y-m-d H:i:s", $v->created )?></td>
    </tr>
    <?php endforeach; ?>
<?php else: ?>
<tr class="itemid">
<td style="text-align: center;" colspan="5">Данных нет</td>
</tr>
<?php endif; ?>
</tbody>
</table>

</div>