<div class="col-sm-12">
<?php if( isset( $this->baseID ) && $this->baseID > 0 ): ?>
<div class="item">
<h3>Закладки</h3>
<div id="asyncLoadPosts">
<?php if( $model ): ?>
<?php $this->renderPartial( "//bookmarks/list.items", array( "model" => $model ) ); ?>
<?php else: ?>
Данных нет
<?php endif; ?>
</div>

</div>
<?php else: ?>
<div class="item"><strong>Не выбрана база</strong></div>
<?php endif; ?>
</div>