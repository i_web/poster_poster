<style type="text/css">
.myTable td,
.myTable th {
    background-color: white;
}
.itemid.active td {
    background-color: #cccccc;
    color: white !important;
}
.itemid.active td a {
    color: white !important;
    text-decoration: none;
}
.itemid.active td a:hover {
    text-decoration: underline;
}
</style>
<input type="button" class="btn btn-primary" value="Добавить закладку" onclick="baseBookMark.add( <?=$this->baseID?> )" />
<input type="button" class="btn btn-primary" value="Обновить закладки" onclick="baseBookMark.update( <?=$this->baseID?> )" />
<br />
<br />
<table class="table table-bordered table-hover myTable">
<thead>
<th>#</th>
<th>photo</th>
<th>ID Группы</th>
<th>Название</th>
<th>Добавлена</th>
<th>Обновлена</th>
</thead>
<tbody>
<?php if( $model ): ?>
    <?php foreach( $model as $k => $v ): ?>
    <?php $arrData = json_decode( $v->arrData, true );?>
    <tr class="itemid">
    <td><?=$k?></td>
    <td><img style="border-radius: 50%;" src="<?php echo $arrData['photo_100']; ?>" /></td>
    <td><a onclick="bookmarks.view( <?=$v->id_group?> )" href="javascript: void(0)"><?=$v->id?></a></td>
    <td><a onclick="bookmarks.view( <?=$v->id_group?> )" href="javascript: void(0)"><?=$v->name?></a></td>
    <td><?=date("Y-m-d H:i:s", $v->created)?></td>
    <td><?=date("Y-m-d H:i:s", $v->updated)?></td>
    </tr>
    <?php endforeach; ?>
<?php else: ?>
<tr class="itemid">
<td colspan="3">Нет данных</td>
</tr>
<?php endif; ?>
</tbody>
</table>