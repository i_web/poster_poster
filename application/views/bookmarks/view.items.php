<?$this->widget('CLinkPager', array(
//'internalPageCssClass' => '',
'pages' => $pages,
'id' => '',
'header' => '',
'maxButtonCount' => 5,
'selectedPageCssClass' => 'active',
//'hiddenPageCssClass' => 'disabled',
'nextPageLabel' => '&raquo;',         // »
'prevPageLabel' => '&laquo;',         // «
'lastPageLabel' => '&raquo;&raquo;',  // »»
'firstPageLabel' => '&laquo;&laquo;', // ««
'htmlOptions' => array('class' => 'pagination tabsLoadPagination'),
))
?>


<br />
<div class="row">

<?php if(count( $model )): ?>
    <?php foreach( $model[ "response" ][ "items" ] as $k => $v ): ?>
    <?php
    //$v = json_encode( $v );
    //$v = json_decode( $v );
    $title = explode( "\n", $v[ "text" ] );
    ?>
    <div class="col-sm-12">
		<div class="panel panel-white">
			<div class="panel-heading border-light">
				<h4 class="panel-title"><?php echo isset($title[0]) ? $title[0] : ''; ?></h4>
				<ul class="panel-heading-tabs border-light">
					<li>
						<strong><?=date("d-m-Y H:i:s", $v[ "date" ])?></strong>
					</li>
                    <li>
                    <code>Likes: <?=$v[ 'likes' ][ 'count' ]?></code>
                    </li>
                    <li>
                    <code>Reposts: <?=$v[ 'reposts' ][ 'count' ]?></code>
                    </li>
					<li class="panel-tools">
						<a data-original-title="Редактировать" data-toggle="tooltip" data-placement="top" class="btn btn-transparent btn-sm panel-refresh" href="javascript: void(0)" onclick="posts.import.get( <?=$_GET[ "id" ]?>_<?=$v[ "id" ]?> )">
                            <i class="ti-plus"></i>
                        </a>
					</li>
				</ul>
			</div>
            
			<div class="panel-body">
                <div class="col-sm-12">
                    <div class="panel panel-white collapses" id="panel5" style="margin-bottom: 0px;">
						<div class="panel-heading">
							<h4 class="panel-title text-primary"><a class="panel-collapse" href="#" style="display: block;">Описание</a></h4>
							<div class="panel-tools">
								<a data-original-title="Collapse" data-toggle="tooltip" data-placement="top" class="btn btn-transparent btn-sm panel-collapse" href="#"><i class="ti-minus collapse-off"></i><i class="ti-plus collapse-on"></i></a>
							</div>
						</div>
						<div class="panel-body" style="display: none;">
                            <p style="unicode-bidi: embed; font-family: monospace; white-space: pre-line;">
                            <?=trim($v[ "text" ])?>
                            </p>
						</div>
					</div>
                </div>
			</div>
            
            <?php if( isset( $v[ "attachments" ] ) && count( $v[ "attachments" ] )): ?>
            <div class="panel-body">
                <div class="col-sm-12">
					<div class="panel panel-white collapses" id="panel5">
						<div class="panel-heading">
							<h4 class="panel-title text-primary"><a class="panel-collapse" href="#" style="display: block;">Медиа ( <?=count( $v[ "attachments" ] )?> )</a></h4>
							<div class="panel-tools">
								<a data-original-title="Collapse" data-toggle="tooltip" data-placement="top" class="btn btn-transparent btn-sm panel-collapse" href="#"><i class="ti-minus collapse-off"></i><i class="ti-plus collapse-on"></i></a>
							</div>
						</div>
						<div class="panel-body" style="display: none;">
                            <?php foreach( $v[ "attachments" ] as $file ): ?>
                                <div class="col-sm-6 col-md-3">
                                    <?php if( $file[ "type" ] == "photo" ): ?>
                                    <a data-fancybox="gallery_<?=$v[ "id" ]?>" style="text-decoration: none;" href="<?php echo $file['photo']['photo_604']; ?>">
                                        <div class="thumbnail" style="background: url(<?php echo $file['photo']['photo_604']; ?>); background-size: cover; width: 250px; height: 250px;">
                                        </div>
                                    </a>
                                    <?php elseif( $file[ "type" ] == "doc" ): ?>
                                    <a data-fancybox="gallery_<?=$v[ "id" ]?>" style="text-decoration: none;" href="<?php echo $file["doc"]["url"]; ?>">
                                        <div class="thumbnail" style="background: url(<?php echo isset($file['doc']['photo_100']) ? $file['doc']['photo_100'] : "/images/404kitten.jpg"; ?>); background-size: cover; width: 250px; height: 250px;">
                                        </div>
                                    </a>
                                    <?php endif; ?>
                                </div>
                            <?php endforeach; ?>
						</div>
					</div>
				</div>
            </div>
            <?php endif; ?>
		</div>
	</div>
    <?php endforeach; ?>
<?php else: ?>
    <div style="text-align: center;"><h3>Поиск не дал результатов</h3></div>
<?php endif; ?>
</div>
<br />


<?$this->widget('CLinkPager', array(
//'internalPageCssClass' => '',
'pages' => $pages,
'id' => '',
'header' => '',
'maxButtonCount' => 5,
'selectedPageCssClass' => 'active',
//'hiddenPageCssClass' => 'disabled',
'nextPageLabel' => '&raquo;',         // »
'prevPageLabel' => '&laquo;',         // «
'lastPageLabel' => '&raquo;&raquo;',  // »»
'firstPageLabel' => '&laquo;&laquo;', // ««
'htmlOptions' => array('class' => 'pagination tabsLoadPagination'),
))
?>