<div class="col-sm-12">

<div class="btn-group">
	<a class="btn btn-wide btn-info" href="javascript: void(0)" onclick="bookmarks.index()">
		<i class="fa fa-backward"></i>
	</a>
</div>

<br /><br />

<?php if( $model ): ?>
<?php $this->renderPartial( "//bookmarks/view.items", array( "model" => $model, "pages" => $pages ) ); ?>
<?php else: ?>
<div class="alert alert-danger" style="text-align: center;">
	<strong>Ошибка данных</strong>
</div>
<?php endif; ?>
</div>