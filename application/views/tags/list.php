<div class="col-sm-12">

<?php if( isset( $this->baseID ) && $this->baseID > 0 ): ?>

<div class="btn-group">
    <a class="btn btn-wide btn-primary" href="javascript: void(0)" onclick="tags.create()">
		Create Tag
	</a>
</div>

<br /><br />

<?php if($model): ?>
<div class="list-group">
    <?php foreach( $model as $k => $v ): ?>
    <a class="list-group-item" href="javascript: void(0)" onclick="tags.edit( <?=$v->id?> )">
        <span class="badge"><?=$v->tagsCount?></span>
		<?php echo $v->clevel > 0 ? str_repeat( "&nbsp;", $v->clevel * 6 ).$v->name : $v->name; ?> ( <?=$v->tagsCount?> )
	</a>
    <?php endforeach; ?>
</div>
<?php else: ?>

<?php endif; ?>

<?php else: ?>
<div class="alert alert-danger" style="text-align: center;">
	<strong>Не выбрана база</strong>
</div>
<?php endif; ?>

</div>