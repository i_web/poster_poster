<div class="simple-table popupForm" style="width: 1000px; height: auto !important;">

<h3 style="text-align: center;">Редактируем теги:</h3>

<div class="container-fluid container-fullw bg-white">
        <div class="row">
    		<div class="col-md-12">
                <div class="form-group">
                <?php echo CHtml::activeHiddenField( $model, 'id', array( 'class' => 'user-data form-control', 'id' => 'itemID', 'placeholder' => 'itemID', 'value' => 0 ) ); ?>
                <?php echo CHtml::activeHiddenField( $model, 'id_base', array( 'class' => 'user-data form-control', 'id' => 'baseID', 'placeholder' => 'baseID', 'value' => $this->baseID ) ); ?>
                <br />
                <?php echo CHtml::activeTextField( $model, 'name', array( 'class' => 'user-data form-control', 'id' => 'name', 'placeholder' => 'Name' ) ); ?>
                <br />
                <?php echo CHtml::activeDropDownList(
                    $model, 'id_parent',
                    CHtml::listData( $tagsList, 'id', 'name'),
                    array( 'class' => 'user-data form-control', 'id' => 'parentID', 'empty' => 'Выберите родительскую категорию' )
                ); ?>
                </div>
            </div>
        </div>
</div>


<div class="container-fluid container-fullw bg-white">

<div class="row">
		<div class="col-md-12" style="text-align: center;">
        
        <input type="button" style="width: 45%;" value="Закрыть" class="btn btn-warning btn-wide fancybox-close" />
        
        <input type="button" style="width: 45%;" value="Сохранить" onclick="tags.save()" class="btn btn-wide btn-success" />
        
        </div>
        
</div>

</div>

</div>